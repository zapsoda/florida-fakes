<?php
require_once("php_include/orderFunctions.php");
require_once("php_include/validate.php");
session_start();
if (!session_check()||$_SESSION['page']<1) {
    header('Location: order.php');
    die();
}
ob_start();
$errorMessage = array();
$shipping_method = post_value_or('shipping_method', 0);
$recipient_first_name = str_replace(' ', '_', post_value_or("recipient_first_name"));
$recipient_last_name = str_replace(' ', '_', post_value_or("recipient_last_name"));
$recipient_name = $recipient_first_name . " " . $recipient_last_name;
$email = post_value_or("email");
$production_speed = post_value_or('production_speed');
$address_line_1 = post_value_or("address_line_1");
$address_line_2 = post_value_or("address_line_2");
$zip_code = post_value_or("zip_code");
$state = post_value_or("state");
$_POST['country'] = 'USA';
$country = post_value_or("country");
$payement_method = post_value_or("payment");
if (isset($_POST["submit_order"])) {
    $production_speedup_charge = intval($production_speed) . ".00";
    $val = new validation;
    $val->addSource($_POST);
    $val->addRule('shipping_method', 'numeric', true, 0, 2, true, 'Shipping method name')
        ->addRule('recipient_first_name', 'string', true, 3, 50, true, 'First name')
        ->addRule('recipient_last_name', 'string', true, 3, 50, true, 'Last name')
        ->addRule('email', 'email', true, 1, 50, true, 'Email')
        ->addRule('production_speed', 'numeric', true, 0, 999, true, 'Production speed')
        ->addRule('address_line_1', 'string', true, 5, 250, true, 'Address 1')
        ->addRule('address_line_2', 'string', false, 5, 250, true, 'Address 2')
        ->addRule('zip_code', 'string', true, 5, 5, true, 'Zip')
        ->addRule('state', 'string', true, 2, 2, true, 'State')
        ->addRule('country', 'string', true, 2, 3, true, 'Country')
        ->addRule('payment', 'string', true, 7, 8, true, 'Payment');
    $val->run();
    $errorMessage = $val->errors;    
    if (empty($errorMessage)) {
        $_SESSION['shipping_method'] = $shipping_method;
        $_SESSION['production_speed'] = $production_speed;
        $_SESSION['recipient_first_name'] = $recipient_first_name;
        $_SESSION['recipient_last_name'] = $recipient_last_name;
        $_SESSION['order_id'] = generate_order_id($recipient_last_name);
        $_SESSION['recipient_name'] = $recipient_name;
        $_SESSION['email'] = $email;
        $_SESSION['address_line_1'] = $address_line_1;
        $_SESSION['address_line_2'] = $address_line_2;
        $_SESSION['zip_code'] = $zip_code;
        $_SESSION['state'] = $state;
        $_SESSION['country'] = $country;
        $_SESSION['payment_method'] = $payement_method;
        $_SESSION['production_speedup_charge'] = $production_speedup_charge;
        if ((isset($_POST['is_new_user'])) && ($_POST['is_new_user'] == "No")) {
            if (!empty($_POST['password'])) {
                $conn = create_connection();
                if (!check_password($email, $_POST['password'])) {
                    array_push($errorMessage, "Incorrect Password!");
                    $_POST['is_new_user'] = "no";
                } else {
                    $_SESSION['page']=2;
                    $_POST['submit'] = NULL;
                    header('Location: order_preview.php');
                    die();
                }
            } else {
                array_push($errorMessage, "Forgot password.");
                $_POST['is_new_user'] = "no";
            }
        } else {
            $conn = create_connection();
            if (!user_exist($email)) {
                $_SESSION['is_new_user'] = "Yes";
                $_SESSION['page']=2;
                $_POST['submit'] = NULL;
                Header('Location: order_preview.php');
                die();
            } else {
                array_push($errorMessage, "There is already a account for that email.");
                $_POST['is_new_user'] = "no";
            }
        }
    }
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Florida Fakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Kevin Rajaram">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
        <script type="text/javascript" src="js/jquery.qtip.min.js"></script>
        <script type="text/javascript">
         $(document).ready(function()
         {
            $('input').qtip({
                show: 'focus',
                hide: 'blur',
                position: {
                    at: 'bottom center',
                    target: 'event'
                }
            });
         });
        </script>
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/jquery.qtip.min.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="header">
            <div class="container">
                <div class="logo"><a href="index.php" title="Home"><img src="img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->

                <nav>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li class="active"><a href="order.php">Order</a></li>
                    </ul>
                </nav>
            </div><!-- /container -->
        </section><!-- #header -->
        
        <section id="announcements">
            <div class="container">
                <h5 class="announce-icon"><strong>Announcements</strong></h5>
                <?php echo getContent('announcement'); ?>
            </div><!-- /container -->
        </section><!-- #announcements -->
        
        <section id="order">
            <div class="container">
                <h1>Order Now</h1>
                <div id="shippingForm">
                    <?php
                    if (!empty($errorMessage)) {
                        echo '<div id="errors">';
                        foreach ($errorMessage as $error) {
                            echo '<span class="error" style="color: red;">'.$error.'</span><br>';
                        }
                        echo '</div>';
                    }
                    ?>
                    <form action="" method="post" name="shipping_info">
                        Shipping Method: <font color="#FF0000"> * </font><br>
                        <select name="shipping_method" class="input_select">
                               <option value="0" selected="selected">&nbsp;</option>
                               <option value="<?=shipping_speed1_cost?>" <?php if ($shipping_method == shipping_speed1_cost){echo "selected=\"selected\"";}?>><?=shipping_speed1_name?></option>
                               <option value="<?=shipping_speed2_cost?>" <?php if ($shipping_method == shipping_speed2_cost){echo "selected=\"selected\"";}?>><?=shipping_speed2_name?></option>
                        </select>
                        <br>
                        Production Speed: <font color="#FF0000"> * </font><br>
                        <select name="production_speed" class="input_select">
                            <option>&nbsp;</option>
                            <option value="<?=production_speed1_cost?>" <?php if($production_speed == production_speed1_cost){echo "selected=\"selected\"";}?>><?=production_speed1_name?></option>
                            <option value="<?=production_speed2_cost?>" <?php if($production_speed == production_speed2_cost){echo "selected=\"selected\"";}?>><?=production_speed2_name?></option>
                        </select>
                        <br>
                        First Name<font color="#FF0000"> * </font><br>
                        <input type="text" placeholder="John" name="recipient_first_name" maxlength="50" value="<?=$recipient_first_name?>" title="Shipping first name">
                        <br>
                        Last Name<font color="#FF0000"> * </font><br>
                        <input type="text" placeholder="Smith" name="recipient_last_name" maxlength="50" value="<?=$recipient_last_name?>"  title="Shipping last name">
                        <br>
                        Email Address<font color="#FF0000"> * </font><br>
                        <input type="text" placeholder="JSmith@example.com" name="email" maxlength="50" class="input_text" value="<?=$email?>" title="Email address for your account">
                        <br>
                        <label for="new_user_yes">New User</label><input type="radio" name="is_new_user" autocomplete="off" value="Yes" id="new_user_yes" checked="checked" onclick="document.getElementById('password_tr').style.display = 'none';"><br>
                        <label for="new_user_no">Existing User</label><input type="radio" name="is_new_user" autocomplete="off" value="No" id="new_user_no" <?php if ((isset($_POST['is_new_user']) && ($_POST['is_new_user'] == "no"))){echo "checked=\"checked\"";}?>><br>
                        Password (If existing user)<font color="#FF0000"> * </font><br>
                        <input type="password" placeholder="******" name="password" maxlength="10" class="input_text" title="This was sent to you when you first ordered"><br>
                        Address Line 1 <font color="#FF0000"> * </font><br>
                        <input type="text" placeholder="123 Main street" name="address_line_1" maxlength="60" class="input_text" value="<?=$address_line_1?>" title="Shipping address line one"> <br>
                        Address Line 2: <br>
                        <input type="text" placeholder="Suite 123" name="address_line_2" maxlength="60" class="input_text" value="<?=$address_line_2?>" title="Shipping address line two"> <br>
                        Zip Code<font color="#FF0000"> * </font><br>
                        <input type="text" placeholder="12345" name="zip_code" maxlength="6" class="input_text" value="<?=$zip_code;?>" title="Shipping zip code"> <br>
                        State<font color="#FF0000"> * </font><br>
                        <?php if (isset($state)) {echo listUSStates($state);} else {echo listUSStates();}?>
                        <br>
                        How would you like to pay<font color="#FF0000"> * </font><br>
                        <select name="payment">
                            <option></option>
                            <option value="ReLoadIt" <?php if($payement_method == "ReLoadIt"){echo "selected=\"selected\"";}?>>ReloadIt</option>
                            <option value="Bitcoin" <?php if($payement_method == "Bitcoin"){echo "selected=\"selected\"";}?>>Bitcoin</option>
                        </select>
                        <br>
                        <input class="button" name="submit_order" style="width:500px;" type="submit" value="Next"/>
                    </form>
                    <a href="./logout.php?redirect=order.php"><input class="button" type="submit" style="width:500px;" value="Start Over"/></a>
                </div>
                <div id="shoppingCart">
                    <?php
                    include "php_include/shopping_cart.php";
                    ?>
                </div>
                <div style="clear:both;"></div>
            </div><!-- /container -->
        </section><!-- #header -->

        <section id="footer">
            <div class="container">
                <div class="half">
                    <p class="large">FloridaFakes</p>
                    <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
                </div>

                <div class="half">
                    <ul class="footer-nav">
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li><a href=""></a></li>
                        <li><a href="order.php">Order</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div><!-- /container -->
        </section><!-- #footer -->
    </body>
</html>
<?php ob_end_flush(); ?>