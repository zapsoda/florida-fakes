<?php
require_once("php_include/orderFunctions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Florida Fakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Kevin Rajaram">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="header">
            <div class="container">
                <div class="logo"><a href="index.php" title="Home"><img src="img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->

                <nav>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li class="active"><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li class="active"><a href="order.php">Order</a></li>
                    </ul>
                </nav>
            </div><!-- /container -->
        </section><!-- #header -->

        <section id="announcements">
            <div class="container">
                <h5 class="announce-icon"><strong>Announcements</strong></h5>
                <?php echo getContent('announcement'); ?>
            </div><!-- /container -->
        </section><!-- #announcements -->

        <section id="prices">
            <div class="container">

                <h1>Prices</h1>

                <div class="high-notice">
                    <p>Group orders must be shipped to the same address. </p>
                    <p>Group orders must be for the same state.</p>
                    <p>Group orders with over 10 IDs get FREE priority shipping and $50 tracking.</p>
                    <p>Group orders with over 20 IDs get FREE priority shipping and FREE tracking.</p>
                </div><!-- /high-notice -->

                <div class="center">
                    <h4>1-2 PEOPLE – $150 each</h4>
                    <h4>3-5 PEOPLE – $125 each</h4>
                    <h4>6-9 PEOPLE – $110 each</h4>
                    <h4>10+ PEOPLE – $100 each</h4>

                    <br/>

                    <p><span class="green">BACK-UP COPY</span> – $50 EACH</p>
                    <p><span class="green">CUSTOM SIGNATURE</span> – $50 (not necessary, we have realistic signature fonts)</p>
                    <p><span class="green">IN STATE FLORIDA</span> – $25 PER ID (ADDS A CLEAR BACK OVERLAY TO THE FLORIDA ID)</p>
                </div><!-- /center -->

                <h1>Payment Methods</h1>
                <br/>
                <img src="img/money.jpg" alt="" style="margin-left:-10px;"/>
                <br/>

                <div class="bitcoin">
                    <h3><strong>bitcoin</strong> Instructions</h3>
                    <p>1) Find a REloadit – They are at the prepaid card section at Safeway, Vons, Pavillions, Carrs, Walgreens, CVS, Rite Aid, 7-Eleven, K-Mart, Dominicks, Randalls, Tom Thumbs, Kroger, Meijer, etc. You can use REloadit’s store locator to find a store near you : <a href="https://www.reloadit.com/store_locator.aspx">https://www.reloadit.com/store_locator.aspx</a></p>

                    <p>2) Grab a REloadit from the prepaid card section and take it to the register. The cashier will ask you how much you want to load onto it. There is nothing sketchy about buying a REloadit either. It’s in the store for a reason!</p>

                    <p>3) Scratch the numbers off of the back and submit the 10 digit number with your order form. It should look exactly like the picture. Do NOT buy a prepaid card (usually have 16 digits). <strong>We DO NOT accept prepaid cards (16 digits)! We DO NOT accept NETSPEND RELOADS  (12 digits) We only accept the REloadit packs (10 digits)</strong>. Please use only ONE REloadit pack for orders below $500! You can load up to $500 on each one, and some even accept up to $1000!</p>
                </div><!-- /bitcoin -->

                <div class="reloadit">
                    <h3><strong>REloadit</strong> Instructions</h3>
                    <p>1) Find a REloadit – They are at the prepaid card section at Safeway, Vons, Pavillions, Carrs, Walgreens, CVS, Rite Aid, 7-Eleven, K-Mart, Dominicks, Randalls, Tom Thumbs, Kroger, Meijer, etc. You can use REloadit’s store locator to find a store near you : <a href="https://www.reloadit.com/store_locator.aspx">https://www.reloadit.com/store_locator.aspx</a></p>

                    <p>2) Grab a REloadit from the prepaid card section and take it to the register. The cashier will ask you how much you want to load onto it. There is nothing sketchy about buying a REloadit either. It’s in the store for a reason!</p>

                    <p>3) Scratch the numbers off of the back and submit the 10 digit number with your order form. It should look exactly like the picture. Do NOT buy a prepaid card (usually have 16 digits). <strong>We DO NOT accept prepaid cards (16 digits)! We DO NOT accept NETSPEND RELOADS  (12 digits) We only accept the REloadit packs (10 digits)</strong>. Please use only ONE REloadit pack for orders below $500! You can load up to $500 on each one, and some even accept up to $1000!</p>

                </div>

                <div class="moneypak">
                    <h3><strong>MoneyPak</strong> Instructions</h3>
                    <p>1) Find a REloadit – They are at the prepaid card section at Safeway, Vons, Pavillions, Carrs, Walgreens, CVS, Rite Aid, 7-Eleven, K-Mart, Dominicks, Randalls, Tom Thumbs, Kroger, Meijer, etc. You can use REloadit’s store locator to find a store near you : <a href="https://www.reloadit.com/store_locator.aspx">https://www.reloadit.com/store_locator.aspx</a></p>

                    <p>2) Grab a REloadit from the prepaid card section and take it to the register. The cashier will ask you how much you want to load onto it. There is nothing sketchy about buying a REloadit either. It’s in the store for a reason!</p>

                    <p>3) Scratch the numbers off of the back and submit the 10 digit number with your order form. It should look exactly like the picture. Do NOT buy a prepaid card (usually have 16 digits). <strong>We DO NOT accept prepaid cards (16 digits)! We DO NOT accept NETSPEND RELOADS  (12 digits) We only accept the REloadit packs (10 digits)</strong>. Please use only ONE REloadit pack for orders below $500! You can load up to $500 on each one, and some even accept up to $1000!</p>

                </div>


                <div class="left">
                    <h1>Shipping</h1>
                    <h5>STANDARD 3-5 DAY – FREE (NOT RECOMMENDED)</h5>
                    <h5>PRIORITY 2-3 DAY – $20 (FREE FOR ORDERS WITH OVER 10 IDS)</h5>
                    <br>
                    <h4 class="green">TRACKING/INSURANCE – $100</h4>
                    <p>Add insurance to your order for $100! Tracking is not necessary, but it insures your order in the rare case that something happens to your package. Orders with over 10 IDs will get $50 off tracking, and orders with over 20 IDs will get free tracking. We really recommend pitching in for insurance if you have a larger order. It will only cost $5-10 extra per person and it’s definitely worth it. We will email you the tracking number with a photo of your finished IDs once your order ships.</p>

                    <h4 class="green">PAY FOR PRIORITY SHIPPING!</h4>
                    <p>USPS guarantees Priority Mail in 2-3 business days! It’s also much safer to have a large priority mail envelope/box than to have a small envelope that can potentially get lost. When you use regular First Class mail your ID can take anywhere from 3-5 days to arrive. Sometimes even longer! While it is rare, First Class white envelopes DO get lost sometimes in the mail. We will not remake your IDs because your envelope got lost in the mail. We recommend paying for tracking!</p>
                </div>
                <div class="right">
                    <h1>Rush Production</h1>
                    <p>Rush production places you at the rront of the proudction line and ensures you have your ID(s) faster than everyone else. It does not mean you will get your ID the day after you order. If you are paying for rush production, you should also pay for priority shipping to ensure your IDs arrive quickly.</p>

                    <h4>1-2 people – <span class="green">$50 per person</span></h4>
                    <h4>3-5 people – <span class="green">$45 per person</span></h4>
                    <h4>6-9 people – <span class="green">$40 per person</span></h4>
                    <h4>10+ people – <span class="green">$35 per person</span></h4>
                </div>

                <div class="clearfix"></div>

                <br/>
                <a href="order.php" class="button">PLACE YOUR ORDER TODAY!</a>
            </div><!-- /container -->
        </section><!-- #prices -->

        <section id="footer">
            <div class="container">
                <div class="half">
                    <p class="large">FloridaFakes</p>
                    <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
                </div>

                <div class="half">
                    <ul class="footer-nav">
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li><a href=""></a></li>
                        <li><a href="order.php">Order</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div><!-- /container -->
        </section><!-- #footer -->




        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script src="js/main.js"></script>

        <script>
            $("#tabs").tabs({show: {effect: "fadeIn", duration: 500}});
        </script>
    </body>
</html>
