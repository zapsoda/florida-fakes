<?php
#DB info
DEFINE('db_name', ""); // Name of DB
DEFINE('db_user', ""); // Username to your DB
DEFINE('db_pass', ""); // Password to your DB

#Password info
DEFINE('passwordSalt', 'gojLvoG3efg8wlllU2qsddBtkddJ8YwwkldieApqiwu1bRoirug'); //Can make random

#upload info
DEFINE('upload_dir', "/test/upload/"); // Where photos should be uploaded to
DEFINE('project_path', '/test');
#Email info
DEFINE('email_user', ""); // Your email user name
DEFINE('email_pass', ""); //The password to your email
DEFINE('email_host', "");

DEFINE('domain', 'www.floridafakes.com/test');
DEFINE('session_token', "qimr77dvm1dp4ddle");

#BTC info
DEFINE('BTC_Address', "16ZBWaM3NB4wW8PNg3tPUr1CunNtSLtdqV");

#Speedup Charges
DEFINE('production_speed1_name', "Standered");
DEFINE('production_speed2_name', "Rush");
DEFINE('production_speed1_cost', "10");
DEFINE('production_speed2_cost', "20");

#Shipping Charges, Do not change the cost values
DEFINE('shipping_speed1_name', "First Class (not recommended) - FREE");
DEFINE('shipping_speed2_name', "Priority (20$ per unique issue state)");
DEFINE('shipping_speed1_cost', "1");
DEFINE('shipping_speed2_cost', "2");

#Bulk Charges
DEFINE('bulkPrice1', "200");
DEFINE('bulkPrice2', "150");
DEFINE('bulkPrice3', "100");

$stateInfo=array(
		array('name'=>'Colorado', 'id'=>1, 'eyeColor'=>array('Black'=>'BLK', 'Green'=>'GRN', 'Brown'=>'BRO', 'Blue'=>'BLU'), 'hairColor'=>array('Brown'=>'BRN', 'Blonde'=>'BLN', 'Black'=>'BLK', 'Red'=>'RED')),
		array('name'=>'Florida', 'id'=>2, 'eyeColor'=>array('Black'=>'BLK', 'Green'=>'GRN', 'Brown'=>'BRN', 'Blue'=>'BLU'), 'hairColor'=>array('Brown'=>'BRN', 'Blonde'=>'BLN', 'Black'=>'BLK', 'Red'=>'RED')),
		array('name'=>'California', 'id'=>3, 'eyeColor'=>array('Black'=>'BLK', 'Green'=>'GRN', 'Brown'=>'BRN', 'Blue'=>'BLU'), 'hairColor'=>array('Brown'=>'BRN', 'Blonde'=>'BLN', 'Black'=>'BLK', 'Red'=>'RED')),
		array('name'=>'New York', 'id'=>4, 'eyeColor'=>array('Black'=>'BK', 'Green'=>'GR', 'Brown'=>'BR', 'Blue'=>'BL'), 'hairColor'=>array('Brown'=>'BRN', 'Blonde'=>'BLN', 'Black'=>'BLK', 'Red'=>'RED')),
		array('name'=>'Illinois', 'id'=>5, 'eyeColor'=>array('Black'=>'BLK', 'Green'=>'GRN', 'Brown'=>'BRN', 'Blue'=>'BLU'), 'hairColor'=>array('Brown'=>'BRN', 'Blonde'=>'BLN', 'Black'=>'BLK', 'Red'=>'RED'))
	);

$sources= array('shipping_info.php', 'order.php', 'order_preview.php');