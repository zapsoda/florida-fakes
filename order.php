<?php
require_once("php_include/orderFunctions.php");
require_once("php_include/imgUploader.class.php");
require_once("php_include/validate.php");
session_start();
$_SESSION['type'] = 'order';
$errorMessage = array();
$first_name = post_value_or("first_name", NULL);
$middle_name = post_value_or("middle_name", NULL);
$last_name = post_value_or("last_name", NULL);
$DOBDay = post_value_or("DOBDay", NULL);
$DOBMonth = post_value_or("DOBMonth", NULL);
$DOBYear = post_value_or("DOBYear", NULL);
$address = post_value_or("address", NULL);
$city = post_value_or("city", NULL);
$state = post_value_or("state", NULL);
$zip = post_value_or("zip", NULL);
$gender = post_value_or("gender", NULL);
$weight = post_value_or("weight", NULL);
$heightFeet = post_value_or("heightFeet", NULL);
$heightInch = post_value_or("heightInch", NULL);
$eyeColor = post_value_or("eyeColor", NULL);
$hairColor = post_value_or("hairColor", NULL);
$quantity = post_value_or("number_of_shirts", NULL);
$printedSig = post_value_or("customPrintedSignature", "");
if((isset($_GET['source']))&&(isset($sources[$_GET['source']]))){$redirect=$sources[$_GET['source']];}else{$redirect="shipping_info.php";}
if (!first_session_check()) {
    array_push($errorMessage, "Your session has expired.<br>Please, start over.");
} else {
    $person_id = generate_person_id($first_name, $last_name);
    if (isset($_POST['add'])) {
        $val = new validation;
        $val->addSource($_POST);
        $val->addRule('first_name', 'string', true, 3, 50, true, 'First name')
            ->addRule('middle_name', 'string', false, 3, 50, true, 'Middle name')
            ->addRule('last_name', 'string', true, 3, 50, true, 'Last name')
            ->addRule('DOBDay', 'numeric', true, 1, 31, true, 'Day')
            ->addRule('DOBMonth', 'numeric', true, 1, 12, true, 'Month')
            ->addRule('DOBYear', 'numeric', true, date("Y")-75, date("Y")-21, true, 'Year')
            ->addRule('address', 'string', true, 5, 250, true, 'Address')
            ->addRule('city', 'string', true, 3, 150, true, 'City')
            ->addRule('state', 'issueState', true, 1, 15, true, 'State')
            ->addRule('zip', 'string', true, 5, 5, true, 'Zip')
            ->addRule('gender', 'string', false, 4, 6, true, 'Gender')
            ->addRule('weight', 'numeric', true, 75, 800, true, 'Weight')
            ->addRule('heightInch', 'numeric', true, 0, 12, true, 'Inch')
            ->addRule('heightFeet', 'numeric', true, 4, 7, true, 'Feet')
            ->addRule('eyeColor', 'string', true, 1, 15, true, 'Eye color')
            ->addRule('hairColor', 'string', true, 1, 15, true, 'Eye color')
            ->addRule('number_of_shirts', 'numeric', true, 1, 4, true, 'Number of shirts')
            ->addRule('customPrintedSignature', 'string', false, 3, 50, true, 'Custom Signature');
        $val->run();
        $errorMessage = $val->errors;
        if (empty($errorMessage)) {
            if(getAge($DOBYear.'-'.$DOBMonth.'-'.$DOBDay, date("d-M-Y")) < 21){
                array_push($errorMessage, "Your age is below 21.");
            }
        }

        if (empty($errorMessage)) {
            if (($_FILES["photo"]["error"] == 0) && ($_FILES["photo"]["error"] != 4)) {
                try{
                    $img = new imgUploader($_FILES['photo']);
                    $dir = upload_dir;
                    $IDPicture = $img->upload_unscaled($dir, $person_id);
                    $photoError = $img->getError();
                    if ($photoError != 0) {
                       throw new Exception($photoError);
                    }
                } catch (Exception $e) {
                    array_push($errorMessage, $e->getMessage());
                }
            } else {
                $IDPicture = 
                array_push($errorMessage, 'You forgot to upload a ID picture');
            }
        }
        
        if (empty($errorMessage)) {
            if (($_FILES["signature"]["error"] == 0) && ($_FILES["signature"]["error"] != 4)) {
                try{
                    $img = new imgUploader($_FILES['signature']);
                    $dir = upload_dir;
                    $sigFilename = $img->upload_unscaled($dir, 'sig_'.$person_id);
                    $sigError = $img->getError();
                    if ($sigError != 0) {
                       throw new Exception($sigError);
                    }
                } catch (Exception $e) {
                    array_push($errorMessage, $e->getMessage());
                }
            } else {
                $sigFilename = NULL;
            }
        }

        if (empty($errorMessage)) {
            if (!isset($_SESSION['cart'])) {$_SESSION['cart'] = array();}
            $_SESSION['person_id'] = $person_id;
            $_SESSION['cart'][$person_id] = array();
            $_SESSION['cart'][$person_id]['FName'] = $first_name;
            $_SESSION['cart'][$person_id]['MName'] = $middle_name;
            $_SESSION['cart'][$person_id]['LName'] = $last_name;
            $_SESSION['cart'][$person_id]['DOB'] = $DOBYear.'-'.$DOBMonth.'-'.$DOBDay;
            $_SESSION['cart'][$person_id]['address'] = $address;
            $_SESSION['cart'][$person_id]['city'] = $city;
            $_SESSION['cart'][$person_id]['state'] = $state;
            $_SESSION['cart'][$person_id]['zip'] = $zip;
            $_SESSION['cart'][$person_id]['gender'] = $gender;
            $_SESSION['cart'][$person_id]['weight'] = $weight;
            $_SESSION['cart'][$person_id]['height'] = $heightFeet.'-'.$heightInch;
            $_SESSION['cart'][$person_id]['eye'] = $eyeColor;
            $_SESSION['cart'][$person_id]['hair'] = $hairColor;
            $_SESSION['cart'][$person_id]['quanity'] = $quantity;
            $_SESSION['cart'][$person_id]['picture'] = $IDPicture;
            $_SESSION['cart'][$person_id]['printedSig'] = $printedSig;
            $_SESSION['cart'][$person_id]['customSig'] = $sigFilename;
            $totalQuantity = 0;
            $price_per_shirt = 0;
            foreach ($_SESSION['cart'] as $ID){
                $totalQuantity += $ID['quanity'];
            }
            if (($totalQuantity >= 1) && ($totalQuantity <= 4)) { //1-4 Shirts
                $price_per_shirt = bulkPrice1;
            } elseif (($totalQuantity >= 5) && ($totalQuantity <= 9)) { //5-9 shirts
                $price_per_shirt = bulkPrice2;
            } elseif ($totalQuantity >= 10) { //10 or more shirts
                $price_per_shirt = bulkPrice3;
            }
            foreach($_SESSION['cart'] as $key => $value) {
                $_SESSION['cart'][$key]['price'] = $price_per_shirt + ($_SESSION['cart'][$key]['quanity']-1)*50;
                $_SESSION['cart'][$key]['shirtPrice'] = $price_per_shirt;
                if($_SESSION['cart'][$key]['customSig']!=NULL){
                    $_SESSION['cart'][$key]['price']+=50;
                }
            }
            unset($first_name);
            unset($middle_name);
            unset($last_name);
            unset($DOBDay);
            unset($DOBMonth);
            unset($DOBYear);
            unset($address);
            unset($city);
            unset($state);
            unset($zip);
            unset($gender);
            unset($weight);
            unset($heightFeet);
            unset($heightInch);
            unset($eyeColor);
            unset($hairColor);
            unset($quantity);
            unset($printedSig);
            array_push($errorMessage, 'Item Added to cart.');
        }
    }

    if (isset($_POST['next'])) {
        if (isset($_SESSION['cart'])) {
            if($_SESSION['page']<1){
                $_SESSION['page']=1;
            }
            header('Location: '.$redirect);
            die();
        } else {
            array_push($errorMessage, "Please add at least 1 item to your cart.");
        }
    }
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Florida Fakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Kevin Rajaram">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
        <script type="text/javascript" src="js/jquery.qtip.min.js"></script>
        <script type="text/javascript">
         $(document).ready(function()
         {
            $('input').qtip({
                show: 'focus',
                hide: 'blur',
                position: {
                    at: 'bottom center',
                    target: 'event'
                }
            });
         });
        </script>
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/jquery.qtip.min.css">
        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="header">
            <div class="container">
                <div class="logo"><a href="index.php" title="Home"><img src="img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->

                <nav>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li class="active"><a href="order.php">Order</a></li>
                    </ul>
                </nav>
            </div><!-- /container -->
        </section><!-- #header -->
        
        <section id="announcements">
            <div class="container">
                <h5 class="announce-icon"><strong>Announcements</strong></h5>
                <?php echo getContent('announcement'); ?>
            </div><!-- /container -->
        </section><!-- #announcements -->
        
        <section id="order">
            <div class="container">
                <h1>Order Now</h1>
                <div id="orderForm">
                    <form action="" method="post" enctype="multipart/form-data">
                        <?php
                        if (!empty($errorMessage)) {
                            echo '<div id="errors">';
                            foreach ($errorMessage as $error) {
                                echo '<span class="error" style="color: red;">'.$error.'</span><br>';
                            }
                            echo '</div>';
                        }
                        ?>
                        <b>First Name</b><font color="#ff0000">*</font><br>
                        <input type="text" placeholder="John" name="first_name" size="25" class="input_text" value="<?=$first_name?>" title="First name on ID">
                        <br>
                        Middle Name:<br>
                        <input type="text" placeholder="White" name="middle_name" size="25" class="input_text" value="<?=$middle_name?>" title="Middle name on ID">
                        <br>
                        <b>Last Name</b><font color="#ff0000">*</font><br>
                        <input type="text" placeholder="Smith" name="last_name" size="25" class="input_text" value="<?=$last_name?>" title="Last name on ID">
                        <br>
                        <b>Date of Birth</b><font color="#ff0000">*</font><br>
                        <?=date_dropdown(21, $DOBDay, $DOBMonth, $DOBYear);?>
                        <br>
                        <b>Address</b><font color="#ff0000">*</font><br>
                        <input type="text" placeholder="123 Fake St" name="address" size="25" class="input_text" value="<?=$address?>" title="Address on ID">
                        <br>
                        <b>City</b><font color="#ff0000">*</font><br>
                        <input type="text" placeholder="Las Vegas" name="city" size="25" class="input_text" value="<?=$city?>" title="City on ID">
                        <br>
                        <b>Issued state</b><font color="#ff0000">*</font><br>
                        <select name="state" class="input_select">
                                <option value="0" selected="selected">&nbsp;</option>
                                <?php foreach($stateInfo as $stateID=>$info) { ?>
                                    <option value="<?=$info['id']?>" <?php if ($state == $info['id']){echo "selected=\"selected\"";}?>><?=$info['name']?></option>
                                <?php } ?>
                        </select>
                        <br>
                        <b>Zip</b><font color="#ff0000">*</font><br>
                        <input type="text" placeholder="12345" name="zip" size="25" class="input_text" value="<?=$zip?>" title="Zip code on ID">
                        <br>
                        <b>Gender</b><font color="#ff0000">*</font><br>
                        <select name="gender" class="input_select">
                            <option value="" <?php if($gender == ""){echo "SELECTED";}?>></option>
                            <option value="Male" <?php if($gender == "Male"){echo "SELECTED";}?>>Male</option>
                            <option value="Female" <?php if($gender == "Female"){echo "SELECTED";}?>>Female</option>
                        </select>
                        <br>
                        Weight (Lb)<font color="#ff0000">*</font><br>
                        <select name="weight" class="input_select">
                            <option value="" <?php if($weight == ""){echo "SELECTED";}?>></option>
                            <?php 
                            for ($i=90; $i < 350; $i+=10) {
                                echo "<option value=\"".$i."\"".(($weight == $i)?" SELECTED":"").">".$i."</option>";
                            }
                            ?>
                        </select>
                        <br>
                        <b>Height</b><font color="#ff0000">*</font><br>
                        <?=listHeight($heightFeet, $heightInch);?>
                        <br>
                        <b>Eye Color</b><font color="#ff0000">*</font><br>
                        <select name="eyeColor">
                            <option value="" <?php if($hairColor == ""){echo "SELECTED"; }?>></option>
                            <?php
                            foreach($stateInfo[0]['eyeColor'] as $fullEyeColor=>$value){
                                echo "<option value=\"".$fullEyeColor."\"". ($eyeColor == $fullEyeColor ? " SELECTED":"").'>'.$fullEyeColor."</option>";
                            }
                            ?>
                        </select>
                        <br>
                        <b>Hair Color</b><font color="#ff0000">*</font><br>
                        <select name="hairColor">
                            <option value="" <?php if($hairColor == ""){echo "SELECTED";}?>></option>
                            <?php
                            foreach($stateInfo[0]['hairColor'] as $fullHairColor=>$value){
                                echo "<option value=\"".$fullHairColor."\"". ($hairColor == $fullHairColor ? " SELECTED":"").">".$fullHairColor."</option>";
                            }
                            ?>
                        </select>
                        <br>
                        Back up copies?<font color="#ff0000"> ($50 Per Additional)</font><br>
                        <select name="number_of_shirts">
                            <option value="1" <?php if($quantity == 1){echo "SELECTED";}?>></option>
                            <option value="2" <?php if($quantity == 2){echo "SELECTED";}?>>1</option>
                            <option value="3" <?php if($quantity == 3){echo "SELECTED";}?>>2</option>
                            <option value="4" <?php if($quantity == 4){echo "SELECTED";}?>>3</option>
                        </select>
                        <br>
                        Custom Printed signature<font color="#ff0000"></font><br>
                        <input class="input_text" placeholder="J Smith" name="customPrintedSignature" type="text" value="<?=$printedSig?>" title="Please enter the text for your custom computer generated signature"/>
                        <br>
                        Custom Written Signature <font color="#ff0000">(50$ fee)</font><br>
                        <input type="file" name="signature"><br>
                        <b>ID Picture</b><font color="#ff0000">*</font><br>
                        <input type="file" name="photo"><br>
                        <br>
                        <input class="button" name="add" style="width:500px;" type="submit" value="Add"/>
                        <input class="button" name="next" style="width:500px;" type="submit" value="<?=($redirect=="shipping_info.php" ? "Next":"Go back")?>" />
                    </form>
                </div>
                <div id="shoppingCart">
                    <?php
                    include "php_include/shopping_cart.php";
                    ?>
                </div>
                <div style="clear:both;"></div>
            </div><!-- /container -->
        </section><!-- #header -->

        <section id="footer">
            <div class="container">
                <div class="half">
                    <p class="large">FloridaFakes</p>
                    <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
                </div>

                <div class="half">
                    <ul class="footer-nav">
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li><a href=""></a></li>
                        <li><a href="order.php">Order</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div><!-- /container -->
        </section><!-- #footer -->
        <script src="js/main.js"></script>
    </body>
</html>
