<?php
require_once("php_include/orderFunctions.php");
require_once("php_include/validate.php");
session_start();
$conn = create_connection();
if (!session_check()) {
  header('Location: ./order_preview.php');
  die();
}
ob_start();
$shipping_method = post_value_or('shipping_method', $_SESSION['shipping_method']);
$name = post_value_or('name', $_SESSION['recipient_first_name'].' '.$_SESSION['recipient_last_name']);
$email = post_value_or('email', $_SESSION['email']);
$production_speed = post_value_or('production_speed', $_SESSION['production_speed']);
$address_line_1 = post_value_or("address_line_1", $_SESSION['address_line_1']);
$address_line_2 = post_value_or("address_line_2", $_SESSION['address_line_2']);
$zip_code = post_value_or("zip_code", $_SESSION['zip_code']);
$state = post_value_or("state", $_SESSION['state']);
$_POST['country'] = 'USA';
$country = post_value_or("country", $_SESSION['country']);
$payement_method = post_value_or("payment", $_SESSION['payment_method']);
$production_speedup_charge = intval($production_speed) . ".00";
if (isset($_POST["update"])) {
    $val = new validation;
    $val->addSource($_POST);
    $val->addRule('shipping_method', 'numeric', true, 0, 2, true, 'Shipping method name')
        ->addRule('name', 'string', true, 3, 250, true, 'Full name')
        ->addRule('email', 'email', true, 1, 50, true, 'Email')
        ->addRule('production_speed', 'numeric', true, 0, 999, true, 'Production speed')
        ->addRule('address_line_1', 'string', true, 5, 250, true, 'Address 1')
        ->addRule('address_line_2', 'string', false, 5, 250, true, 'Address 2')
        ->addRule('zip_code', 'string', true, 5, 5, true, 'Zip')
        ->addRule('state', 'string', true, 2, 2, true, 'State')
        ->addRule('country', 'string', true, 2, 3, true, 'Country')
        ->addRule('payment', 'string', true, 7, 8, true, 'Payment');
    $val->run();
    $errorMessage = $val->errors;    
    if (empty($errorMessage)) {
        $_SESSION['shipping_method'] = $shipping_method;
        $_SESSION['production_speed'] = $production_speed;
        $_SESSION['production_speedup_charge'] = $production_speedup_charge;
        $_SESSION['recipient_name'] = $name;
        $_SESSION['email'] = $email;
        $_SESSION['address_line_1'] = $address_line_1;
        $_SESSION['address_line_2'] = $address_line_2;
        $_SESSION['zip_code'] = $zip_code;
        $_SESSION['state'] = $state;
        $_SESSION['country'] = $country;
        $_SESSION['payment_method'] = $payement_method;
        header('Location: ./order_preview.php');
    }
}
?>
    <!DOCTYPE html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Florida Fakes</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Kevin Rajaram">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
    <script type="text/javascript" src="js/jquery.qtip.min.js"></script>
    <script type="text/javascript">
     $(document).ready(function()
     {
        $('input').qtip({
            show: 'focus',
            hide: 'blur',
            position: {
                at: 'bottom center',
                target: 'event'
            }
        });
     });
    </script>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/jquery.qtip.min.css">
    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/main.css">

    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
    <![endif]-->
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<section id="header">
    <div class="container">
        <div class="logo"><a href="index.php" title="Home"><img src="img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->
        <nav>
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="prices.php">Prices</a></li>
                <li><a href="media.php">Media</a></li>
                <li><a href="faq.php">FAQ</a></li>
                <li><a href="resellers.php">Resellers</a></li>
                <li class="active"><a href="order.php">Order</a></li>
            </ul>
        </nav>
    </div><!-- /container -->
</section><!-- #header -->

<section id="announcements">
    <div class="container">
        <h5 class="announce-icon"><strong>Announcements</strong></h5>
        <?php echo getContent('announcement'); ?>
    </div><!-- /container -->
</section><!-- #announcements -->

<section id="editItem">
    <div class="container">
        <h1>Edit Shipping Info</h1>
        <?php
        if (!empty($errorMessage)) {
            echo '<div id="errors">';
            foreach ($errorMessage as $error) {
                echo '<span class="error" style="color: red;">'.$error.'</span><br>';
            }
            echo '</div>';
        }
        ?>
        <form action="" method="post" name="shipping_info">
            <b>Shipping Method:</b> <font color="#FF0000"> * </font><br>
            <select name="shipping_method" class="input_select">
               <option value="0" selected="selected">&nbsp;</option>
               <option value="<?=shipping_speed1_cost?>" <?php if ($shipping_method == shipping_speed1_cost){echo "selected=\"selected\"";}?>><?=shipping_speed1_name?></option>
               <option value="<?=shipping_speed2_cost?>" <?php if ($shipping_method == shipping_speed2_cost){echo "selected=\"selected\"";}?>><?=shipping_speed2_name?></option>
            </select>
            <br>
            <b>Production Speed:</b> <font color="#FF0000"> * </font><br>
            <select name="production_speed" class="input_select">
                <option>&nbsp;</option>
                <option value="<?=production_speed1_cost?>" <?php if($production_speed == production_speed1_cost){echo "selected=\"selected\"";}?>><?=production_speed1_name?></option>
                <option value="<?=production_speed2_cost?>" <?php if($production_speed == production_speed2_cost){echo "selected=\"selected\"";}?>><?=production_speed2_name?></option>
            </select>
            <br>
            <b>Full Name</b><font color="#FF0000"> * </font><br>
            <input type="text" name="name" maxlength="50" value="<?=$name?>"  title="Full name for shipping">
            <br>
            <b>Email Address</b><font color="#FF0000"> * </font><br>
            <input type="text" name="email" maxlength="50" class="input_text" value="<?=$email?>" title="Email address for your account">
            <br>
            <b>Address Line 1 </b><font color="#FF0000"> * </font><br>
            <input type="text" name="address_line_1" maxlength="60" class="input_text" value="<?=$address_line_1?>" title="Shipping address line one"> <br>
            Address Line 2:<br>
            <input type="text" name="address_line_2" maxlength="60" class="input_text" value="<?=$address_line_2?>" title="Shipping address line two"> <br>
            <b>Zip Code</b><font color="#FF0000"> * </font><br>
            <input type="text" name="zip_code" maxlength="6" class="input_text" value="<?=$zip_code?>" title="Shipping zip code"> <br>
            <b>State</b><font color="#FF0000"> * </font><br>
            <?php if (isset($state)) {echo listUSStates($state);} else {echo listUSStates();}?>
            <br>
            <b>How would you like to pay</b><font color="#FF0000"> * </font><br>
            <select name="payment">
                <option></option>
                <option value="ReLoadIt" <?php if($payement_method == "ReLoadIt"){echo "selected=\"selected\"";}?>>ReloadIt</option>
                <option value="Bitcoin" <?php if($payement_method == "Bitcoin"){echo "selected=\"selected\"";}?>>Bitcoin</option>
            </select>
            <input name="update" class="button" style="width:500px" type="submit" value="Update"/>
        </form>
        <a href="./manage.php?order_id=<?php echo $row[0]['order_id']; ?>"><input class="button" style="width:500px" type="submit" value="Go Back"/></a>
    </div>
</section>


<section id="footer">
    <div class="container">
        <div class="half">
            <p class="large">FloridaFakes</p>
            <p style="padding-right:60px;">We're just a couple of kids that like to have fun. We know our IDs work because we use them ourselves. We know what it's like to look for a fake ID so we're hoping we can make the process easier for you!</p>
        </div>

        <div class="half">
            <ul class="footer-nav">
                <li><a href="prices.php">Prices</a></li>
                <li><a href="media.php">Media</a></li>
                <li><a href="faq.php">FAQ</a></li>
                <li><a href="resellers.php">Resellers</a></li>
                <li><a href="order.php">Order</a></li>
                <li><a href="contact.html">Contact Us</a></li>
            </ul>
        </div>
    </div><!-- /container -->
</section><!-- #footer -->
</body>
</html>
<?php ob_end_flush(); ?>