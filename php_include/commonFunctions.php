<?php
function create_connection() {
    global $conn;
    $conn = new PDO('mysql:host=localhost;dbname=' . db_name . ';charset=utf8', db_user, db_pass);
    return $conn;
}

function toState($id){
    global $stateInfo;
    foreach ($stateInfo as $state => $info) {
        if($id == $info['id']){
           return $info['name'];
        }
    }
    return "";
}

function getContent($area){
    $conn = create_connection();
    $selectContent = $conn->prepare("SELECT * FROM content WHERE area = :area LIMIT 1");
    $selectContent->execute(array(':area' => $area));
    $content = $selectContent->fetch(PDO::FETCH_ASSOC);
    return $content['content'];
}

function listHeight($setFeet='', $setInch=''){
	$feetArray = array('',4,5,6,7);
	$inchArray = array('',1,2,3,4,5,6,7,8,9,10,11);
	$output = 'Feet <select name="heightFeet">';
	foreach ($feetArray as $feet) {
		if($feet == $setFeet){
			$output.='<option value="'.$feet.'" SELECTED>'.$feet.'</option>';
		} else {
			$output.='<option value="'.$feet.'">'.$feet.'</option>';
		}
	}
	$output.="</select>";
	
	$output .= ' Inches <select name="heightInch">';
	foreach ($inchArray as $inch) {
		if($inch == $setInch){
			$output.='<option value="'.$inch.'" SELECTED>'.$inch.'</option>';
		} else {
			$output.='<option value="'.$inch.'">'.$inch.'</option>';
		}
	}
	$output.="</select>";
	return $output;
}

function date_dropdown($year_limit = 0, $setDay = '', $setMonth = '', $setYear = ''){
    /*days*/
    $html_output = '<select name="DOBDay" id="day_select">';
    if($setDay==''){$html_output .= '<option value="" SELECTED></option>';}
    for ($day = 1; $day <= 31; $day++) {
        if($day == $setDay){
            $html_output .= '<option value="'.$day.'" SELECTED>' . $day . '</option>';
        } else {
            $html_output .= '<option value="'.$day.'">' . $day . '</option>';
        }
    }
    $html_output .= '</select>'."\n";

    /*months*/
    $html_output .= '<select name="DOBMonth" id="month_select" >'."\n";
    if($setMonth==''){$html_output .= '<option value="" SELECTED></option>';}
    $months = array("", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    for ($month = 1; $month <= 12; $month++) {
        if($month == $setMonth){
            $html_output .= '<option value="' . $month . '" SELECTED>' . $months[$month] . '</option>';
        } else {
            $html_output .= '<option value="' . $month . '">' . $months[$month] . '</option>';
        }
    }
    $html_output .= '</select>';

    /*years*/
    $html_output .= '<select name="DOBYear" id="year_select">';
    if($setYear==''){$html_output .= '<option value="" SELECTED></option>';}
    for ($year = (date("Y") - 21); $year >= date("Y")-75; $year--) {
        if($year == $setYear){
            $html_output .= '<option value="'.$year.'" selected>' . $year . '</option>';
        } else {
            $html_output .= '<option value="'.$year.'">' . $year . '</option>';
        }
    }
    $html_output .= '</select>';
    return $html_output;
}

function post_value_or($key, $default = NULL) {
    return isset($_POST[$key]) && !empty($_POST[$key]) ? $_POST[$key] : $default;
}

function generate_photo_name($formID, $order_id, $first_name, $last_name) {
    $char_set = "0123456789";
    $str = substr(str_shuffle($char_set), 0, 3);
    return strtoupper($formID . $order_id . $first_name . $last_name . "-" . $str);
}

function toShippingMethod($shippingSpeed) {
   switch ($shippingSpeed) {
        case shipping_speed1_cost:
            return shipping_speed1_name;
            break;
        case shipping_speed2_cost:
            return shipping_speed2_name;
            break;
        default:
            return "";
    }
}

function toProductionSpeed($production_speed) {
   switch ($production_speed) {
        case production_speed1_cost:
            return production_speed1_name;
            break;
        case production_speed2_cost:
            return production_speed2_name;
            break;
        case production_speed3_cost:
            return production_speed3_name;
            break;
        default:
            return "";
    }
}

function generate_password_hash($password) {
    $cost = 10;
    $salt = passwordSalt;
    $salt = sprintf("$2a$%02d$", $cost) . $salt;
    $hash = crypt($password, $salt);
    return $hash;
}

function get_file_extension($file_name) {
    return substr(strrchr($file_name, '.'), 1);
}