<?php
require_once("php_include/imgUploader.class.php");
$orderID = $_GET['id'];
$conn = create_connection();
$row=$_SESSION['cart'][$orderID];
$first_name = post_value_or("first_name", $row['FName']);
$middle_name = post_value_or("middle_name", $row['MName']);
$last_name = post_value_or("last_name", $row['LName']);
$orderDate = getdate(strtotime($row['DOB']));
$DOBDay = post_value_or("DOBDay", $orderDate['mday']);
$DOBMonth = post_value_or("DOBMonth", $orderDate['month']);
$DOBYear = post_value_or("DOBYear", $orderDate['year']);
$address = post_value_or("address", $row['address']);
$city = post_value_or("city", $row['city']);
$state = post_value_or("state", $row['state']);
$zip = post_value_or("zip", $row['zip']);
$gender = post_value_or("gender", $row['gender']);
$weight = post_value_or("weight", $row['weight']);
$height = explode("-", $row['height']);
$heightFeet = post_value_or("heightFeet", $height[0]);
$heightInch = post_value_or("heightInch", $height[1]);
$eyeColor = post_value_or("eyeColor", $row['eye']);
$hairColor = post_value_or("hairColor", $row['hair']);
$quantity = post_value_or("number_of_shirts", $row['quanity']);
$image = upload_dir.$row["picture"];
$sigImage = upload_dir.$row['customSig'];
if((isset($_GET['source']))&&(isset($sources[$_GET['source']]))){$redirect=$sources[$_GET['source']];}else{$redirect="order_preview.php";}
if (isset($_POST['submit'])) {
    $val = new validation;
    $val->addSource($_POST);
    $val->addRule('first_name', 'string', true, 3, 50, true, 'First name')
        ->addRule('middle_name', 'string', false, 3, 50, true, 'Middle name')
        ->addRule('last_name', 'string', true, 3, 50, true, 'Last name')
        ->addRule('DOBDay', 'numeric', true, 1, 31, true, 'Day')
        ->addRule('DOBMonth', 'numeric', true, 1, 12, true, 'Month')
        ->addRule('DOBYear', 'numeric', true, date("Y")-75, date("Y")-21, true, 'Year')
        ->addRule('address', 'string', true, 5, 250, true, 'Address')
        ->addRule('city', 'string', true, 3, 150, true, 'City')
        ->addRule('state', 'issueState', true, 1, 15, true, 'State')
        ->addRule('zip', 'string', true, 5, 5, true, 'Zip')
        ->addRule('gender', 'string', false, 4, 6, true, 'Gender')
        ->addRule('weight', 'numeric', true, 75, 800, true, 'Weight')
        ->addRule('heightInch', 'numeric', true, 0, 12, true, 'Inch')
        ->addRule('heightFeet', 'numeric', true, 4, 7, true, 'Feet')
        ->addRule('eyeColor', 'string', true, 1, 15, true, 'Eye color')
        ->addRule('hairColor', 'string', true, 1, 15, true, 'Hair color')
        ->addRule('number_of_shirts', 'numeric', true, 1, 4, true, 'Number of shirts')
        ->addRule('customPrintedSignature', 'string', false, 3, 50, true, 'Custom Signature');
    $val->run();
    $errorMessage = $val->errors;
    if (empty($errorMessage)) {
        if (($_FILES["photo"]["error"] == 0) && ($_FILES["photo"]["error"] != 4)) {
            try{
                $img = new imgUploader($_FILES['photo']);
                $dir = upload_dir;
                $img->upload_unscaled($dir, substr($row['picture'], 0, -4));
                $photoError = $img->getError();
                if ($photoError != 0) {
                   throw new Exception($photoError);
                }
            } catch (Exception $e) {
                array_push($errorMessage, $e->getMessage());
            }
        }
    }

    if (empty($errorMessage)) {
        if (($_FILES["sigImage"]["error"] == 0) && ($_FILES["sigImage"]["error"] != 4)) {
            try{
                $img = new imgUploader($_FILES['sigImage']);
                $dir = upload_dir;
                $_SESSION['cart'][$orderID]['customSig'] = $img->upload_unscaled($dir, 'sig_'.$orderID);
                $sigError = $img->getError();
                if ($photoError != 0) {
                   throw new Exception($sigError);
                }
            } catch (Exception $e) {
                array_push($errorMessage, $e->getMessage());
            }
        }
    }

    if (empty($errorMessage)) {
        $_SESSION['cart'][$orderID]['FName'] = $first_name;
        $_SESSION['cart'][$orderID]['MName'] = $middle_name;
        $_SESSION['cart'][$orderID]['LName'] = $last_name;
        $_SESSION['cart'][$orderID]['DOB'] = $DOBYear.'-'.$DOBMonth.'-'.$DOBDay;
        $_SESSION['cart'][$orderID]['address'] = $address;
        $_SESSION['cart'][$orderID]['city'] = $city;
        $_SESSION['cart'][$orderID]['state'] = $state;
        $_SESSION['cart'][$orderID]['zip'] = $zip;
        $_SESSION['cart'][$orderID]['gender'] = $gender;
        $_SESSION['cart'][$orderID]['weight'] = $weight;
        $_SESSION['cart'][$orderID]['height'] = $heightFeet.'-'.$heightInch;
        $_SESSION['cart'][$orderID]['eye'] = $eyeColor;
        $_SESSION['cart'][$orderID]['hair'] = $hairColor;
        $_SESSION['cart'][$orderID]['quanity'] = $quantity;
        $_SESSION['cart'][$orderID]['printedSig'] = $printedSig;
        updateItemPrices();
        sleep(1);
        $key = array_search('vegetable', $sources); 
        header("Location: ./".$redirect);
        die();
    }
}
?>
<form action="" method="post" name="editItem" enctype="multipart/form-data">
    <?php
    if (!empty($errorMessage)) {
        echo '<div id="errors">';
        foreach ($errorMessage as $error) {
            echo '<span class="error" style="color: red;">'.$error.'</span><br>';
        }
        echo '</div>';
    }
    ?>
    First Name<font color="#ff0000">*</font><br>
    <input type="text" name="first_name" size="25" class="input_text" value="<?=$first_name?>" title="First name on ID">
    <br>
    Middle Name:<br>
    <input type="text" name="middle_name" size="25" class="input_text" value="<?=$middle_name?>" title="Middle name on ID">
    <br>
    Last Name<font color="#ff0000">*</font><br>
    <input type="text" name="last_name" size="25" class="input_text" value="<?=$last_name?>" title="Last name on ID">
    <br>
    Date of Birth<font color="#ff0000">*</font><br>
    <?=date_dropdown(21, $DOBDay, $DOBMonth, $DOBYear);?>
    <br>
    Address<font color="#ff0000">*</font><br>
    <input type="text" name="address" size="25" class="input_text" value="<?=$address?>" title="Address on ID">
    <br>
    City<font color="#ff0000">*</font><br>
    <input type="text" name="city" size="25" class="input_text" value="<?=$city?>" title="City on ID">
    <br>
    Issued state<font color="#ff0000">*</font><br>
    <select name="state" class="input_select">
        <option value="0" selected="selected">&nbsp;</option>
        <?php foreach($stateInfo as $stateID=>$info) { ?>
            <option value="<?=$info['id']?>" <?php if ($state == $info['id']){echo "selected=\"selected\"";}?>><?=$info['name']?></option>
        <?php } ?>
    </select>
    <br>
    Zip<font color="#ff0000">*</font><br>
    <input type="text" name="zip" size="25" class="input_text" value="<?=$zip?>" title="Zip code on ID">
    <br>
    Gender<font color="#ff0000">*</font><br>
    <select name="gender" class="input_select">
        <option value="" <?php if($gender == ""){echo "SELECTED";}?>></option>
        <option value="Male" <?php if($gender == "Male"){echo "SELECTED";}?>>Male</option>
        <option value="Female" <?php if($gender == "Female"){echo "SELECTED";}?>>Female</option>
    </select>
    <br>
    Weight (Lb)<font color="#ff0000">*</font><br>
    <select name="weight" class="input_select">
        <option value="" <?php if($weight == ""){echo "SELECTED";}?>></option>
        <?php 
        for ($i=90; $i < 350; $i+=10) {
            echo "<option value=\"".$i."\"".(($weight == $i)?" SELECTED":"").">".$i."</option>";
        }
        ?>
    </select>
    <br>
    Height<font color="#ff0000">*</font><br>
    <?=listHeight($heightFeet, $heightInch);?>
    <br>
    <b>Eye Color</b><font color="#ff0000">*</font><br>
    <select name="eyeColor">
        <option value="" <?php if($hairColor == ""){echo "SELECTED"; }?>></option>
        <?php
        foreach($stateInfo[0]['eyeColor'] as $fullEyeColor=>$value){
            echo "<option value=\"".$fullEyeColor."\"". ($eyeColor == $fullEyeColor ? " SELECTED":"").'>'.$fullEyeColor."</option>";
        }
        ?>
    </select>
    <br>
    <b>Hair Color</b><font color="#ff0000">*</font><br>
    <select name="hairColor">
        <option value="" <?php if($hairColor == ""){echo "SELECTED";}?>></option>
        <?php
        foreach($stateInfo[0]['hairColor'] as $fullHairColor=>$value){
            echo "<option value=\"".$fullHairColor."\"". ($hairColor == $fullHairColor ? " SELECTED":"").">".$fullHairColor."</option>";
        }
        ?>
    </select>
    <br>
    Back up copies?<font color="#ff0000"> ($50 Per Additional)</font><br>
    <select name="number_of_shirts">
        <option value="1" <?php if($quantity == 1){echo "SELECTED";}?>></option>
        <option value="2" <?php if($quantity == 2){echo "SELECTED";}?>>1</option>
        <option value="3" <?php if($quantity == 3){echo "SELECTED";}?>>2</option>
        <option value="4" <?php if($quantity == 4){echo "SELECTED";}?>>3</option>
    </select>
    <br>
    Custom Printed signature<font color="#ff0000"></font><br>
    <input class="input_text" placeholder="J Smith" name="customPrintedSignature" type="text" value="<?=$printedSig?>" title="Please enter the text for your custom computer generated signature"/>
    <br>
    ID Photo: <input type="file" name="photo"><br>
    <img src="<?=$image?>" width="375px"><br>
    <br>
    Signature image: <input type="file" name="sigImage"><br>
    <?php if ($sigImage != upload_dir) { ?>
        <img src="<?=$sigImage?>" width="375px"><br>
    <?php } ?>
    <br>
    <input name="submit" class="button" type="submit" style="width:500px" value="Update"/>
</form>
<a href="./<?=$redirect?>"><input class="button" type="submit" style="width:500px" value="Go Back"/></a>