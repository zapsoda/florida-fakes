<TABLE class="cart_table">
    <?php
    if (session_check() && isset($_SESSION['cart'])) {
        ?>
        <TR class="cart_table_title">
            <TD colspan="4" class="cart_table_title_td" style="font-size: 14px; font-weight: bold;">
                Current Order
            </TD>
        </TR>
        <TR align="center" valign="center">
            <th>Name</th>
            <th>Eye Color</th>
            <th>Weight</th>
            <th>Issue State</th>
            <th>Signature</th>            
            <th>Gender</th></th>            
            <th>Height</th>
            <th>Prints</th>
            <th>Total Price</th>
            <th>Edit</th>
        </TR>
        <?php foreach ($_SESSION['cart'] as $key => $cart_value) { ?>
            <TR align="center" valign="center">
                <TD style="color: black; width: 15%;">
                    <?= $_SESSION['cart'][$key]['FName'] . " " . $_SESSION['cart'][$key]['LName'] ?>
                </TD>
                <TD style="color: black; width: 5%;">
                    <?= $_SESSION['cart'][$key]['eye'] ?>  
                </TD>
                <TD style="color: black; width: 7%;">
                    <?= $_SESSION['cart'][$key]['weight']." Lb" ?>
                </TD>
                <TD style="color: black; width: 7%;">
                    <?= toState($_SESSION['cart'][$key]['state'])?>
                </TD>
                <TD style="color: black; width: 5%;">
                    <?php if($_SESSION['cart'][$key]['customSig']!=NULL){echo 'Y';}else{echo 'N';}?>
                </TD>
                <TD style="color: black; width: 10%;">
                    <?= $_SESSION['cart'][$key]['gender'] ?>
                </TD>
                <TD style="color: black; width: 10%;">
                    <?= $_SESSION['cart'][$key]['height'] ?>
                </TD>
                <TD style="color: black; width: 5%;">
                    <?= $_SESSION['cart'][$key]['quanity'] ?>
                </TD>
                <TD style="color: black; width: 5%;">                       
                    <?= "$" . $_SESSION['cart'][$key]['price'] ?>
                </TD>
                <TD style="color: black; width: 5%;">                       
                    <a href="./modify.php?id=<?=$key?>&source=<?=array_search(basename($_SERVER['REQUEST_URI']), $sources)?>">Edit</a>
                </TD>
            </TR>
        <?php } ?>
            <tr><td><a href="./logout.php?redirect=order.php">Clear Cart</a></td></tr>
    <?php } ?>
</TABLE>