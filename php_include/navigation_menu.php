<table style="border-collapse: collapse; width:100%; border: none; float: left;	margin: 0 auto; padding: 0px 0px 0px 0px;	border-spacing: 0; background-color: #262626;">
    <tbody>
        <tr style="border-collapse: collapse; width:100%; padding: 0px 0px 0px 0px; border-spacing: 0; border: none;">
            <td style="border-collapse: collapse; width:100%; padding: 0px 0px 0px 0px; border-spacing: 0; border: none;">
                <table class="navigation-menu">
                    <tbody>
                        <tr>
                            <td>
                                <a href="index.php">Home</a>
                            </td>
                            <td>
                                <a href="order_online.php">Order On-line</a>
                            </td>
                            <td>
                                <a href="track_order.php">Track Order</a>
                            </td>
                            <?php
                            if (isset($_SESSION['recipient_name'])) {
                                ?>
                                <td>
                                    <a href="logout.php">Logout</a>
                                </td>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
