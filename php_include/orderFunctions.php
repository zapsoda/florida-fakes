<?php
require_once('config.php');
require_once('commonFunctions.php');

function getAge( $dob, $tdate )
{
        $dob = new DateTime( $dob );
        $age = 0;
        $now = new DateTime( $tdate );
        while( $dob->add( new DateInterval('P1Y') ) < $now )
        {
                $age++;
        }
        return $age;
}

function listUSStates($key_selected = "") {
    $state_values = array(
        'AL' => "Alabama",
        'AK' => "Alaska",
        'AZ' => "Arizona",
        'AR' => "Arkansas",
        'CA' => "California",
        'CO' => "Colorado",
        'CT' => "Connecticut",
        'DE' => "Delaware",
        'DC' => "District Of Columbia",
        'FL' => "Florida",
        'GA' => "Georgia",
        'HI' => "Hawaii",
        'ID' => "Idaho",
        'IL' => "Illinois",
        'IN' => "Indiana",
        'IA' => "Iowa",
        'KS' => "Kansas",
        'KY' => "Kentucky",
        'LA' => "Louisiana",
        'ME' => "Maine",
        'MD' => "Maryland",
        'MA' => "Massachusetts",
        'MI' => "Michigan",
        'MN' => "Minnesota",
        'MS' => "Mississippi",
        'MO' => "Missouri",
        'MT' => "Montana",
        'NE' => "Nebraska",
        'NV' => "Nevada",
        'NH' => "New Hampshire",
        'NJ' => "New Jersey",
        'NM' => "New Mexico",
        'NY' => "New York",
        'NC' => "North Carolina",
        'ND' => "North Dakota",
        'OH' => "Ohio",
        'OK' => "Oklahoma",
        'OR' => "Oregon",
        'PA' => "Pennsylvania",
        'RI' => "Rhode Island",
        'SC' => "South Carolina",
        'SD' => "South Dakota",
        'TN' => "Tennessee",
        'TX' => "Texas",
        'UT' => "Utah",
        'VT' => "Vermont",
        'VA' => "Virginia",
        'WA' => "Washington",
        'WV' => "West Virginia",
        'WI' => "Wisconsin",
        'WY' => "Wyoming"
    );
    $string = "<select name=\"state\">";
    if (!empty($state_values)) {
        if (($key_selected == "") || (!isset($key_selected))) {
            $string.="<option value=\"\">Please select</option>\n";
        }
        foreach ($state_values as $state_short => $state_full) {
            if ($key_selected != "" && $key_selected == $state_short) {
                $additional = " SELECTED";
            } else {
                $additional = "";
            }
            $string.="<option value=\"" . $state_short . "\"" . $additional . ">" . $state_full . "</option>\n";
        }
        $string.="</select>\n";
        return $string;
    }
}

function first_session_check() {
    if (isset($_SESSION['timeout_idle'])) {
        if ($_SESSION['timeout_idle'] < time()) {
            unset($_SESSION['timeout_idle']);
            session_destroy();
            return false;
        }
    } else {
        $_SESSION['timeout_idle'] = time() + 1200;
        return true;
    }
    $_SESSION['timeout_idle'] = time() + 1200;
    return true;
}

function session_check() {
    if ((isset($_SESSION['timeout_idle']))&&(($_SESSION['type']=="order"))) {
        if ($_SESSION['timeout_idle'] < time()) {
            return false;
        }
    } else {
        return false;
    }
    $_SESSION['timeout_idle'] = time() + 1200;
    return true;
}

function create_order_session($user) {
    session_start();
    $_SESSION['email'] = $user;
    $_SESSION['type'] = "order";
    $_SESSION['page'] = 0;
    $_SESSION['timeout_idle'] = time() + 1200;
}

function logout($redirect = "index.php") {
    if (!session_check()) {
        session_start();
    }
    session_destroy();
    $_SESSION = array();
    var_dump($_SESSION);
    header('Location: ' . $redirect);
    sleep(2);
    exit();
}

function move_photo($oldName, $newName) {
    if (copy($_SERVER['DOCUMENT_ROOT'] . upload_dir . $oldName, $_SERVER['DOCUMENT_ROOT'] . upload_dir . $newName . "." . get_file_extension($oldName))) {
        return true;
    } else {
        return false;
    }
}

function updateItemPrices(){
    $totalQuantity = 0;
    $price_per_shirt = 0;
    foreach ($_SESSION['cart'] as $ID){
        $totalQuantity += $ID['quanity'];
    }
    if (($totalQuantity >= 1) && ($totalQuantity <= 4)) { //1-4 Shirts
        $price_per_shirt = bulkPrice1;
    } elseif (($totalQuantity >= 5) && ($totalQuantity <= 9)) { //5-9 shirts
        $price_per_shirt = bulkPrice2;
    } elseif ($totalQuantity >= 10) { //10 or more shirts
        $price_per_shirt = bulkPrice3;
    }
    foreach($_SESSION['cart'] as $key => $value) {
        $_SESSION['cart'][$key]['price'] = $price_per_shirt + ($_SESSION['cart'][$key]['quanity']-1)*50;
        $_SESSION['cart'][$key]['shirtPrice'] = $price_per_shirt;
        if($_SESSION['cart'][$key]['customSig']!=NULL){
            $_SESSION['cart'][$key]['price']+=50;
        }
    }
    return true;
}

function generate_person_id($first_name, $last_name) {
    $char_set = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = substr(str_shuffle($char_set), 0, 4);
    return $first_name . $last_name . date("mdy_h-i-sA", time()) . "_" . $str;
}

function generate_order_id($last_name) {
    $today = date("mdy");
    $char_set = "0123456789";
    $str = substr(str_shuffle($char_set), 0, 3);
    $order_id = $today . strtoupper($last_name) . $str;
    return $order_id;
}

function generate_password() {
    $char_set = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = substr(str_shuffle($char_set), 0, 8);
    return $str;
}

function send_order_confirmation($username, $password, $order_id) {
    require 'class.phpmailer.php';
    $content = "<html>";
    $content.="<head>";
    $content.="</head>";
    $content.="<body>";
    $content.="<p>Thank you for your order.";
    if ($password != "") {
        $content.="  Following is your login information.</p><br><br>";
        $content.="<p>Username&nbsp;:&nbsp;" . $username . "</p>";
        $content.="<p>Password&nbsp;:&nbsp;" . $password . "</p><br>";
    }
    $content.="<br>Your order number is <a href=\"http://".domain."/userPanel?order_id=" . $order_id . "\">" . $order_id . "</a> to submit payment for your order please login to your account at FloridaFakes.com/userPanel.";
    $content.="</body>";
    $content.="</html>";

    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->Debugoutput = 'html';
    $mail->Host = email_host;
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = email_user;
    $mail->Password = email_pass;
    $mail->SetFrom(email_user, 'Florida Fakes');
    $mail->AddReplyTo(email_user, 'Florida Fakes');
    $mail->AddAddress($username, $username);
    $mail->Subject = 'Florida Fakes Order Confirmation';
    $mail->IsHTML(true);
    $mail->Body = $content;
    if (!$mail->Send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    }
}

function add_user($username, $hash) {
    global $conn;
    if (user_exist($username)) {
        return false;
    }

    $stmt = $conn->prepare("insert into user(username, password) values (:user, :pass)");
    $stmt->execute(array(':user' => $username, ':pass' => $hash));
    return true;
}

function user_exist($username) {
    global $conn;
    $stmt = $conn->prepare("select * from user where username like :user limit 1");
    $stmt->execute(array(':user' => $username));
    $row_count = $stmt->rowCount();
    if ($row_count == 1) {
        return true;
    } else {
        return false;
    }
}

function check_password($username, $password) {
    global $conn;
    $stmt = $conn->prepare("select username, password from user where username like :user limit 1");
    $stmt->execute(array(':user' => $username));
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $cost = 10;
    $salt = passwordSalt;
    $salt = sprintf("$2a$%02d$", $cost) . $salt;
    $hash = crypt($password, $salt);

    if ((isset($row[0]['password']))&&($hash == $row[0]['password'])) {
        return true;
    } else {
        return false;
    }
}