<tr>
    <td style="padding: 0;">
        <table class="form_table" style="table-layout:fixed;">
            <tbody>
                <tr>
                    <th style="width: 3%; font-size: 11px; text-align: center; vertical-align: middle;">No.</th>
                    <th style="width: 10%; font-size: 11px; text-align: center; vertical-align: middle;">First Name</th>
                    <th style="width: 10%; font-size: 11px; text-align: center; vertical-align: middle;">Middle Name</th>
                    <th style="width: 10%; font-size: 11px; text-align: center; vertical-align: middle;">Last Name</th>
                    <th style="width: 10%; font-size: 11px; text-align: center; vertical-align: middle;">Gender</th>
                    <th style="width: 7%; font-size: 11px; text-align: center; vertical-align: middle;">Height</th>
                    <th style="width: 13%; font-size: 11px; text-align: center; vertical-align: middle;">No. of T-Shirts</th>
                    <th style="width: 10%; font-size: 11px; text-align: center; vertical-align: middle;">Picture</th>
                    <th style="width: 7%; font-size: 11px; text-align: center; vertical-align: middle;">Price</th>
                    <th style="width: 20%; font-size: 11px; text-align: center; vertical-align: middle;">Order Status</th>																										
                </tr>
                <?php
                $conn = create_connection();
                $count = 0;
                $stmt = $conn->prepare("select * from order_detail where order_id like :orderNum");
                $stmt->execute(array(':orderNum' => $order_id));
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $count+=1;
                    ?>
                    <tr>
                        <td style="width: 3%; font-size: 11px; text-align: center; vertical-align: middle;"><?= $count ?></td>												
                        <td style="width: 10%; font-size: 11px; text-align: center; vertical-align: middle; overflow:hidden; white-space:nowrap;"><?= $row['first_name'] ?></td>
                        <td style="width: 10%; font-size: 11px; text-align: center; vertical-align: middle; overflow:hidden; white-space:nowrap;"><?= $row['middle_name'] ?></td>
                        <td style="width: 10%; font-size: 11px; text-align: center; vertical-align: middle; overflow:hidden; white-space:nowrap;"><?= $row['last_name'] ?></td>
                        <td style="width: 10%; font-size: 11px; text-align: center; vertical-align: middle; overflow:hidden; white-space:nowrap;"><?= $row['gender'] ?></td>
                        <td style="width: 10%; font-size: 11px; text-align: center; vertical-align: middle; overflow:hidden; white-space:nowrap;"><?= $row['height'] ?></td>
                        <td style="width: 13%; font-size: 11px; text-align: center; vertical-align: middle; overflow:hidden; white-space:nowrap;"><?= $row['number_of_shirts'] ?></td>
                        <td style="width: 10%; font-size: 11px; text-align: center; vertical-align: middle; overflow:hidden; white-space:nowrap;"><img src="<?= upload_dir.$row['photo_name'] ?>" width="150px" max-height="150px" /></td>
                        <td style="width: 7%; font-size: 11px; text-align: center; vertical-align: middle; overflow:hidden; white-space:nowrap;"><?= "$".$row['totalItemPrice'] ?></td>
                        <td style="width: 20%; font-size: 11px; text-align: center; vertical-align: middle; overflow:hidden; white-space:nowrap;"><?= $row['item_status'] ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </td>
</tr>
<?php
$stmt = $conn->prepare("select * from shipping_info where order_id like :orderNum");
$stmt->execute(array(':orderNum' => $order_id));
$row = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<tr>
    <td style="padding: 0;">
        <table class="form_table">
            <tr>
                <th style="width: 40%">
                    Shipping Information
                </th>
                <th style="width: 40%">
                    &nbsp;&nbsp;
                </th>
                <th style="width: 10%">
                    &nbsp;&nbsp;
                </th>														
            </tr>
            <tr>
                <td style="width: 40%">
                    Shipping Method&nbsp;<font color="#FF0000">*</font>
                </td>
                <td style="width: 40%">
                    <?= toShippingMethod($row['0']['shipping_method']) ?>
                </td>
                <td style="width: 10%">
                    <label id="shipping_method_error" class="error_message_label">&nbsp;</label>
                </td>														
            </tr>									
            <tr>
                <td style="width: 40%">
                    Recipient's Name&nbsp;<font color="#FF0000">*</font>
                </td>
                <td style="width: 40%">
                    <?= $row['0']['recipient_name'] ?>
                </td>
                <td style="width: 10%">
                    <label id="recipient_name_error" class="error_message_label">&nbsp;</label>
                </td>														
            </tr>
            <tr>
                <td style="width: 40%">
                    Email Address&nbsp;<font color="#FF0000">*</font>
                </td>
                <td style="width: 40%">
                    <?= $row['0']['email'] ?>
                </td>
                <td style="width: 10%">
                    <label id="email_error" class="error_message_label">&nbsp;</label>
                </td>														
            </tr>
            <tr>
                <td style="width: 40%">
                    Address Line 1&nbsp;<font color="#FF0000">*</font>
                </td>
                <td style="width: 40%">
                    <?= $row['0']['address_line1'] ?>
                </td>
                <td style="width: 10%">
                    <label id="addr_line1_error" class="error_message_label">&nbsp;</label>
                </td>														
            </tr>
            <tr>
                <td style="width: 40%">
                    Address Line 2
                </td>
                <td style="width: 40%">
                    <?= $row['0']['address_line2'] ?>
                </td>
                <td style="width: 10%">
                    <label id="addr_line2_error" class="error_message_label">&nbsp;</label>
                </td>														
            </tr>
            <tr>
                <td style="width: 40%">
                    Zip Code&nbsp;<font color="#FF0000">*</font>
                </td>
                <td style="width: 40%">
                    <?= $row['0']['zip_code'] ?>
                </td>
                <td style="width: 10%">
                    <label id="zip_code_error" class="error_message_label">&nbsp;</label>
                </td>														
            </tr>
            <tr>
                <td style="width: 40%">
                    State&nbsp;<font color="#FF0000">*</font>
                </td>
                <td style="width: 40%">
                    <?= $row['0']['state'] ?>
                </td>
                <td style="width: 10%">
                    <label id="state_error" class="error_message_label">&nbsp;</label>
                </td>														
            </tr>
            <tr>
                <td style="width: 40%">
                    Country&nbsp;<font color="#FF0000">*</font>
                </td>
                <td style="width: 40%">
                    <?= $row['0']['country'] ?>
                </td>
                <td style="width: 10%">
                    <label id="country_error" class="error_message_label">&nbsp;</label>
                </td>														
            </tr>
        </table>
    </td>
</tr>