<?php
require_once("imgUploader.class.php");
$orderID = $_GET['order'];
$conn = create_connection();
$stmt = $conn->prepare("SELECT * FROM order_detail INNER JOIN `order_main` ON (`order_detail`.`order_id` = `order_main`.`order_id`) WHERE order_main.order_id LIKE ?");
$stmt->execute(array($orderID));
$row = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach($row AS $key=>$cartItem){if($cartItem['person_id']==$_GET['id']){$itemKey=$key;}}
$first_name = post_value_or("first_name", $row[$itemKey]['first_name']);
$middle_name = post_value_or("middle_name", $row[$itemKey]['middle_name']);
$last_name = post_value_or("last_name", $row[$itemKey]['last_name']);
$orderDate = getdate(strtotime($row[$itemKey]['DOB']));
$DOBDay = post_value_or("DOBDay", $orderDate['mday']);
$DOBMonth = post_value_or("DOBMonth", $orderDate['month']);
$DOBYear = post_value_or("DOBYear", $orderDate['year']);
$address = post_value_or("address", $row[$itemKey]['address']);
$city = post_value_or("city", $row[$itemKey]['city']);
$state = post_value_or("state", $row[$itemKey]['issueState']);
$zip = post_value_or("zip", $row[$itemKey]['zip']);
$gender = post_value_or("gender", $row[$itemKey]['gender']);
$weight = post_value_or("weight", $row[$itemKey]['weight']);
$height = explode("-", $row[$itemKey]['height']);
$heightFeet = post_value_or("heightFeet", $height[0]);
$heightInch = post_value_or("heightInch", $height[1]);
$eyeColor = post_value_or("eyeColor", $row[$itemKey]['eyeColor']);
$hairColor = post_value_or("hairColor", $row[$itemKey]['hairColor']);
$quantity = post_value_or("number_of_shirts", $row[$itemKey]['number_of_shirts']);
$image = upload_dir.$row[$itemKey]["photo_name"];
$sigImage = upload_dir.$row[$itemKey]["signatureName"];
if (isset($_POST['submit'])) {
    $val = new validation;
    $val->addSource($_POST);
    $val->addRule('first_name', 'string', true, 3, 50, true, 'First name')
        ->addRule('middle_name', 'string', false, 3, 50, true, 'Middle name')
        ->addRule('last_name', 'string', true, 3, 50, true, 'Last name')
        ->addRule('DOBDay', 'numeric', true, 1, 31, true, 'Day')
        ->addRule('DOBMonth', 'numeric', true, 1, 12, true, 'Month')
        ->addRule('DOBYear', 'numeric', true, date("Y")-75, date("Y")-21, true, 'Year')
        ->addRule('address', 'string', true, 5, 250, true, 'Address')
        ->addRule('city', 'string', true, 3, 150, true, 'City')
        ->addRule('state', 'issueState', true, 1, 15, true, 'State')
        ->addRule('zip', 'string', true, 5, 5, true, 'Zip')
        ->addRule('gender', 'string', false, 4, 6, true, 'Gender')
        ->addRule('weight', 'numeric', true, 90, 350, true, 'Weight')
        ->addRule('heightInch', 'numeric', true, 0, 12, true, 'Inch')
        ->addRule('heightFeet', 'numeric', true, 4, 7, true, 'Feet')
        ->addRule('eyeColor', 'string', true, 1, 15, true, 'Eye color')
        ->addRule('hairColor', 'string', true, 1, 15, true, 'Hair color')
        ->addRule('number_of_shirts', 'numeric', true, 1, 4, true, 'Number of shirts')
        ->addRule('customPrintedSignature', 'string', false, 3, 50, true, 'Custom Signature');
    $val->run();
    $errorMessage = $val->errors;
    $person_id = generate_photo_name($state, $orderID, $first_name, $last_name);
    if (empty($errorMessage)) {
        if (($_FILES["photo"]["error"] == 0) && ($_FILES["photo"]["error"] != 4)) {
            try{
                $img = new imgUploader($_FILES['photo']);
                $dir = upload_dir;
                $IDPictureName = $img->upload_unscaled($dir, $person_id);
                $photoError = $img->getError();
                if ($photoError != 0) {
                   throw new Exception($photoError);
                }
            } catch (Exception $e) {
                array_push($errorMessage, $e->getMessage());
            }
        } else {$IDPictureName = $row[$itemKey]["photo_name"];}
    }

    if (empty($errorMessage)) {
        if (($_FILES["sigImage"]["error"] == 0) && ($_FILES["sigImage"]["error"] != 4)) {
            try{
                $img = new imgUploader($_FILES['sigImage']);
                $dir = upload_dir;
                $customSigImage = $img->upload_unscaled($dir, 'sig_'.$person_id);
                $sigError = $img->getError();
                if ($photoError != 0) {
                   throw new Exception($sigError);
                }
            } catch (Exception $e) {
                array_push($errorMessage, $e->getMessage());
            }
        } else {$customSigImage = $row[$itemKey]["signatureName"];}
    }

    if (empty($errorMessage)) {
        //Set just the order number
        $stmt = $conn->prepare("UPDATE order_detail SET number_of_shirts=:number_of_shirts WHERE person_id=:personID");
        $stmt->execute(array(':number_of_shirts'=>$quantity, ':personID'=>$row[$itemKey]['person_id']));

        //With new order number refetch data
        $stmt = $conn->prepare("SELECT * FROM order_detail WHERE order_id LIKE ?");
        $stmt->execute(array($orderID));
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        updateItemPrices();
        $stmt = $conn->prepare("UPDATE order_detail SET first_name=:first_name, middle_name=:middle_name, last_name=:last_name, DOB=:DOB, address=:address, city=:city, issueState=:state, zip=:zip, gender=:gender, weight=:weight, height=:height, eyeColor=:eyeColor, hairColor=:hairColor, photo_name=:IDPictureName, signatureName=:signatureName WHERE person_id=:personID");
        $stmt->execute(array(':first_name'=>$first_name, ':middle_name'=>$middle_name, ':last_name'=>$last_name, ':DOB'=>$DOBYear."-".$DOBMonth."-".$DOBDay, ':address'=>$address, ':city'=>$city, ':state'=>$state, ':zip'=>$zip, ':gender'=>$gender, ':weight'=>$weight, ':height'=>$height[0]."-".$height[1], ':eyeColor'=>$eyeColor, ':hairColor'=>$hairColor, ':IDPictureName'=>$IDPictureName, ':signatureName'=>$customSigImage, ':personID'=>$row[$itemKey]['person_id']));
        sleep(1);
        header("Location: ./manage.php?order_id=".$row[$itemKey]['order_id']);
    }
}
?>
<form action="" method="post" name="editItem" enctype="multipart/form-data">
    <?php
    if (!empty($errorMessage)) {
        echo '<div id="errors">';
        foreach ($errorMessage as $error) {
            echo '<span class="error" style="color: red;">'.$error.'</span><br>';
        }
        echo '</div>';
    }
    ?>
    First Name<font color="#ff0000">*</font><br>
    <input type="text" name="first_name" size="25" class="input_text" value="<?=$first_name?>" title="First name on ID">
    <br>
    Middle Name:<br>
    <input type="text" name="middle_name" size="25" class="input_text" value="<?=$middle_name?>" title="Middle name on ID">
    <br>
    Last Name<font color="#ff0000">*</font><br>
    <input type="text" name="last_name" size="25" class="input_text" value="<?=$last_name?>" title="Last name on ID">
    <br>
    Date of Birth<font color="#ff0000">*</font><br>
    <?=date_dropdown(21, $DOBDay, $DOBMonth, $DOBYear);?>
    <br>
    Address<font color="#ff0000">*</font><br>
    <input type="text" name="address" size="25" class="input_text" value="<?=$address?>" title="Street address on ID">
    <br>
    City<font color="#ff0000">*</font><br>
    <input type="text" name="city" size="25" class="input_text" value="<?=$city?>" title="City on ID">
    <br>
    Issued state<font color="#ff0000">*</font><br>
    <select name="state" class="input_select">
        <option value="0" selected="selected">&nbsp;</option>
        <?php foreach($stateInfo as $stateID=>$info) { ?>
            <option value="<?=$info['id']?>" <?php if ($state == $info['id']){echo "selected=\"selected\"";}?>><?=$info['name']?></option>
        <?php } ?>
    </select>
    <br>
    Zip<font color="#ff0000">*</font><br>
    <input type="text" name="zip" size="25" class="input_text" value="<?=$zip?>" title="Zipcode on ID">
    <br>
    Gender<font color="#ff0000">*</font><br>
    <select name="gender" class="input_select">
        <option value="" <?php if($gender == ""){echo "SELECTED";}?>></option>
        <option value="Male" <?php if($gender == "Male"){echo "SELECTED";}?>>Male</option>
        <option value="Female" <?php if($gender == "Female"){echo "SELECTED";}?>>Female</option>
    </select>
    <br>
    Weight (Lb)<font color="#ff0000">*</font><br>
    <select name="weight" class="input_select">
        <option value="" <?php if($weight == ""){echo "SELECTED";}?>></option>
        <?php 
        for ($i=90; $i < 350; $i+=10) {
            echo "<option value=\"".$i."\"".(($weight == $i)?" SELECTED":"").">".$i."</option>";
        }
        ?>
    </select>
    <br>
    Height<font color="#ff0000">*</font><br>
    <?=listHeight($heightFeet, $heightInch);?>
    <br>
    <b>Eye Color</b><font color="#ff0000">*</font><br>
    <select name="eyeColor">
        <option value="" <?php if($hairColor == ""){echo "SELECTED"; }?>></option>
        <?php
        foreach($stateInfo[0]['eyeColor'] as $fullEyeColor=>$value){
            echo "<option value=\"".$fullEyeColor."\"". ($eyeColor == $fullEyeColor ? " SELECTED":"").'>'.$fullEyeColor."</option>";
        }
        ?>
    </select>
    <br>
    <b>Hair Color</b><font color="#ff0000">*</font><br>
    <select name="hairColor">
        <option value="" <?php if($hairColor == ""){echo "SELECTED";}?>></option>
        <?php
        foreach($stateInfo[0]['hairColor'] as $fullHairColor=>$value){
            echo "<option value=\"".$fullHairColor."\"". ($hairColor == $fullHairColor ? " SELECTED":"").">".$fullHairColor."</option>";
        }
        ?>
    </select>
    <br>
    Back up copies?<font color="#ff0000"> ($50 Per Additional)</font><br>
    <select name="number_of_shirts">
        <option value="1" <?php if($quantity == 1){echo "SELECTED";}?>></option>
        <option value="2" <?php if($quantity == 2){echo "SELECTED";}?>>1</option>
        <option value="3" <?php if($quantity == 3){echo "SELECTED";}?>>2</option>
        <option value="4" <?php if($quantity == 4){echo "SELECTED";}?>>3</option>
    </select>
    <br>
    ID Photo: <input type="file" name="photo"><br>
    <img src="<?=$image?>" width="375px"><br>
    <br>
    Signature image: <input type="file" name="sigImage"><br>
    <?php if ($sigImage != upload_dir) { ?>
        <img src="<?=$sigImage?>" width="375px"><br>
    <?php } ?>
    <br>
    <input name="submit" class="button" type="submit" style="width:500px" value="Update"/>
    </form>
    <a href="./manage.php?order_id=<?php echo $row[$itemKey]['order_id']; ?>"><input class="button" type="submit" style="width:500px" value="Go Back"/></a>
</body>