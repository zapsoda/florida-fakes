<?php 
$selectShipping = $conn->prepare("SELECT shipping_method FROM shipping_info WHERE order_id LIKE :orderNum LIMIT 1");
$selectShipping->execute(array(':orderNum' => $orderid));
$shipping = $selectShipping->fetch(PDO::FETCH_ASSOC);

$selectItems = $conn->prepare("SELECT * FROM order_detail WHERE order_id LIKE :orderNum");
$selectItems->execute(array(':orderNum' => $orderid));
$items = $selectItems->fetchAll(PDO::FETCH_ASSOC);
$total_price = money_format('%i', 0);

$tempArray = array();
foreach($items as $item){
    $total_price += money_format('%i', $item['totalItemPrice']);
    array_push($tempArray, $item['issueState']);
}
$uniqueStateCount = count(array_unique($tempArray));
if($shipping['shipping_method']==2){$shippingCharge=money_format('%i', 20*$uniqueStateCount);} else {$shippingCharge = money_format('%i', 0.00);}

$total_price += money_format('%i', $shippingCharge + $order['production_speedup_charge']);

if($paymentMethod=="ReLoadIt"){ ?>
    <table style="width:500px;">
        <tr>
            <td>Type (first 4 digits)</td>
            <td>Confirmed</td>
            <td>Unconfirmed</td>
            <td>Both</td>
            <td>Edit</td>
        </tr>
        <?php
        $stmt = $conn->prepare("SELECT * FROM moneypak WHERE order_id LIKE :orderNum");
        $stmt->execute(array(':orderNum' => $orderid));
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $reloadit_both = money_format('%i', 0.00);
        $reloadit_unconfirmed = money_format('%i', 0.00);
        $reloadit_confirmed = money_format('%i', 0.00);
        foreach($row as $reloadit){$reloadit_both+=$reloadit['amount'];?>
            <tr>
                <td><?=$reloadit['type']?> (<?=substr($reloadit['code'], -4, 4)?>)</td>
                <?php if($reloadit['confirmed']==0){$reloadit_unconfirmed += $reloadit['amount'];?>
                    <td>$0.00</td>
                    <td>$<?=$reloadit['amount'];?></td>
                    <td>$<?=$reloadit['amount'];?></td>
                <?php } else {$reloadit_confirmed += $reloadit['amount']; ?>
                    <td>$<?=$reloadit['amount'];?></td>
                    <td>$0.00</td>
                    <td>$<?=$reloadit['amount'];?></td>
                <?php } ?>
                <td><?php if(!$reloadit['confirmed']){?><a href="payment.php?id=<?=$_GET['id']?>&MP=<?=$reloadit['id']?>&action=modify">Edit</a><br><a href="payment.php?id=<?=$_GET['id']?>&MP=<?=$reloadit['id']?>&action=delete">Delete</a><?php } ?></td>
            </tr>
        <?php } ?>
        <tr>
            <td>Total Price</td>
            <td>$<?=money_format('%i', $total_price);?></td>
            <td>$<?=money_format('%i', $total_price);?></td>
            <td>$<?=money_format('%i', $total_price);?></td>
            <td></td>
        </tr>
        <tr>
            <td>Total Owed</td>
            <td>$<?=money_format('%i', $total_price-$reloadit_confirmed);?></td>
            <td>$<?=money_format('%i', $total_price-$reloadit_unconfirmed);?></td>
            <td>$<?=money_format('%i', $total_price-$reloadit_both);?></td>
            <td></td>
        </tr>
    </table>
<?php 
} else { 
    $btc_confirmed = sprintf("%.8f",JSONtoAmount(file_get_contents("https://blockchain.info/q/getreceivedbyaddress/".$order['btc_address']."?confirmations=".confirms)));
    $btc_unconfirmed = number_format(unconfirmed($order['btc_address']), 8, '.', '');
    $btc_both = sprintf("%.8f",JSONtoAmount(file_get_contents("https://blockchain.info/q/getreceivedbyaddress/".$order['btc_address']."?confirmations=0")));
?>                    
    <table style="width:500px;">
        <tr>
            <td></td>
            <td>Confirmed</td>
            <td>Unconfirmed</td>
            <td>Both</td>
        </tr>
        <tr>
            <td>Bitcoin</td>
            <td><?=$btc_confirmed?></td>
            <td><?=$btc_unconfirmed?></td>
            <td><?=$btc_both?></td>
        </tr>
        <tr>
            <td>Total Price (USD)</td>
            <td>$<?=$total_price;?></td>
            <td>$<?=$total_price;?></td>
            <td>$<?=$total_price;?></td>
        </tr>
        <tr>
            <td>Total Price (BTC)</td>
            <td><?=file_get_contents("https://blockchain.info/tobtc?currency=USD&value=".$total_price);?></td>
            <td><?=file_get_contents("https://blockchain.info/tobtc?currency=USD&value=".$total_price);?></td>
            <td><?=file_get_contents("https://blockchain.info/tobtc?currency=USD&value=".$total_price);?></td>
        </tr>
        <tr>
            <td>Total Owed (BTC)</td>
            <td><?=file_get_contents("https://blockchain.info/tobtc?currency=USD&value=".$total_price)-$btc_confirmed;?></td>
            <td><?=file_get_contents("https://blockchain.info/tobtc?currency=USD&value=".$total_price)-$btc_unconfirmed;?></td>
            <td><?=file_get_contents("https://blockchain.info/tobtc?currency=USD&value=".$total_price)-$btc_both;?></td>
        </tr>
    </table>
    Bitcoin Address: <?=$order['btc_address'];?>
<?php } ?>
<br>
<a href="./manage.php?order_id=<?= $order['order_id']; ?>"><input class="button" type="submit" value="Go Back" style="width:500px;"/></a>
<br>