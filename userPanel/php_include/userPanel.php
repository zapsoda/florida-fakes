<tr>
    <td style="padding: 0;">
        <table class="form_table" style="table-layout:fixed;">
            <tbody>
                <tr>
                    <th width="10%">ORDER ID</th>
                    <th width="10%">QUANTITY</th>
                    <th width="10%">ORDER STATUS</th>
                    <th width="10%">PAYMENT</th>
                    <th width="10%">ORDER DATE</th>
                    <th width="10%">TOTAL</th>
                    <th width="10%">DETAILS</th>
                </tr>
                <?php
                $count = 0;
                $selectOrders = $conn->prepare("SELECT * FROM order_main where email=:email");
                $selectOrders->execute(array(':email' => $_SESSION['username']));
                while ($orders = $selectOrders->fetch(PDO::FETCH_ASSOC)) {
                    $selectItems =$conn->prepare("SELECT * FROM order_detail WHERE order_id LIKE ?");
                    $selectItems->execute(array($orders['order_id']));    
                    $items = $selectItems->fetchAll(PDO::FETCH_ASSOC);
                    if($items!=NUll){
                        $count+=1;
                        $totalPrice = 0;
                        $totalIDs = 0;
                        $tempArray = array();
                        foreach($items as $item){
                            array_push($tempArray, $item['issueState']);
                            $totalPrice += $item['totalItemPrice'];
                            $totalIDs += $item['number_of_shirts'];
                        }
                        $uniqueStateCount = count(array_unique($tempArray));
                        $getShipping_info = $conn->prepare('SELECT shipping_method FROM shipping_info WHERE order_id=? LIMIT 1');
                        $getShipping_info->execute(array($orders['order_id']));
                        $shipping_info = $getShipping_info->fetch(PDO::FETCH_ASSOC);
                        if($shipping_info['shipping_method']==2){$shippingCharge=number_format(20*$uniqueStateCount, 2, '.', '');} else {$shippingCharge = "0.00";}
                        ?>
                        <tr>										
                            <td><?= $orders['order_id'] ?></td>
                            <td><?= $totalIDs ?></td>
                            <td><?= $orders['order_status'] ?></td>
                            <td><?= $orders['paymentMethod'] ?></td>
                            <td><?= date("F j, Y ", strtotime($orders['date_time'])) ?></td>
                            <td><?= "$".number_format( number_format($totalPrice, 2, '.', '') +  $orders['production_speedup_charge'] + number_format($shippingCharge),2,'.',''); ?></td>
                            <td>
                                <a href="?order_id=<?= $orders['order_id'] ?>">View</a><br>
                                <a href="?order_id=<?= $orders['order_id'] ?>&action=delete">Delete</a>
                            </td>
                        </tr>
                <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </td>
</tr>