<?php
session_start();
require_once('../config.php');
require_once('../php_include/commonFunctions.php');

if ((stripos($_SERVER["SCRIPT_NAME"], "userPanel/login.php") === false) && (stripos($_SERVER["SCRIPT_NAME"], "userPanel/index.php") === false)) {
    if (!check_user_session()) {
        header('Location: login.php');
        die();
    }
}

function create_user_session($username) {
    $_SESSION['timeout_idle'] = time() + 1200;
    $_SESSION['username'] = $username;
    $_SESSION['type'] = "user";
    $_SESSION['token'] = session_token;
    return true;
}

function check_user_session() {
    if (isset($_SESSION['token']) && ($_SESSION['token'] == session_token) && ($_SESSION['type'] == "user")) {
        if (isset($_SESSION['username'])) {
            if (isset($_SESSION['timeout_idle']) && ($_SESSION['timeout_idle'] > time())) {
                $_SESSION['timeout_idle'] = time() + 1200;
                return true;
            }
        }
        $_SESSION=array();
        session_destroy();
    }
    return false;
}

function generate_password() {
    $char_set = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = substr(str_shuffle($char_set), 0, 8);
    return $str;
}

function updateItemPrices($row=NULL){
    global $conn;
    if($row == NULL){
        $selectItemsTable = $conn->query('SELECT * FROM order_detail');
        $row = $selectItemsTable->fetchAll(PDO::FETCH_ASSOC);
    }
    $totalQuantity = array();
    $total = array();
    $price_per_shirt = array();
    foreach ($row as $items){
        if(!isset($totalQuantity[$items['order_id']])){
            $totalQuantity[$items['order_id']] = 0;
        }
        $totalQuantity[$items['order_id']] += $items['number_of_shirts'];
    }
    foreach ($row as $items){
        if (($totalQuantity[$items['order_id']] >= 1) && ($totalQuantity[$items['order_id']] <= 4)) { //1-4 Shirts
            $price_per_shirt[$items['order_id']] = bulkPrice1;
        } elseif (($totalQuantity[$items['order_id']] >= 5) && ($totalQuantity[$items['order_id']] <= 9)) { //5-9 shirts
            $price_per_shirt[$items['order_id']] = bulkPrice2;
        } elseif ($totalQuantity[$items['order_id']] >= 10) { //10 or more shirts
            $price_per_shirt[$items['order_id']] = bulkPrice3;
        }
    }
    foreach($row as $value) {
        $total[$value['order_id']] = $price_per_shirt[$value['order_id']] + ($value['number_of_shirts']-1)*50;
        $updateTotals = $conn->prepare('UPDATE order_detail SET totalItemPrice=:total, price_per_shirt=:pricePerShirt WHERE person_id=:personID');
        $updateTotals->execute(array(':total'=>$total[$value['order_id']], ':pricePerShirt'=>$price_per_shirt[$value['order_id']], ':personID'=>$value['person_id']));
        if(!empty($value['signatureName'])){
            $total[$value['order_id']] += 50;
            $updateTotals = $conn->prepare('UPDATE order_detail SET totalItemPrice=:total WHERE person_id=:personID');
            $updateTotals->execute(array(':total'=>$total[$value['order_id']], ':personID'=>$value['person_id']));
        }
    }
    return true;
}

function add_user($username, $password) {
    global $conn;
    if (user_exist($username)) {
        return false;
    }
    $cost = 10;
    $salt = passwordSalt;
    $salt = sprintf("$2a$%02d$", $cost) . $salt;
    $password_hash = crypt($password, $salt);
    $stmt = $conn->prepare("insert into user(username, password) values (:user, :pass)");
    $stmt->execute(array(':user' => $username, ':pass' => $password_hash));
    return true;
}

function checkIfOrderHasItems($orderNumber) {
    global $conn;
    $stmt = $conn->prepare("SELECT * FROM order_main INNER JOIN order_detail ON (order_main.order_id = order_detail.order_id) WHERE order_detail.order_id LIKE :order_number AND order_main.order_id LIKE :order_number LIMIT 1;");
    $stmt->execute(array(':order_number' => $orderNumber));
    $row_count = $stmt->rowCount();
    if ($row_count == 1) {
        return true;
    } else {
        return false;
    }
}

function JSONtoAmount($value) {
        return bcdiv($value, 100000000, 8);
}

function unconfirmed($addy){
    $confirmed = file_get_contents("https://blockchain.info/q/getreceivedbyaddress/".$addy."?confirmations=".confirms);
    $unconfirmed = file_get_contents("https://blockchain.info/q/getreceivedbyaddress/".$addy."?confirmations=0");
    $unconfirmed = $unconfirmed - $confirmed;
    if ($unconfirmed > 0){
        $string = sprintf("%.8f",JSONtoAmount($unconfirmed));
        return $string;
    }
    return floatval("0.00000000");
}

function MPBelongsToUser($RowID){
    global $conn;
    return true;
    $getMoneyPaks = $conn->prepare('SELECT * FROM moneypak WHERE id=? LIMIT 1');
    $getMoneyPaks->execute(array($RowID));
    if($getMoneyPaks->rowCount() != 1){return false;}
    $moneypaks = $getMoneyPaks->fetch(PDO::FETCH_ASSOC);
    checkUserOrder($moneypaks['order_id']);
}

function checkUserOrder($orderNumber) {
    global $conn;
    $stmt = $conn->prepare("SELECT * FROM order_detail INNER JOIN order_main ON (order_detail.order_id = order_main.order_id) WHERE order_main.order_id like :order_number AND order_main.email like :user limit 1");
    $stmt->execute(array(':order_number' => $orderNumber, ':user' => $_SESSION['username']));
    if ($stmt->rowCount() == 1) {
        return true;
    } else {
        return false;
    }
}

function orderExist($orderNumber) {
    global $conn;
    $stmt = $conn->prepare("select * FROM order_detail WHERE order_id like :order_number LIMIT 1");
    $stmt->execute(array(':order_number' => $orderNumber));
    $row_count = $stmt->rowCount();
    if ($row_count == 1) {
        return true;
    } else {
        return false;
    }
}

function checkUserItem($personID) {
    global $conn;
    $stmt = $conn->prepare("SELECT * FROM order_detail INNER JOIN order_main ON (order_detail.order_id = order_main.order_id) WHERE person_id like :person_id AND order_main.email like :user limit 1");
    $stmt->execute(array(':person_id' => $personID, ':user' => $_SESSION['username']));
    $row_count = $stmt->rowCount();
    if ($row_count == 1) {
        return true;
    } else {
        return false;
    }
}

function user_exist($username) {
    global $conn;
    $stmt = $conn->prepare("select * from user where username like :user limit 1");
    $stmt->execute(array(':user' => $username));
    $row_count = $stmt->rowCount();
    if ($row_count == 1) {
        return true;
    } else {
        return false;
    }
}

function check_user_login($username, $password) {
    global $conn;
    $stmt = $conn->prepare("select username, password from user where username like :user limit 1");
    $stmt->execute(array(':user' => $username));
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $cost = 10;
    $salt = passwordSalt;
    $salt = sprintf("$2a$%02d$", $cost) . $salt;
    $hash = crypt($password, $salt);
    if (!user_exist($username)) {
        return false;
    }
    if ($hash == $row['0']['password']) {
        return true;
    } else {
        return false;
    }
}