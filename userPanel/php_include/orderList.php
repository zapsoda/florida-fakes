<tr>
    <td style="padding: 0;">
        <table class="form_table" style="table-layout:fixed;width:100%;">
            <tbody>
                <?php
                $confirmed = 0;
                $count = 0;
                $selectDetails = $conn->prepare('SELECT * FROM order_detail INNER JOIN order_main ON (order_detail.order_id = order_main.order_id) INNER JOIN shipping_info ON (order_detail.order_id = shipping_info.order_id) WHERE order_main.order_id LIKE :orderNum');
                $selectDetails->execute(array(':orderNum' => $order_id));
                $order_detail = $selectDetails->fetchAll(PDO::FETCH_ASSOC);
                if (isset($row[0])) {
                    ?>

                    <tr>
                        <th>Name</th>
                        <th>Birth date</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Zip</th>
                        <th>Gender</th>
                        <th>Weight</th>
                        <th>Height</th>
                        <th>Eye Color</th>
                        <th>#</th>
                        <th width="110px">Picture</th>
                        <th width="110px">Signature</th>
                        <th>Price</th>
                        <th>DETAILS</th>
                    </tr>
                    <?php
                    $total = 0;
                    $productionCharge = 0;
                    $shippingCharge = 0;
                    foreach ($order_detail as $cell) {
                    ?>
                        <tr>
                            <td><?=$cell['first_name']." ".$cell['last_name'];?></td>
                            <td><?=$cell['DOB'];?></td>
                            <td><?=$cell['address'];?></td>
                            <td><?=$cell['city'];?></td>
                            <td><?=toState($cell['issueState']);?></td>
                            <td><?=$cell['zip'];?></td>
                            <td><?=$cell['gender'];?></td>
                            <td><?=$cell['weight'];?></td>
                            <td><?=$cell['height'];?></td>
                            <td><?=$cell['eyeColor'];?></td>
                            <td><?=$cell['number_of_shirts'];?></td>
                            <td><img src="<?= upload_dir.$cell['photo_name'] ?>" width="100px" max-height="150px"/></td>
                            <td><?php if($cell['signatureName']!=NULL){?><img src="<?= upload_dir.$cell['signatureName'] ?>" width="100px" max-height="50px"/><?php } ?></td>
                            <td><?="$".number_format($cell['totalItemPrice'], 2, '.', '');?></td>
                            <td>
                                <a href="editOrder.php?id=<?= $cell['person_id']; ?>&order=<?= $order_id?>&action=modify">Modify<a><br>
                                <a href="editOrder.php?id=<?= $cell['person_id']; ?>&order=<?= $order_id?>&action=delete">Delete<a>
                            </td>
                        </tr>
                <?php
                        $total += $cell['totalItemPrice'];
                        $productionCharge = $cell['production_speedup_charge'];

                    }
                } else {  ?>
                    <tr>
                        <td>
                            No items found, Please contact us or order again.
                        </td>
                    </tr>
                <?php 
                }
                $tempArray = array();
                foreach ($order_detail as $key => $value) {
                    array_push($tempArray, $value['issueState']);
                }
                $uniqueStateCount = count(array_unique($tempArray));
                if($cell['shipping_method']==2){$shippingCharge=number_format(20*$uniqueStateCount, 2, '.', '');} else {$shippingCharge = "0.00";}
                ?>
                <tr>
                    <td colspan="12"></td>
                    <td style="width: 13%; font-size: 11px; text-align: right; vertical-align: middle;"><b>Sub Total&nbsp;:&nbsp;</b></td>
                    <td style="width: 7%; font-size: 11px; text-align: center; vertical-align: middle;"><?= "$" . number_format($total, 2, '.', '') ?></td>
                </tr>
                <tr>
                    <td colspan="12"></td>
                    <td style="width: 13%; font-size: 11px; text-align: right; vertical-align: middle;"><b>Prod Charge&nbsp;:&nbsp;</b></td>
                    <td style="width: 7%; font-size: 11px; text-align: center; vertical-align: middle;"><?= "$" . $productionCharge ?></td>
                </tr>
                <tr>
                    <td colspan="12"></td>
                    <td style="width: 13%; font-size: 11px; text-align: right; vertical-align: middle;"><b>Shipping Charge:&nbsp;</b></td>
                    <td style="width: 7%; font-size: 11px; text-align: center; vertical-align: middle;"><?= "$" . $shippingCharge ?></td>
                </tr>
                <tr>
                    <td colspan="12"></td>
                    <td style="width: 13%; font-size: 11px; text-align: right; vertical-align: middle;"><b>Total Price&nbsp;:&nbsp;</b></td>
                    <td style="width: 7%; font-size: 11px; text-align: center; vertical-align: middle;"><?= "$" . number_format( number_format($total, 2, '.', '') +  $productionCharge + number_format($shippingCharge),2,'.',''); ?></td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>
<tr>
    <td style="padding: 0;">
        <table style="text-align: left;">
            <tr>
                <th colspan="3">
                    Shipping Information
                </th>											
            </tr>
            <?php
            $selectShipping = $conn->prepare("SELECT * FROM shipping_info INNER JOIN order_main ON order_main.order_id = shipping_info.order_id WHERE order_main.order_id like :orderNum LIMIT 1");
            $selectShipping->execute(array(':orderNum' => $order_id));
            $shippingInfo = $selectShipping->fetchAll(PDO::FETCH_ASSOC);
            if (isset($shippingInfo[0])) { ?>
                <tr>
                    <td>
                        Shipping Method&nbsp;<font color="#FF0000">*</font>
                    </td>
                    <td>
                        <?=toShippingMethod($shippingInfo['0']['shipping_method']);?>
                    </td>													
                </tr>	
                <tr>
                    <td>
                        Production Speed&nbsp;<font color="#FF0000">*</font>
                    </td>
                    <td>
                        <?=toProductionSpeed($shippingInfo['0']['production_speed']);?>
                    </td>													
                </tr>	
                <tr>
                    <td>
                        Recipient's Name&nbsp;<font color="#FF0000">*</font>
                    </td>
                    <td>
                        <?= $shippingInfo['0']['recipient_name'] ?>
                    </td>												
                </tr>
                <tr>
                    <td>
                        Email Address&nbsp;<font color="#FF0000">*</font>
                    </td>
                    <td>
                        <?= $shippingInfo['0']['email'] ?>
                    </td>											
                </tr>
                <tr>
                    <td>
                        Address Line 1&nbsp;<font color="#FF0000">*</font>
                    </td>
                    <td>
                        <?= $shippingInfo['0']['address_line1'] ?>
                    </td>											
                </tr>
                <tr>
                    <td>
                        Address Line 2
                    </td>
                    <td>
                        <?= $shippingInfo['0']['address_line2'] ?>
                    </td>										
                </tr>
                <tr>
                    <td>
                        Zip Code&nbsp;<font color="#FF0000">*</font>
                    </td>
                    <td>
                        <?= $shippingInfo['0']['zip_code'] ?>
                    </td>													
                </tr>
                <tr>
                    <td>
                        State&nbsp;<font color="#FF0000">*</font>
                    </td>
                    <td>
                        <?= $shippingInfo['0']['state'] ?>
                    </td>										
                </tr>
                <tr>
                    <td>
                        Country&nbsp;<font color="#FF0000">*</font>
                    </td>
                    <td>
                        <?= $shippingInfo['0']['country'] ?>
                    </td>									
                </tr>
            <?php } else { ?>
                <tr>
                    <td colspan="3">
                        No shipping info found, Please contact us or try again.
                    </td>
                </tr>
            <?php } ?>
            <?php if ($confirmed == 0) { ?>
                <tr>
                    <td colspan="3">
                        <a href="./editShipping.php?id=<?= $row[0]['order_id'] ?>">Modify shipping information</a>
                    </td>											
                </tr>
            <?php } ?>
        </table>
    </td>
</tr>
<?php
$selectPayment = $conn->prepare("SELECT paymentMethod FROM order_main WHERE order_id LIKE :orderNum LIMIT 1");
$selectPayment->execute(array(':orderNum' => $order_id));
$payment = $selectPayment->fetch(PDO::FETCH_ASSOC);
?>
<tr>
    <td style="padding: 0;">
        <table class="form_table">
            <tr>
                <th colspan="3">
                    Payment Information
                </th>											
            </tr>
            <?php if (isset($payment)) { ?>
                <tr>
                    <td>
                        Payment Method&nbsp;<font color="#FF0000">*</font>
                    </td>
                    <td>
                        <?= $payment['paymentMethod'] ?>
                    </td>
                    <td>
                        <label id="country_error" class="error_message_label">&nbsp;</label>
                    </td>														
                </tr>
            <?php } else { ?>
                <tr>
                    <th colspan="3">
                        No payment info found, Please contact us or try again.
                    </th>											
                </tr> 
            <?php } ?>
            <tr>
                <td colspan="3">
                    <a href="./payment.php?id=<?= $order_detail[0]['order_id'] ?>">Edit or add Payment</a>
                </td>											
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="2"><a href="./manage.php"><input class="button" type="submit" value="Go Back" style="width:500px"/></a></td>
</tr>