<?php
require_once("php_include/userPanelFunctions.php");
session_start();
if (isset($_REQUEST['order_id'])) {
    $conn = create_connection();
    if (orderExist($_GET['order_id'])) {
        $order_id = $_GET['order_id'];
    } else {
        if(check_user_session()){
            header('Location: manage.php');
        }
        $errorMessage = "Invalid order ID.";
    }
} else {
    if(check_user_session()){
        header('Location: manage.php');
    }
    $errorMessage = "Please enter a order ID.";
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Florida Fakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Kevin Rajaram">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/normalize.min.css">
        <link rel="stylesheet" href="../css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="header">
            <div class="container">
                <div class="logo"><a href="../index.php" title="Home"><img src="../img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->
                <nav>
                    <ul>
                        <?php if (check_user_session()) {echo "<li><a href=\"../logout.php\">Logout</a></li>";}?>
                        <li><a href="../index.php">Home</a></li>
                        <li><a href="../prices.php">Prices</a></li>
                        <li><a href="../media.php">Media</a></li>
                        <li><a href="../faq.php">FAQ</a></li>
                        <li><a href="../resellers.php">Resellers</a></li>
                        <li class="active"><a href="../order.php">Order</a></li>
                    </ul>
                </nav>
            </div><!-- /container -->
        </section><!-- #header -->
        
        <section id="announcements">
            <div class="container">
                <h5 class="announce-icon"><strong>Announcements</strong></h5>
                <?php echo getContent('announcement'); ?>
            </div><!-- /container -->
        </section><!-- #announcements -->
        
        <section id="manage">
            <div class="container">
                <?php if (isset($errorMessage)) {echo $errorMessage."<br>";}?>
                <h1>FIND YOUR ORDERS</h1>
                <?php
                if (isset($order_id)) {include_once("php_include/orderDetails.php");}
                require_once("php_include/enterOrderID.php");
                ?>
            </div>
        </section>

        <section id="footer">
            <div class="container">
                <div class="half">
                    <p class="large">FloridaFakes</p>
                    <p style="padding-right:60px;">We're just a couple of kids that like to have fun. We know our IDs work because we use them ourselves. We know what it's like to look for a fake ID so we're hoping we can make the process easier for you!</p>
                </div>

                <div class="half">
                    <ul class="footer-nav">
                        <li><a href="../prices.php">Prices</a></li>
                        <li><a href="../media.php">Media</a></li>
                        <li><a href="../faq.php">FAQ</a></li>
                        <li><a href="../resellers.php">Resellers</a></li>
                        <li><a href="../order.php">Order</a></li>
                        <li><a href="../contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div><!-- /container -->
        </section><!-- #footer -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="js/main.js"></script>
    </body>
</html>