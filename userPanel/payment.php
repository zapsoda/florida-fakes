<?php
require_once("php_include/userPanelFunctions.php");
require_once("../php_include/validate.php");
session_start();
$conn = create_connection();
$errorMessage = array();

if ((!check_user_session())||(!isset($_GET['id']))||(!checkUserOrder($_GET['id']))) {
  header('Location: ./login.php');
  die();
}
$orderid = $_GET['id'];

if ((isset($_GET['action'])) && (isset($_GET['MP']))) {
    if (MPBelongsToUser($_GET['MP'])) {
        $getMoneyPaks = $conn->prepare('SELECT * FROM moneypak WHERE id=? LIMIT 1');
        $getMoneyPaks->execute(array($_GET['MP']));
        $moneypaks = $getMoneyPaks->fetch(PDO::FETCH_ASSOC);
        $type = post_value_or('type', $moneypaks['type']);
        $ReLoadItNumber = post_value_or('reloadit', $moneypaks['code']);
        $amount = post_value_or('amount', $moneypaks['amount']);

        if ($_GET['action']=="delete") {
            $deleteMP = $conn->prepare('DELETE FROM moneypak WHERE id=?');
            $deleteMP->execute(array($_GET['MP']));
            header("Location: ./payment.php?id=".$orderid);
        }

        if($_GET['action']=="modify"){
            $_POST['type'] = $type;
            if (isset($_POST["modifyNumber"])) {
                $val = new validation;
                $val->addSource($_POST);
                $val->addRule('type', 'string', true, 7, 8, true, 'Type')
                    ->addRule('amount', 'string', true, 1, 50, true, 'Amount');
                if($type=="Moneypak"){
                    $val->addRule('reloadit', 'string', true, 14, 14, true, 'Code');
                }else{
                    $val->addRule('reloadit', 'string', true, 10, 10, true, 'Code');
                }
                $val->run();
                $errorMessage = array_merge($val->errors, $errorMessage);
                if (empty($errorMessage)) {
                    $updateMP = $conn->prepare("UPDATE moneypak SET CODE=?, amount=? WHERE id=?");
                    $updateMP->execute(array($ReLoadItNumber, $amount, $_GET['MP']));
                    header('Location: ./payment.php?id='.$orderid);
                }
            }
        }
    }
}

$stmt = $conn->prepare("SELECT * FROM order_main WHERE order_main.order_id LIKE :orderNum");
$stmt->execute(array(':orderNum' => $orderid));
$order = $stmt->fetch(PDO::FETCH_ASSOC);

$paymentMethod = $order['paymentMethod'];
if($paymentMethod=="ReLoadIt"){
    $newPaymentMethod = "Bitcoin";
} else {
    $newPaymentMethod = "ReLoadIt";
}
if (isset($_POST["switchPayment"])) {
    $paymentMethod = $newPaymentMethod;
    $stmt = $conn->prepare("UPDATE order_main SET paymentMethod=? WHERE order_id=?"); 
    $stmt->execute(array($paymentMethod, $orderid));
    header('Location: ./payment.php?id='.$orderid);
}
if (isset($_POST["addNumber"])) {
    $ReLoadItNumber = $_POST['reloadit'];
    $amount = $_POST["amount"];
    $val = new validation;
    $val->addSource($_POST);
    $val->addRule('type', 'string', true, 7, 8, true, 'Type')
        ->addRule('amount', 'string', true, 1, 50, true, 'Amount');
    if ($_POST['type']=='Moneypak') {
        $type='Moneypak';
        $val->addRule('reloadit', 'string', true, 14, 14, true, 'Code');
    } else {
        $type='Reloadit';
        $val->addRule('reloadit', 'string', true, 10, 10, true, 'Code');
    }
    $val->run();
    $errorMessage = array_merge($val->errors, $errorMessage);
    $findMP = $conn->prepare("SELECT id FROM moneypak WHERE code=? LIMIT 1");
    $findMP->execute(array($ReLoadItNumber));
    $mp = $findMP->fetch(PDO::FETCH_ASSOC);
    if (!empty($mp)) {
        array_push($errorMessage, "Moneypak already submited, Try another");
    }
    if (empty($errorMessage)) {
        $stmt = $conn->prepare("INSERT INTO moneypak(code, amount, order_id, type) VALUE(?, ?, ?, ?)"); 
        $stmt->execute(array($ReLoadItNumber, $amount, $orderid, ucwords($type)));
        array_push($errorMessage, $type." added");
        unset($type);
        unset($ReLoadItNumber);
        unset($amount);
        header('Location: ./payment.php?id='.$orderid);
    }
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Florida Fakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Kevin Rajaram">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/normalize.min.css">
        <link rel="stylesheet" href="../css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="header">
            <div class="container">
                <div class="logo"><a href="../index.php" title="Home"><img src="../img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->
                <nav>
                    <ul>
                        <?php if (check_user_session()) {echo "<li><a href=\"../logout.php\">Logout</a></li>";}?>
                        <li><a href="../index.php">Home</a></li>
                        <li><a href="../prices.php">Prices</a></li>
                        <li><a href="../media.php">Media</a></li>
                        <li><a href="../faq.php">FAQ</a></li>
                        <li><a href="../resellers.php">Resellers</a></li>
                        <li class="active"><a href="../order.php">Order</a></li>
                    </ul>
                </nav>
            </div><!-- /container -->
        </section><!-- #header -->
        
        <section id="announcements">
            <div class="container">
                <h5 class="announce-icon"><strong>Announcements</strong></h5>
                <?php echo getContent('announcement'); ?>
            </div><!-- /container -->
        </section><!-- #announcements -->
        
        <section id="manage">
            <div class="container">
                <h1>Payment</h1>
                <div id="payment">
                    <?php
                    if (!empty($errorMessage)) {
                        echo '<div id="errors">';
                        foreach ($errorMessage as $error) {
                            echo '<span class="error" style="color: red;">'.$error.'</span><br>';
                        }
                        echo '</div>';
                    }
                    ?>
                    <form action="" method="post">
                        Your current payment method is <b><?=$paymentMethod;?></b><br> <input type="submit" style="width:500px;" name="switchPayment" class="button" value="Switch payment to <?=$newPaymentMethod?>"><br>
                    </form>
                    <?php if($paymentMethod=="ReLoadIt"){ ?>
                        <form action="" method="post">
                            Number: <br><input type="text" name="reloadit" value="<?= $ReLoadItNumber ?>"><br>
                            Amount: <br><input type="text" name="amount" value="<?= $amount ?>"><br>
                            <?php if($_GET['action']!='modify'){?>
                                Type: <br><select name="type"><option value=""></option><option value="Moneypak" <?php if ($type=='Moneypak') {echo "SELECTED";} ?>>MoneyPak</option><option value="Reloadit" <?php if ($type=='Reloadit') {echo "SELECTED";} ?>>ReLoadIt</option></select>
                                <br>
                            <?php } ?>
                            <br>
                            <input type="submit" value="<?=($_GET['action']=='modify' ? "Modify Number" : "Add Number")?>" class="button" style="width:500px;" name="<?=($_GET['action']=='modify' ? "modifyNumber" : "addNumber")?>"><br>
                        </form>
                        <?php if($_GET['action']=='modify'){?><a href="./payment.php?id=<?=$orderid?>"><input type="submit" value="Add New Number" class="button" style="width:500px;"></a><br><br><?php } ?>
                    <?php } ?>
                </div>
                <div id="paymentInfo">
                    <?php
                    include "php_include/giftCards.php";
                    ?>
                </div>
                <div style="clear:both;"></div>
            </div>
        </section>

        <section id="footer">
            <div class="container">
                <div class="half">
                    <p class="large">FloridaFakes</p>
                    <p style="padding-right:60px;">We're just a couple of kids that like to have fun. We know our IDs work because we use them ourselves. We know what it's like to look for a fake ID so we're hoping we can make the process easier for you!</p>
                </div>

                <div class="half">
                    <ul class="footer-nav">
                        <li><a href="../prices.php">Prices</a></li>
                        <li><a href="../media.php">Media</a></li>
                        <li><a href="../faq.php">FAQ</a></li>
                        <li><a href="../resellers.php">Resellers</a></li>
                        <li><a href="../order.php">Order</a></li>
                        <li><a href="../contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div><!-- /container -->
        </section><!-- #footer -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="js/main.js"></script>
    </body>
</html>