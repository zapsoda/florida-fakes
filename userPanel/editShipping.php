<?php
require_once("php_include/userPanelFunctions.php");
require_once("../php_include/validate.php");
session_start();
$conn = create_connection();
if ((!check_user_session())||(!isset($_GET['id']))||(!checkUserOrder($_GET['id']))) {
  header('Location: ./login.php');
  die();
}
$stmt = $conn->prepare("SELECT * FROM shipping_info INNER JOIN order_main ON (shipping_info.order_id = order_main.order_id) WHERE order_main.order_id LIKE :orderNum");
$stmt->execute(array(':orderNum' => $_GET['id']));
$order = $stmt->fetch(PDO::FETCH_ASSOC);
$shipping_method = post_value_or('shipping_method', $order['shipping_method']);
$name = post_value_or('name', $order['recipient_name']);
$production_speed = post_value_or('production_speed', $order['production_speed']);
$address_line_1 = post_value_or("address_line_1", $order['address_line1']);
$address_line_2 = post_value_or("address_line_2", $order['address_line2']);
$zip_code = post_value_or("zip_code", $order['zip_code']);
$state = post_value_or("state", $order['state']);
$_POST['country'] = 'USA';
$country = post_value_or("country", $order['country']);
$production_speedup_charge = intval($production_speed) . ".00";
if (isset($_POST["update"])) {
    $val = new validation;
    $val->addSource($_POST);
    $val->addRule('shipping_method', 'numeric', true, 0, 2, true, 'Shipping method name')
        ->addRule('name', 'string', true, 7, 250, true, 'First name')
        ->addRule('production_speed', 'numeric', true, 0, 999, true, 'Production speed')
        ->addRule('address_line_1', 'string', true, 5, 250, true, 'Address 1')
        ->addRule('address_line_2', 'string', false, 5, 250, true, 'Address 2')
        ->addRule('zip_code', 'string', true, 5, 5, true, 'Zip')
        ->addRule('state', 'string', true, 2, 2, true, 'State')
        ->addRule('country', 'string', true, 2, 3, true, 'Country');
    $val->run();
    $errorMessage = $val->errors;    
    if (empty($errorMessage)) {
        $stmt = $conn->prepare("UPDATE shipping_info INNER JOIN order_main ON order_main.order_id = shipping_info.order_id SET recipient_name=:name, shipping_method=:shippingMethod, order_main.production_speed=:productionSpeed, address_line1=:addy1, address_line2=:addy2, zip_code=:zip, state=:state, country=:country WHERE order_main.order_id=:order_id"); 
        $stmt->execute(array(':name' => $name, ':shippingMethod' => $shipping_method, ':productionSpeed' => $production_speed, ':addy1' => $address_line_1, ':addy2' => $address_line_2, ':zip' => $zip_code, ':state' => $state, ':country' => $country, ':order_id'=>$_GET['id']));
        header('Location: ./manage.php?order_id='.$_GET['id']);
    }
}
?>
    <!DOCTYPE html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Florida Fakes</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Kevin Rajaram">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
    <script type="text/javascript" src="../js/jquery.qtip.min.js"></script>
    <script type="text/javascript">
     $(document).ready(function()
     {
        $('input').qtip({
            show: 'focus',
            hide: 'blur',
            position: {
                at: 'bottom center',
                target: 'event'
            }
        });
     });
    </script>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/normalize.min.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/jquery.qtip.min.css">

    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
    <![endif]-->
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<section id="header">
    <div class="container">
        <div class="logo"><a href="../index.php" title="Home"><img src="../img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->
        <nav>
            <ul>
                        <?php if (check_user_session()) {echo "<li><a href=\"../logout.php\">Logout</a></li>";}?>
                <li><a href="../index.php">Home</a></li>
                <li><a href="../prices.php">Prices</a></li>
                <li><a href="../media.php">Media</a></li>
                <li><a href="../faq.php">FAQ</a></li>
                <li><a href="../resellers.php">Resellers</a></li>
                <li class="active"><a href="../order.php">Order</a></li>
            </ul>
        </nav>
    </div><!-- /container -->
</section><!-- #header -->

<section id="announcements">
    <div class="container">
        <h5 class="announce-icon"><strong>Announcements</strong></h5>
        <?php echo getContent('announcement'); ?>
    </div><!-- /container -->
</section><!-- #announcements -->

<section id="editItem">
    <div class="container">
        <h1>Edit Shipping Info</h1>
        <?php
        if (!empty($errorMessage)) {
            echo '<div id="errors">';
            foreach ($errorMessage as $error) {
                echo '<span class="error" style="color: red;">'.$error.'</span><br>';
            }
            echo '</div>';
        }
        ?>
        <form action="" method="post" name="shipping_info">
            Shipping Method: <font color="#FF0000"> * </font><br>
            <select name="shipping_method" class="input_select">
                   <option value="0" selected="selected">&nbsp;</option>
                   <option value="<?=shipping_speed1_cost?>" <?php if ($shipping_method == shipping_speed1_cost){echo "selected=\"selected\"";}?>><?=shipping_speed1_name?></option>
                   <option value="<?=shipping_speed2_cost?>" <?php if ($shipping_method == shipping_speed2_cost){echo "selected=\"selected\"";}?>><?=shipping_speed2_name?></option>
            </select>
            <br>
            Production Speed: <font color="#FF0000"> * </font><br>
            <select name="production_speed" class="input_select">
                <option>&nbsp;</option>
                <option value="<?=production_speed1_cost?>" <?php if($production_speed == production_speed1_cost){echo "selected=\"selected\"";}?>><?=production_speed1_name?></option>
                <option value="<?=production_speed2_cost?>" <?php if($production_speed == production_speed2_cost){echo "selected=\"selected\"";}?>><?=production_speed2_name?></option>
            </select>
            <br>
            Full Name<font color="#FF0000"> * </font><br>
            <input type="text" name="name" maxlength="50" value="<?=$name?>" title="Full name for shipping">
            <br>
            Address Line 1 <font color="#FF0000"> * </font><br>
            <input type="text" name="address_line_1" maxlength="60" class="input_text" value="<?=$address_line_1?>" title="Shipping address line one"> <br>
            Address Line 2:<br>
            <input type="text" name="address_line_2" maxlength="60" class="input_text" value="<?=$address_line_2?>" title="Shipping address line two"> <br>
            Zip Code<font color="#FF0000"> * </font><br>
            <input type="text" name="zip_code" maxlength="6" class="input_text" value="<?=$zip_code?>" title="Shipping zip code"> <br>
            State<font color="#FF0000"> * </font><br>
            <?php if (isset($state)) {echo listUSStates($state);} else {echo listUSStates();}?>
            <br>
            <input name="update" class="button" style="width:500px" type="submit" value="Update"/>
        </form>
        <a href="./manage.php?order_id=<?php echo $order['order_id']; ?>"><input class="button" style="width:500px" type="submit" value="Go Back"/></a>
    </div>
</section>


<section id="footer">
    <div class="container">
        <div class="half">
            <p class="large">FloridaFakes</p>
            <p style="padding-right:60px;">We're just a couple of kids that like to have fun. We know our IDs work because we use them ourselves. We know what it's like to look for a fake ID so we're hoping we can make the process easier for you!</p>
        </div>

        <div class="half">
            <ul class="footer-nav">
                <li><a href="../prices.php">Prices</a></li>
                <li><a href="../media.php">Media</a></li>
                <li><a href="../faq.php">FAQ</a></li>
                <li><a href="../resellers.php">Resellers</a></li>
                <li><a href="../order.php">Order</a></li>
                <li><a href="../contact.html">Contact Us</a></li>
            </ul>
        </div>
    </div><!-- /container -->
</section><!-- #footer -->
</body>
</html>
<?php
function listUSStates($key_selected = "") {
    $state_values = array(
        'AL' => "Alabama",
        'AK' => "Alaska",
        'AZ' => "Arizona",
        'AR' => "Arkansas",
        'CA' => "California",
        'CO' => "Colorado",
        'CT' => "Connecticut",
        'DE' => "Delaware",
        'DC' => "District Of Columbia",
        'FL' => "Florida",
        'GA' => "Georgia",
        'HI' => "Hawaii",
        'ID' => "Idaho",
        'IL' => "Illinois",
        'IN' => "Indiana",
        'IA' => "Iowa",
        'KS' => "Kansas",
        'KY' => "Kentucky",
        'LA' => "Louisiana",
        'ME' => "Maine",
        'MD' => "Maryland",
        'MA' => "Massachusetts",
        'MI' => "Michigan",
        'MN' => "Minnesota",
        'MS' => "Mississippi",
        'MO' => "Missouri",
        'MT' => "Montana",
        'NE' => "Nebraska",
        'NV' => "Nevada",
        'NH' => "New Hampshire",
        'NJ' => "New Jersey",
        'NM' => "New Mexico",
        'NY' => "New York",
        'NC' => "North Carolina",
        'ND' => "North Dakota",
        'OH' => "Ohio",
        'OK' => "Oklahoma",
        'OR' => "Oregon",
        'PA' => "Pennsylvania",
        'RI' => "Rhode Island",
        'SC' => "South Carolina",
        'SD' => "South Dakota",
        'TN' => "Tennessee",
        'TX' => "Texas",
        'UT' => "Utah",
        'VT' => "Vermont",
        'VA' => "Virginia",
        'WA' => "Washington",
        'WV' => "West Virginia",
        'WI' => "Wisconsin",
        'WY' => "Wyoming"
    );
    $string = "<select name=\"state\">";
    if (!empty($state_values)) {
        if (($key_selected == "") || (!isset($key_selected))) {
            $string.="<option value=\"\">Please select</option>\n";
        }
        foreach ($state_values as $state_short => $state_full) {
            if ($key_selected != "" && $key_selected == $state_short) {
                $additional = " SELECTED";
            } else {
                $additional = "";
            }
            $string.="<option value=\"" . $state_short . "\"" . $additional . ">" . $state_full . "</option>\n";
        }
        $string.="</select>\n";
        return $string;
    }
}
?>