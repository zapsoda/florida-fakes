<?php
if ($_GET['redirect']) {
    $redirect = $_GET['redirect'];
} else {
    $redirect = "index.php";
}
session_start();
session_destroy();
$_SESSION = array();
header('Location: ' . $redirect);
die();