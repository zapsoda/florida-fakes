<?php
if ((stripos($_SERVER["SCRIPT_NAME"], "admin/index.php") === false) && (stripos($_SERVER["SCRIPT_NAME"], "admin/addAdmin.php") === false)) {
    if (!check_admin_session()) {
        header('Location: ../index.php');
        die();
    }
}
$extraPath = "";

if(stripos($_SERVER["SCRIPT_NAME"], "php_include")){$extraPath = "../";}
require_once($extraPath.'../config.php');
require_once($extraPath.'../php_include/commonFunctions.php');

function create_admin_session($username) {
    session_start();
    $_SESSION['timeout_idle'] = time() + 1200;
    $_SESSION['username'] = $username;
    $_SESSION['type'] = 'admin';
    $_SESSION['token'] = session_token;
    return true;
}

function AbrvEyes($eyeColor, $state){
    global $stateInfo;
    foreach($stateInfo as $stateID=>$info) {
        if($info['id']==$state){
            foreach ($info['eyeColor'] as $color=>$colorAbr) {
                if($eyeColor == $color){return $colorAbr;}
            }
        }
    }
    return $eyeColor;
}

function AbrvHair($hairColor, $state){
    global $stateInfo;
    foreach($stateInfo as $stateID=>$info) {
        if($info['id']==$state){
            foreach ($info['hairColor'] as $color=>$colorAbr) {
                if($hairColor == $color){return $colorAbr;}
            }
        }
    }
    return $hairColor;
}

function check_admin_session() {
    session_start();    
    if (isset($_SESSION['username'])) {
        if (isset($_SESSION['timeout_idle']) && ($_SESSION['timeout_idle'] > time())) {
            //if ((isset($_SESSION['token'])) && ($_SESSION['token'] == session_token)) {
                if($_SESSION['type'] == 'admin'){
                    //echo "Token: ".$_SESSION['token'];
                    $_SESSION['timeout_idle'] = time() + 1200;
                    return true;
                }
            //}
        }
    }
    $_SESSION = array();
    session_destroy();
    return false;
}

function generate_password() {
    $char_set = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = substr(str_shuffle($char_set), 0, 8);
    return $str;
}

function add_admin($username, $password) {
    $conn = create_connection();
    if (admin_exist($username)) {
        return false;
    }
    $cost = 10;
    $salt = passwordSalt;
    $salt = sprintf("$2a$%02d$", $cost) . $salt;
    $password_hash = crypt($password, $salt);
    $stmt = $conn->prepare("insert into admin(username, password) values (:user, :pass)");
    $stmt->execute(array(':user' => $username, ':pass' => $password_hash));
    return true;
}

function admin_exist($username) {
    $conn = create_connection();
    $stmt = $conn->prepare("select * from admin where username like :user limit 1");
    $stmt->execute(array(':user' => $username));
    $row_count = $stmt->rowCount();
    if ($row_count == 1) {
        return true;
    } else {
        return false;
    }
}

function check_admin_login($username, $password) {
    $conn = create_connection();
    $stmt = $conn->prepare("SELECT username, password FROM admin WHERE username LIKE :user limit 1");
    $stmt->execute(array(':user' => $username));
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $cost = 10;
    $salt = passwordSalt;
    $salt = sprintf("$2a$%02d$", $cost) . $salt;
    $hash = crypt($password, $salt);

    if (!admin_exist($username)) {
        return false;
    }
    if ($hash == $row['0']['password']) {
        return true;
    } else {
        return false;
    }
}
