<?php
require_once("adminFunctions.php");
if (!check_admin_session()) {
    header('Location: index.php');
    die();
} else {
    require_once("db_functions.php");
}
?>
<thead>
    <tr>
        <th><input type="text" name="search_ID" class="filter search_init" /></th>
        <th><input type="text" name="search_FName" class="filter search_init" /></th>
        <th><input type="text" name="search_MName" class="filter search_init" /></th>
        <th><input type="text" name="search_LName" class="filter search_init" /></th>
        <th>
            <select name="search_Height" class="filter search_init">
                <option value=""></option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
        </th>
        <th><input type="text" name="search_Height" class="filter search_init" /></th>
        <th></th>
        <th><input type="text" name="search_NumShirts" class="filter search_init" /></th>
        <th><input type="text" name="search_TotPrice" class="filter search_init" /></th>
        <th>
            <select name="search_Status" class="filter search_init">
                <option value=""></option>
                <option value="PENDING">PENDING</option>
                <option value="CONFIRMED">CONFIRMED</option>
                <option value="UNCONFIRMED">UNCONFIRMED</option>
            </select>
        </th>
        <th>
            <select name="search_Form" class="filter search_init">
                <option value=""></option>
                <option value="F">F</option>
                <option value="C">C</option>
                <option value="N">N</option>
                <option value="M">M</option>
            </select>
        </th>
    </tr>
    <tr>
        <th style="display: none;">ID</th>
        <th>Order ID</th>
        <th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
        <th>Gender</th>
        <th>Height</th>
        <th>Photo Name</th>
        <th>Number of Shirts</th>
        <th>Total Price</th>
        <th>Order Status</th>
        <th>Form</th>
    </tr>
</thead>
<tbody>
    <?php
    $conn = create_connection();
    foreach ($conn->query('SELECT * FROM order_detail') as $row) {
        ?>
        <tr>
            <td style="display: none;"><?= $row['id'] ?></td>
            <td><?= $row['order_id'] ?></td>
            <td><?= $row['first_name'] ?></td>
            <td><?= $row['middle_name'] ?></td>
            <td><?= $row['last_name'] ?></td>
            <td><?= $row['gender'] ?></td>
            <td><?= $row['height'] ?></td>
            <td><img src="../Custom/upload/<?= $row['photo_name'] ?>"></td>
            <td><?= $row['number_of_shirts'] ?></td>
            <td><?= $row['totalItemPrice'] ?></td>
            <td><?= $row['item_status'] ?></td>
            <td><?= $row['form'] ?></td>
        </tr>
    <?php } ?>
</tbody>
<script type="text/javascript">//var oTable = init_table();</script>