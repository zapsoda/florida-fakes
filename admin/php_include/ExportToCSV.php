<?php
require_once("adminFunctions.php");

if (!check_admin_session()) {
    header('Location: ../index.php');
    die();
}

$conn = $conn = create_connection();
$extra = "";
$params = array();
switch ($_POST['file']) {
    case 'shipping':
        if ((isset($_POST['form'])) && (!empty($_POST['form']))) { 
            $extra = ' INNER JOIN order_detail ON (shipping_info.order_id=order_detail.order_id) WHERE order_detail.issueState=?';
            $params[] = $_POST['form'];
        }
        $stmt = $conn->prepare('SELECT shipping_info.order_id, recipient_name, address_line1, address_line2, zip_code, shipping_info.state, country, email, shipping_method FROM shipping_info'.$extra);
        $stmt->execute($params);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($data as $row=>$info) {
            $data[$row]['shipping_method'] = toShippingMethod($info['shipping_method']);
        }
        array_unshift($data, array('ORDER ID', 'NAME', 'ADDRESS 1', 'ADDRESS 2', 'ZIP', 'STATE', 'COUNTRY', 'EMAIL', 'SHIPPING METHOD'));
        break;

    case 'reloadit':
    case 'moneypak':
        if ((isset($_POST['form'])) && (!empty($_POST['form']))) { 
            $extra = " INNER JOIN order_detail ON (order_main.order_id = order_detail.order_id) WHERE order_detail.issueState=? AND type=?";
            $params = array($_POST['form'], $_POST['file']); 
        } else {
            $extra = " WHERE type=?";
            $params = array($_POST['file']);
        }
        $stmt = $conn->prepare('SELECT order_main.order_id, "", SUM(IF(confirmed=1, amount, 0)), GROUP_CONCAT(CONCAT(code, ": ", amount, " | ") FROM order_main INNER JOIN moneypak ON (moneypak.order_id = order_main.order_id)'.$extra.' GROUP BY order_main.order_id');
        $stmt->execute($params);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($data, array('ORDER ID', 'CONFIRMED/UNCONFIRMED', 'ORDER TOTAL', 'CODES'));
        break;

    case 'items':
        if ((isset($_POST['form'])) && (!empty($_POST['form']))) { 
            $extra = ' WHERE issueState=?';
            $params = array($_POST['form']);
        }
        $stmt = $conn->prepare('SELECT order_id, number_of_shirts, first_name, middle_name, last_name, DOB, address, city, issueState, zip, gender, weight, height, eyeColor, hairColor, photo_name, signatureName, printedSig FROM order_detail'.$extra);
        $stmt->execute($params);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($data as $key=>$value){
            $data[$key]['eyeColor'] = AbrvEyes($value['eyeColor'], $value['issueState']);
            $data[$key]['hairColor'] = AbrvHair($value['hairColor'], $value['issueState']);
            $data[$key]['issueState'] = toState($value['issueState']);
        }
        array_unshift($data, array('ORDER ID', 'QUANTITY', 'FIRST NAME', 'MIDDLE NAME', 'LAST NAME', 'DATE OF BIRTH', 'ADDRESS', 'CITY', 'STATE', 'ZIP', 'GENDER', 'WEIGHT', 'HEIGHT', 'EYE COLOR', 'HAIR COLOR', 'PHOTO NAME', 'SIGNATURE NAME', 'CUSTOM SIG TEXT'));
        break;
    
    default:
        header('Location: ../index.php');
}


if ((isset($_POST['name'])) && (!empty($_POST['name']))) {
    $name = $_POST['name'];
} else {
    $name = "OrdersBackup";
}

$csv = new CSV_Writer($data);
$csv->headers($name);                           
$csv->output();
/**
 * Simple class to properly output CSV data to clients. PHP 5 has a built 
 * in method to do the same for writing to files (fputcsv()), but many times 
 * going right to the client is beneficial. 
 * 
 * @author Jon Gales 
 */
class CSV_Writer{

    public $data = array();
    public $deliminator;

    /**
     * Loads data and optionally a deliminator. Data is assumed to be an array 
     * of associative arrays. 
     * 
     * @param array $data 
     * @param string $deliminator 
     */
    function __construct($data, $deliminator = ",") {
        if (!is_array($data)) {
            throw new Exception('CSV_Writer only accepts data as arrays');
        }

        $this->data = $data;
        $this->deliminator = $deliminator;
    }

    private function wrap_with_quotes($data) {
        $data = preg_replace('/"(.+)"/', '""$1""', $data);
        return sprintf('"%s"', $data);
    }

    /**
     * Echos the escaped CSV file with chosen delimeter 
     * 
     * @return void 
     */
    public function output() {
        foreach ($this->data as $row) {
            $quoted_data = array_map(array('CSV_Writer', 'wrap_with_quotes'), $row);
            echo sprintf("%s\n", implode($this->deliminator, $quoted_data));
        }
    }

    /**
     * Sets proper Content-Type header and attachment for the CSV outpu 
     * 
     * @param string $name 
     * @return void 
     */
    public function headers($name) {
        header('Content-Type: application/csv');
        header("Content-disposition: attachment; filename={$name}.csv");
    }

}

?>