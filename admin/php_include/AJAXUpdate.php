<?php
include "adminFunctions.php";
$row = json_decode($_POST['rows'], TRUE);
$status = $_POST['state'];
// Make an array of row numbers

foreach ($row as $orders) {
    $orderIDs[] = $orders[10];
}

// Make the query.
$sql = "UPDATE order_detail SET item_status=? WHERE id IN (";

// Get number of placeholders
$sql .= implode(',', array_fill(0, count($orderIDs), '?'));
$sql .= ")";

// This is that other stuff tha goes to prepare() to fill in the question mark parameter things
$dataForQuery = array_merge(array($status), $orderIDs);
$conn = create_connection();
$stmt = $conn->prepare($sql);
$stmt->execute($dataForQuery);
if (($status == "CONFIRMED") || ($status == "SHIPPING")) {
    //Update main orders status
    $orders = $conn->query("SELECT DISTINCT(order_id) FROM order_detail WHERE item_status='CONFIRMED'");
    $row = $orders->fetchAll(PDO::FETCH_ASSOC);
    // Make an array of row numbers
    foreach ($row as $orders) {
        $orderIDs2[] = $orders['order_id'];
    }

    // Make the query.
    $sql2 = "UPDATE order_main SET order_status='CONFIRMED' WHERE order_id IN (";

    // Get number of placeholders
    $sql2 .= implode(',', array_fill(0, count($orderIDs), '?'));
    $sql2 .= ")";
    $stmt2 = $conn->prepare($sql);
    $stmt2->execute($orderIDs2);
}
?>