<?php
include "adminFunctions.php";
$row = json_decode($_POST['rows'], TRUE);
$status = $_POST['state'];
// Make an array of row numbers

foreach ($row as $orders) {
    $orderIDs[] = $orders[1];
}

// Make the query.
$sql = "UPDATE moneypak SET confirmed=? WHERE code IN (";

// Get number of placeholders
$sql .= implode(',', array_fill(0, count($orderIDs), '?'));
$sql .= ")";

// This is that other stuff tha goes to prepare() to fill in the question mark parameter things
$dataForQuery = array_merge(array($status), $orderIDs);

$conn = create_connection();
$stmt = $conn->prepare($sql);
$stmt->execute($dataForQuery);

$stmt = $conn->query('SELECT order_main.order_id, SUM(IF(confirmed=1, amount, 0)) AS "amount" FROM order_main INNER JOIN moneypak ON (moneypak.order_id = order_main.order_id) GROUP BY order_main.order_id');
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
$message = "";
foreach ($data as $key=>$value) {
	$stmt = $conn->prepare('SELECT SUM(totalItemPrice) AS "amount" FROM order_detail WHERE order_id=?');
	$stmt->execute(array($value['order_id']));
	$orderTotal = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach ($orderTotal as $key2 => $value2) {
		$message .= var_dump($orderTotal);
		$message .= "\n\n".$value['amount'];
		if ($value2['amount'] >= $orderTotal[$key]['amount']) {
			$message .= "Order total: ".$orderTotal[$key]['amount']."\n"."Order price: ".$value2['order_id'];
			$stmt = $conn->prepare('UPDATE order_main SET order_status="CONFIRMED" WHERE order_id=?');
			$stmt->execute(array($value2['order_id']));
		}
	}
}
echo $message;
?>