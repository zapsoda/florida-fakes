<?php
require_once("php_include/adminFunctions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Florida Fakes</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Kevin Rajaram">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/normalize.min.css">
    <link rel="stylesheet" href="../css/main.css">

    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
    <![endif]-->
    <title>Admin - Update</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>

    <script type="text/javascript" language="javascript" src="datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8" src="datatables/extras/TableTools/media/js/ZeroClipboard.js"></script>
    <script type="text/javascript" language="javascript" src="datatables/extras/TableTools/media/js/TableTools.js"></script>

    <script src="datatables/extras/ColumnFilter/media/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
    <style type="text/css">
        @import "datatables/media/css/demo_page.css";
        @import "datatables/media/css/demo_table_jui.css";
        @import "datatables/media/css/demo_table.css";
        @import "datatables/media/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        @import "datatables/extras/TableTools/media/css/TableTools_JUI.css";
        @import "datatables/extras/TableTools/media/css/TableTools.css";
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            var oTable = init_table();
            var asInitVals = new Array();

            $("thead input").keyup(function() {
                oTable.fnFilter(this.value, $("thead input").index(this));
            });


            $("thead input").each(function(i) {
                asInitVals[i] = this.value;
            });
        });

        function init_table() {
            var table = $("#datatables").dataTable({
                "bJQueryUI": false,
                "sDom": 'T<"clear">lrtip',
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "oTableTools": {
                    "sRowSelect": "multi",
                    "aButtons": [
                        //{"sExtends": "ajax_confirmed", "sAjaxUrl": "php_include/AJAXUpdate.php", "sButtonText": "Set Selected To Confirmed", "sDiv": "SetToConfirmed"},
                        {"sExtends": "ajax_unconfirmed", "sAjaxUrl": "php_include/AJAXUpdate.php", "sButtonText": "Set Selected To Unconfirmed", "sDiv": "SetToUnconfirmed"},
                        {"sExtends": "ajax_shipped", "sAjaxUrl": "php_include/AJAXUpdate.php", "sButtonText": "Set Selected To Shipped", "sDiv": "SetToShipped"},
                        {"sExtends": "ajax_pending", "sAjaxUrl": "php_include/AJAXUpdate.php", "sButtonText": "Set Selected To Pending", "sDiv": "SetToPending"},
                        "select_all",
                        "select_none"
                    ]
                }
            })
            return table;
        }
    </script>
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<section id="header">
    <div class="container">
        <div class="logo"><a href="./index.php" title="Home"><img src="../img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->
        <nav>
            <ul><?php include('php_include/navigation_menu.php'); ?></ul>
        </nav>
    </div><!-- /container -->
</section><!-- #header -->

<section id="announcements">
    <div class="container">
        <h5 class="announce-icon"><strong>Announcements</strong></h5>
        <?php echo getContent('announcement'); ?>
    </div><!-- /container -->
</section><!-- #announcements -->

<section id="admin">
    <div class="container">
        <table id="datatables" class="display">
            <thead>
                <tr>
                    <th><input type="text" name="search_ID" class="filter search_init" /></th>
                    <th><input type="text" name="search_FName" class="filter search_init" /></th>
                    <th><input type="text" name="search_MName" class="filter search_init" /></th>
                    <th><input type="text" name="search_LName" class="filter search_init" /></th>
                    <th><input type="text" name="search_State" class="filter search_init" /></th>
                    <th><input type="text" name="search_NumShirts" class="filter search_init" /></th>
                    <th><input type="text" name="search_TotPrice" class="filter search_init" /></th>
                    <th><input type="text" name="search_Status" class="filter search_init" /></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <th>Order ID</th>
                    <th>First Name</th>
                    <th>Initial</th>
                    <th>Last Name</th>
                    <th>State</th>
                    <th># of Shirts</th>
                    <th>Total Price</th>
                    <th>Order Status</th>
                    <th>Photo</th>
                    <th>Date</th>
                    <th style="display: none;">ID</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = create_connection();
                foreach ($conn->query('SELECT * FROM order_detail') as $row) {
                    ?>
                    <tr>
                        <td><?= $row['order_id'] ?></td>
                        <td><?= $row['first_name'] ?></td>
                        <td><?php if(isset($row['middle_name'][0])){echo $row['middle_name'][0];}?></td>
                        <td><?= $row['last_name'] ?></td>
                        <?php
                        $row['state'] = toState($row['state']);
                        ?>
                        <td><?= $row['state'] ?></td>
                        <td><?= $row['number_of_shirts'] ?></td>
                        <td><?= $row['totalItemPrice'] ?></td>
                        <td><?= $row['item_status'] ?></td>
                        <td><img src="//<?= domain."/upload/".$row['photo_name'] ?>"></td>
                        <td><?= $row['date_time'] ?></td>
                        <td style="display: none;"><?= $row['id'] ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</section>

<section id="footer">
    <div class="container">
        <div class="half">
            <p class="large">FloridaFakes</p>
            <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
        </div>

        <div class="half">
            <ul class="footer-nav">
                <?php include('php_include/navigation_menu.php'); ?>
            </ul>
        </div>
    </div><!-- /container -->
</section><!-- #footer -->
</body>
</html>