<?php
require_once("php_include/adminFunctions.php");
if (isset($_POST['updateContent'])) {
    $conn = create_connection();
    $areas = array('announcement'=>$_POST['annoucementContent'], 'footer'=>$_POST['footerContent']);
    foreach ($areas as $area => $content) {
        $setData = $conn->prepare('INSERT INTO content(area, content) values(:area, :content) ON DUPLICATE KEY UPDATE area=:area, content=:content;');
        $setData->execute(array(':area'=>$area, ':content'=>$content));
    }
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Florida Fakes</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Kevin Rajaram">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/normalize.min.css">
    <link rel="stylesheet" href="../css/main.css">
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<section id="header">
    <div class="container">
        <div class="logo"><a href="./index.php" title="Home"><img src="../img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->
        <nav>
            <ul><?php include('php_include/navigation_menu.php'); ?></ul>
        </nav>
    </div><!-- /container -->
</section><!-- #header -->
<form method="post">
    <section id="announcements">
        <div class="container">
            <h5 class="announce-icon"><strong>Announcements</strong></h5>
            <textarea style="margin-top: 0px; margin-bottom: 0px; height: 100px;width: 100%;" name="annoucementContent"><?php echo getContent('announcement'); ?></textarea>
        </div><!-- /container -->
    </section><!-- #announcements -->

    <section id="admin">
        <div class="container">
            <h1>Edit Content</h1>
            You can use this page to edit the site wide content on your site.<br>
            Enter your content in the text boxes around this page and when you are done click update<br>
            <input type="submit" value="Update" name="updateContent" class="button"/>
            <br>
        </div>
    </section>

    <section id="footer">
        <div class="container">
            <div class="half">
                <p class="large">FloridaFakes</p>
                <p style="padding-right:60px;"><textarea style="margin-top: 0px; margin-bottom: 0px; height: 100px;width: 100%;" name="footerContent"><?php echo getContent('footer'); ?></textarea></p>
            </div>

            <div class="half">
                <ul class="footer-nav">
                    <?php include('php_include/navigation_menu.php'); ?>
                </ul>
            </div>
        </div><!-- /container -->
    </section><!-- #footer -->
</form>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="js/main.js"></script>
</body>
</html>