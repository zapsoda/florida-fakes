<?php
require_once("php_include/adminFunctions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Florida Fakes</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Kevin Rajaram">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/normalize.min.css">
    <link rel="stylesheet" href="../css/main.css">
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<section id="header">
    <div class="container">
        <div class="logo"><a href="./index.php" title="Home"><img src="../img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->
        <nav>
            <ul><?php include('php_include/navigation_menu.php'); ?></ul>
        </nav>
    </div><!-- /container -->
</section><!-- #header -->

<section id="announcements">
    <div class="container">
        <h5 class="announce-icon"><strong>Announcements</strong></h5>
        <?php echo getContent('announcement'); ?>
    </div><!-- /container -->
</section><!-- #announcements -->

<section id="admin">
    <div class="container">
        <form method="post" action="php_include/ExportToCSV.php">
            Name:<br>
            <input type="text" name="name">.CSV<br>
            Issue State:<br>
            <select name="form">
                <option value="">All</option>
                <?php foreach($stateInfo as $stateID=>$info) { ?>
                    <option value="<?=$info['id']?>"><?=$info['name']?></option>
                <?php } ?>
            </select><br>
            Info:<br>
            <select name="file">
                <option value="shipping" SELECTED>Shipping info</option>
                <option value="items">Ordered Items</option>
                <option value="reloadit">ReLoadIt</option>
                <option value="moneypak">MoneyPak</option>
            </select><br>
            <input type="submit" class="button" value="Download"><br>
        </form>
    </div>
</section>

<section id="footer">
    <div class="container">
        <div class="half">
            <p class="large">FloridaFakes</p>
            <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
        </div>

        <div class="half">
            <ul class="footer-nav">
                <?php include('php_include/navigation_menu.php'); ?>
            </ul>
        </div>
    </div><!-- /container -->
</section><!-- #footer -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="js/main.js"></script>
</body>
</html>