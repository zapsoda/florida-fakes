<?php
require_once("php_include/adminFunctions.php");
if (isset($_POST['username'])) {
    $conn = create_connection();
    if (check_admin_login($_POST['username'], $_POST['password'])) {
        if(create_admin_session($_POST['username'])){
            if(check_admin_session()){
                header('Location: update_order.php');
                die();
            } else {
                echo "<br>Check failed :(";
            }
        }
    } else {
        $errorMessage = "Incorrect Username or Password<br>";
    }
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Florida Fakes</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Kevin Rajaram">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/normalize.min.css">
    <link rel="stylesheet" href="../css/main.css">

    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
    <![endif]-->
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<section id="header">
    <div class="container">
        <div class="logo"><a href="../index.php" title="Home"><img src="../img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->
        <nav>
        </nav>
    </div><!-- /container -->
</section><!-- #header -->

<section id="announcements">
    <div class="container">
        <h5 class="announce-icon"><strong>Announcements</strong></h5>
        <?php echo getContent('announcement'); ?>
    </div><!-- /container -->
</section><!-- #announcements -->

<section id="login">
    <div class="container">
        <table class="main_table">
            <tr>
                <td>
                    <form name="user_login" action="" method="post">
                        <table class="form_table">
                            <tbody>
                            <?php
                            if (isset($errorMessage)) {
                                ?>
                                <tr>
                                    <td style="text-align:center; vertical-align:middle;" colspan="2">
                                        <font color=red><?= $errorMessage ?></font>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <th style="width:100%" colspan="2">
                                    Admin Login
                                </th>
                            </tr>
                            <tr>
                                <td style="width:40%">
                                    Email&nbsp;:
                                </td>
                                <td style="width:60%">
                                    <input type="text" name="username" class="input_text" maxlength="40" value="<?php if (isset($_POST['username'])){echo $_POST['username'];} else {echo "";}?>" />
                                    <label class="error_message_label" id="username_error">&nbsp;</label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:40%">
                                    Password&nbsp;:
                                </td>
                                <td style="width:60%">
                                    <input type="password" name="password" class="input_text" maxlength="99" />
                                    <label class="error_message_label" id="password_error">&nbsp;</label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:100%; text-align:center; vertical-align:middle;" colspan="2">
                                    <input type="submit" name="login" value="Login" class="form_submit_button" />
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </td>
            </tr>
        </table>
    </div>
</section>

<section id="footer">
    <div class="container">
        <div class="half">
            <p class="large">FloridaFakes</p>
            <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
        </div>

        <div class="half">
            <ul class="footer-nav">
            </ul>
        </div>
    </div><!-- /container -->
</section><!-- #footer -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="js/main.js"></script>
</body>
</html>