<?php
require_once("php_include/orderFunctions.php");

session_start();
$_SESSION['type']="order";
if (!session_check()||$_SESSION['page']<2) {
    header('Location: order.php');
    die();
}
$gold_membership = 0;
$gold_member_discount = 0;
$membership_id = "";

$secret = BTC_secret;
$my_address = BTC_Address;
$my_callback_url = 'http://test.com';
$root_url = 'https://blockchain.info/api/receive';
$parameters = 'method=create&address=' . $my_address .'&shared=false&callback='. urlencode($my_callback_url);
$response = file_get_contents($root_url . '?' . $parameters);
$object = json_decode($response);
$_SESSION['btc_address'] = $object->input_address;

$conn = create_connection();
$order_id = $_SESSION['order_id'];

$stmt = $conn->prepare("insert into order_main(order_id, production_speed, email, production_speedup_charge, btc_address, paymentMethod) values (:order_id, :production_speed, :email, :production_speedup_charge, :btc_address, :payment)");
$stmt->execute(array(':order_id'=>$order_id, ':production_speed'=>$_SESSION['production_speed'], ':email'=>$_SESSION['email'], ':production_speedup_charge'=>$_SESSION['production_speedup_charge'], ':btc_address'=>$_SESSION['btc_address'], ':payment'=>$_SESSION['payment_method']));
foreach ($_SESSION['cart'] as $key=>$cart_arr) {
    $newPhotoName = generate_photo_name($cart_arr['state'], $_SESSION['order_id'], $cart_arr['FName'], $cart_arr['LName']);
    move_photo($cart_arr['picture'], $newPhotoName);
    $cart_arr['picture'] = $newPhotoName . "." . get_file_extension($cart_arr['picture']);
    if ($cart_arr['customSig'] != NULL) {
        move_photo($cart_arr['customSig'], 'Signature_'.$newPhotoName);
        $cart_arr['customSig'] = 'Signature_' . $newPhotoName . "." . get_file_extension($cart_arr['picture']);
    }
    $stmt = $conn->prepare("insert into order_detail(order_id, person_id, first_name, middle_name, last_name, DOB, address, city, issueState, zip, gender, weight, height, eyeColor, hairColor, photo_name, printedSig, signatureName, number_of_shirts, price_per_shirt, totalItemPrice) values (:order_id, :key, :FirstName, :MiddleName, :LastName, :DOB, :Address, :City, :State, :Zip, :Gender, :Weight, :Height, :EyeColor, :HairColor, :Photo, :printedSig, :Sig, :Number, :PricePer, :Total)");
    $stmt->execute(array(':order_id'=>$order_id, ':key'=>$key, ':FirstName'=>$cart_arr['FName'], ':MiddleName'=>$cart_arr['MName'], ':LastName'=>$cart_arr['LName'], ':DOB'=>$cart_arr['DOB'], ':Address'=>$cart_arr['address'], ':City'=>$cart_arr['city'], ':State'=>$cart_arr['state'], ':Zip'=>$cart_arr['zip'], ':Gender'=>$cart_arr['gender'], ':Weight'=>$cart_arr['weight'], ':Height'=>$cart_arr['height'], ':EyeColor'=>$cart_arr['eye'], ':HairColor'=>$cart_arr['hair'], ':Photo'=>$cart_arr['picture'], ':printedSig'=>$cart_arr['printedSig'], ':Sig'=>$cart_arr['customSig'], ':Number'=>$cart_arr['quanity'], ':PricePer'=>$cart_arr['shirtPrice'], ':Total'=>$cart_arr['price']));
}
$stmt = $conn->prepare("insert into shipping_info(order_id, recipient_name, address_line1, address_line2, zip_code, state, country, email, shipping_method) values (:order_id, :recipient_name, :address_line1, :address_line2, :zip_code, :state, :country, :email, :shipping_method)");
$stmt->execute(array(':order_id'=>$order_id, ':recipient_name'=>$_SESSION['recipient_name'], ':address_line1'=>$_SESSION['address_line_1'], ':address_line2'=>$_SESSION['address_line_2'], ':zip_code'=>$_SESSION['zip_code'], ':state'=>$_SESSION['state'], ':country'=>$_SESSION['country'], ':email'=>$_SESSION['email'], ':shipping_method'=>$_SESSION['shipping_method']));
if (isset($_SESSION['is_new_user']) && ($_SESSION['is_new_user'] == "Yes")) {
    $password = generate_password();
    add_user($_SESSION['email'], generate_password_hash($password));
    check_password($_SESSION['email'], $password);
    send_order_confirmation($_SESSION['email'], $password, $order_id);
} else {
    send_order_confirmation($_SESSION['email'], "", $order_id);
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Florida Fakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Kevin Rajaram">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="header">
            <div class="container">
                <div class="logo"><a href="index.php" title="Home"><img src="img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->

                <nav>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li class="active"><a href="order.php">Order</a></li>
                    </ul>
                </nav>
            </div><!-- /container -->
        </section><!-- #header -->
        
        <section id="announcements">
            <div class="container">
                <h5 class="announce-icon"><strong>Announcements</strong></h5>
                <?php echo getContent('announcement'); ?>
            </div><!-- /container -->
        </section><!-- #announcements -->
        
        <section id="order">
            <div class="container">
                <h1>Order Confirmation</h1>
                <br>Thank you for placing an order your with us.
                <br>You'll receive an e-mail confirmation with your order details <?php if ((isset($_SESSION['is_new_user'])) && ($_SESSION['is_new_user'] == "Yes")) {echo "and password";}?> within 24 hours.
                <br>Please login to the user control panel and submit payment within 3 days.
                <br>Your order tracking number is <b><?= $order_id; ?></b>
                <br><a href="./userPanel/index.php?order_id=<?= $order_id; ?>">Track Your Order Here</a>
            </div>
        </section>

        <section id="footer">
            <div class="container">
                <div class="half">
                    <p class="large">FloridaFakes</p>
                    <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
                </div>

                <div class="half">
                    <ul class="footer-nav">
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li><a href=""></a></li>
                        <li><a href="order.php">Order</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div><!-- /container -->
        </section><!-- #footer -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="js/main.js"></script>
    </body>
</html>
<?php
session_destroy();
?>