<?php
require_once("php_include/orderFunctions.php");
session_start();
if (!session_check()||$_SESSION['page']<2) {
    header('Location: order.php');
    die();
}
$tempArray = array();
foreach ($_SESSION['cart'] as $key => $value) {
    array_push($tempArray, $value['state']);
}
$uniqueStateCount = count(array_unique($tempArray));

if($_SESSION['shipping_method']==2){$_SESSION['shipping_method_price']=number_format(20*$uniqueStateCount, 2, '.', '');} else {$_SESSION['shipping_method_price'] = "0.00";}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Florida Fakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Kevin Rajaram">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="header">
            <div class="container">
                <div class="logo"><a href="index.php" title="Home"><img src="img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->

                <nav>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li class="active"><a href="order.php">Order</a></li>
                    </ul>
                </nav>
            </div><!-- /container -->
        </section><!-- #header -->
        
        <section id="announcements">
            <div class="container">
                <h5 class="announce-icon"><strong>Announcements</strong></h5>
                <?php echo getContent('announcement'); ?>
            </div><!-- /container -->
        </section><!-- #announcements -->
        <section id="order">
            <div class="container">
                <h1>Order Preview</h1>
                <div id="orderPreview">
                    <table width="100%">
                        <tr>
                            <td width="100%">
                                <table style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Birth date</th>
                                            <th>Address</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Zip</th>
                                            <th>Gender</th>
                                            <th>Weight</th>
                                            <th>Height</th>
                                            <th>Eye Color</th>
                                            <th>#</th>
                                            <th>Picture</th>
                                            <th>Signature</th>
                                            <th>Price</th>
                                            <th>EDIT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $final_total_of_prices = 0;
                                        foreach ($_SESSION['cart'] as $key => $cart_arr) {
                                            $final_total_of_prices+=$cart_arr['price'];
                                            ?>
                                            <tr>
                                                <td><?=$cart_arr['FName']." ".$cart_arr['LName'];?></td>
                                                <td><?=$cart_arr['DOB'];?></td>
                                                <td><?=$cart_arr['address'];?></td>
                                                <td><?=$cart_arr['city'];?></td>
                                                <td><?=toState($cart_arr['state']);?></td>
                                                <td><?=$cart_arr['zip'];?></td>
                                                <td><?=$cart_arr['gender'];?></td>
                                                <td><?=$cart_arr['weight'];?></td>
                                                <td><?=$cart_arr['height'];?></td>
                                                <td><?=$cart_arr['eye'];?></td>
                                                <td><?=$cart_arr['quanity'];?></td>
                                                <td><img src="<?= upload_dir.$cart_arr['picture'] ?>" width="100px" max-height="150px"/></td>
                                                <td><?php if($cart_arr['customSig']!=NULL){?><img src="<?= upload_dir.$cart_arr['customSig'] ?>" width="100px" max-height="50px"/><?php } ?></td>
                                                <td><?="$".number_format($cart_arr['price'], 2, '.', '');?></td>
                                                <td><a href="modify.php?id=<?=$key?>">MODIFY</a><br><a href="modify.php?id=<?=$key?>&action=delete">DELETE</a></td>
                                            </tr>
                                        <?php
                                        }
                                        $final_price = $final_total_of_prices + $_SESSION['production_speedup_charge'] + $_SESSION['shipping_method_price'];
                                        $_SESSION['total_price'] = number_format($final_price, 2, '.', '');
                                        ?>
                                        <tr>
                                            <td colspan="12"></td>
                                            <td style="width: 13%; font-size: 11px; text-align: right; vertical-align: middle;"><b>Sub Total&nbsp;:&nbsp;</b></td>
                                            <td style="width: 7%; font-size: 11px; text-align: center; vertical-align: middle;"><?= "$" . number_format($final_total_of_prices, 2, '.', '') ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="12"></td>
                                            <td style="width: 13%; font-size: 11px; text-align: right; vertical-align: middle;"><b>Prod Charge&nbsp;:&nbsp;</b></td>
                                            <td style="width: 7%; font-size: 11px; text-align: center; vertical-align: middle;"><?= "$" . $_SESSION['production_speedup_charge'] ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="12"></td>
                                            <td style="width: 13%; font-size: 11px; text-align: right; vertical-align: middle;"><b>Shipping Charge:&nbsp;</b></td>
                                            <td style="width: 7%; font-size: 11px; text-align: center; vertical-align: middle;"><?= "$" . $_SESSION['shipping_method_price'] ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="12"></td>
                                            <td style="width: 13%; font-size: 11px; text-align: right; vertical-align: middle;"><b>Total Price&nbsp;:&nbsp;</b></td>
                                            <td style="width: 7%; font-size: 11px; text-align: center; vertical-align: middle;"><?= "$" . $_SESSION['total_price'] ?></td>
                                        </tr>
                                        <?php ?>                                                                                
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0;">
                                <table style="width: 50%;">
                                    <tr>
                                        <th style="width: 35%; text-align: left;">
                                            Shipping Information
                                        </th>
                                        <th style="width: 35%; text-align: left;">
                                            &nbsp;&nbsp;
                                        </th>
                                        <th style="width: 30%">
                                            &nbsp;&nbsp;
                                        </th>														
                                    </tr>
                                    <tr>
                                        <td style="width: 35%; text-align: left;">
                                            Shipping Method:
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <?= toShippingMethod($_SESSION['shipping_method']);?>
                                        </td>
                                        <td style="width: 30%">
                                            <label id="shipping_method_error" class="error_message_label">&nbsp;</label>
                                        </td>														
                                    </tr>
                                    <tr>
                                        <td style="width: 35%; text-align: left;">
                                            Production Speed:
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <?=  toProductionSpeed($_SESSION['production_speed']);?>
                                        </td>
                                        <td style="width: 30%">
                                            <label id="production_speed_error" class="error_message_label">&nbsp;</label>
                                        </td>														
                                    </tr>
                                    <tr>
                                        <td style="width: 35%; text-align: left;">
                                            Recipient's Name:
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <?= $_SESSION['recipient_name'] ?>
                                        </td>
                                        <td style="width: 30%">
                                            <label id="recipient_name_error" class="error_message_label">&nbsp;</label>
                                        </td>														
                                    </tr>
                                    <tr>
                                        <td style="width: 35%; text-align: left;">
                                            Email Address:
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <?= $_SESSION['email'] ?>
                                        </td>
                                        <td style="width: 30%">
                                            <label id="email_error" class="error_message_label">&nbsp;</label>
                                        </td>														
                                    </tr>
                                    <tr>
                                        <td style="width: 35%; text-align: left;">
                                            Address Line 1:
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <?= $_SESSION['address_line_1'] ?>
                                        </td>
                                        <td style="width: 30%">
                                            <label id="addr_line1_error" class="error_message_label">&nbsp;</label>
                                        </td>														
                                    </tr>
                                    <tr>
                                        <td style="width: 35%; text-align: left;">
                                            Address Line 2:
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <?= $_SESSION['address_line_2'] ?>
                                        </td>
                                        <td style="width: 30%">
                                            <label id="addr_line2_error" class="error_message_label">&nbsp;</label>
                                        </td>														
                                    </tr>
                                    <tr>
                                        <td style="width: 35%; text-align: left;">
                                            Zip Code:
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <?= $_SESSION['zip_code'] ?>
                                        </td>
                                        <td style="width: 30%">
                                            <label id="zip_code_error" class="error_message_label">&nbsp;</label>
                                        </td>														
                                    </tr>
                                    <tr>
                                        <td style="width: 35%; text-align: left;">
                                            State:
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <?= $_SESSION['state'] ?>
                                        </td>
                                        <td style="width: 30%">
                                            <label id="state_error" class="error_message_label">&nbsp;</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 35%; text-align: left;">
                                            Country:
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <?= $_SESSION['country'] ?>
                                        </td>
                                        <td style="width: 30%">
                                            <label id="country_error" class="error_message_label">&nbsp;</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 35%; text-align: left;">
                                            Payment Method:
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <?= $_SESSION['payment_method'] ?>
                                        </td>
                                        <td style="width: 30%">
                                            <label id="country_error" class="error_message_label">&nbsp;</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><a href="./editShipping.php"><input type="submit" value="Modify" class="button"><a href="./order.php?source=<?=array_search(basename($_SERVER['REQUEST_URI']), $sources)?>"><input type="submit" value="Add more" class="button"></a></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td  style="width: 100%; padding-bottom: 0;">
                                <table class="form_table_bottom">
                                    <tbody>
                                        <tr>
                                            <form method="post" action="order_confirmation.php">
                                                <td style="width: 100%;">
                                                    <input class="button" type="submit" value="CONFIRM ORDER" />
                                                </td>
                                            </form>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div><!-- /container -->
        </section><!-- #header -->

        <section id="footer">
            <div class="container">
                <div class="half">
                    <p class="large">FloridaFakes</p>
                    <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
                </div>

                <div class="half">
                    <ul class="footer-nav">
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li><a href=""></a></li>
                        <li><a href="order.php">Order</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div><!-- /container -->
        </section><!-- #footer -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="js/main.js"></script>
    </body>
</html>