<?php
require_once("php_include/orderFunctions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Florida Fakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Kevin Rajaram">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="header">
            <div class="container">
                <div class="logo"><a href="index.php" title="Home"><img src="img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->

                <nav>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li class="active"><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li class="active"><a href="order.php">Order</a></li>
                    </ul>
                </nav>
            </div><!-- /container -->
        </section><!-- #header -->

        <section id="announcements">
            <div class="container">
                <h5 class="announce-icon"><strong>Announcements</strong></h5>
                <?php echo getContent('announcement'); ?>
            </div><!-- /container -->
        </section><!-- #announcements -->

        <section id="faq">
            <div class="container">

                <h1>Frequently Asked Questions</h1>

                <div id="tabs">

                    <ul>
                        <li><a href="#fragment-1">Quality Questions</a></li>
                        <li><a href="#fragment-2">Order Questions</a></li>
                        <li><a href="#fragment-3">Shipping Questions</a></li>
                        <li><a href="#fragment-4">Payment Questions</a></li>
                    </ul>

                    <div class="clearfix"></div>
                    <br/>

                    <div id="fragment-1">
                        <dl>          
                            <dt>Do your IDs pass in-state?</dt>
                            <dd>Most of the time, yes. It really depends on how you look, dress, walk, talk, etc. If you would like to use your ID in-state, we recommend getting an ID for a different state as well when you place your order. There is nothing “sketchy” about using an out of state ID. People attend colleges from different states all the time.</dd>

                            <dt>Are your IDs scannable?</dt>
                            <dd>Yes! The 2D barcode and the magnetic strip on all of our IDs scan everywhere.</dd>

                            <dt>Do they pass the blacklight test?</dt>
                            <dd>Yes! All of our IDs have high quality UV ink. Check out our <a href="media.php">pictures</a>!</dd>

                            <dt>Do they pass the bend test?</dt>
                            <dd>Yes! Our New York IDs will pass the NY bend test. All of our other IDs bend just like the real IDs and pass any “bend test”. Watch the videos on our <a href="media.php">videos page</a> to see for yourself.</dd>

                            <dt>Do they have holograms?</dt>
                            <dd>Yes! Please check out the <a href="media.php">pictures</a>!</dd>

                            <dt>Do your NY and MN IDs have the correct front laminate?</dt>
                            <dd>Yes. We use a special laminate on our NY and MN licenses that mimic the feel of the real IDs</dd>

                            <dt>Do your IDs have microprint?</dt>
                            <dd>Yes! We print using high quality offset printers that other vendors cannot afford! So we are able to accurately replicate microprint on our IDs.</dd>

                            <dt>What is the clear back overlay on your Florida ID?</dt>
                            <dd>Florida and some other states have a clear overlay that goes on the back of the ID (pictures can be found on the <a href="media.php">pictures</a> page). If you are using your Florida ID in a state other than Florida you will most likely not need this because people in your state don’t know exactly what the back of a Florida ID looks like.</dd>

                            <dt>When do your California IDs expire?</dt>
                            <dd>The old CA IDs were last issued in 2010, so they will expire on your birthday in 2015.</dd>

                            <dt>Do these IDs have any disclaimers on them?</dt>
                            <dd>NO! These IDs LOOK and FEEL exactly like real ones.</dd>

                            <dt>Are you sure it’s supposed to look/feel like this?</dt>
                            <dd>Yes! We have spent hundreds of hours perfecting every state we make. We know exactly how the IDs are supposed to look and feel. Please do not email us telling us that the photo for our CA ID is on the wrong side! The UNDER-21 and OVER-21 CA ID are different. Please do not email us asking why our NY ID is so thin. We know what we’re doing! <strong>You need to be CONFIDENT when using your ID!</strong></dd>

                            <dt>Can I use a custom signature?</dt>
                            <dd>We apply realistic looking signatures to all IDs. However, we do offer custom signatures for $50. if you want to use your own signature just sign a white sheet of paper with a black pen and make a scan of it.  Make sure the pen is not TOO thick or TOO thin. It needs to be the same thickness as your real signature.</dd>

                            <dt>Will it pass if a cop see’s it?</dt>
                            <dd>Probably. Some customers brag about showing their IDs to cops and getting away with it. But don’t be stupid, why push your luck? Avoid places with cops at the door!</dd>

                            <dt>What if my ID gets taken? How much are reprints?</dt>
                            <dd>Reprints are $75 for the first 30 days after your ID has shipped. After that you will have to pay the full price. Please email help@floridafakes.com with the subject “REPRINT”</dd>
                        </dl>
                    </div><!-- fragment-1 -->

                    <div id="fragment-2">

                        <dl>          
                            <dt>Can we get different states for a group order?</dt>
                            <dd>Our IDs all ship from different locations. If you want to get different states for your order you will need to pay for priority shipping for each separate state. Make sure you list the shipping address in each order form you submit, and make sure you write in the notes which states your order has.</dd>

                            <dt>Can we pay after you make our ID?</dt>
                            <dd>No. <a href="nonpayers.html">Click here to find out why</a></dd>

                            <dt>ARE YOU GUYS A SCAM?!</dt>
                            <dd>No! We have many happy customers. We have <a href="media.php">PICTURES</a> of sample IDs and We’ve even got <a href="media.php">VIDEOS</a> of us MAKING THE IDS! Customers have even made video reviews of our IDs and uploaded them to YouTube! Just search FLORIDA FAKES REVIEW on YouTube!</dd>

                            <dd>We know what it’s like to buy a fake ID. You have to trust someone you’ve never met before AND you have to send them the money first. We’re planning on being in business for a while and we NEED your referrals. You were probably referred here by someone else. Someone who has used our product and had great success with it.</dd>

                            <dt>Why should we buy from you?</dt>
                            <dd>Because we’re kids just like you. We know what it’s like to look for an ID. You don’t want to order your ID from some sketchy website and you don’t want to get scammed! You worked hard for that cash. We’ve been in the exact same spot. That’s why we started this business.</dd>

                            <dt>If I order from your site is my information kept private?</dt>
                            <dd>Yes, your info is kept 100% private. We use private offshore emails and web hosting so that NOBODY can access your information except for us. All of your information is permanently deleted after you receive your ID. We keep it temporarily just in case there is a problem with your order.</dd>

                            <dt>How can you have this website without getting in trouble?</dt>
                            <dd>We use an offshore web host. Security is our #1 priority.</dd>

                            <dt>Do you sell to people older than 21?</dt>
                            <dd>We only sell to people age 20 or younger who want to drink and have fun! Do not email us if you are trying to use our IDs to break any laws. We will not respond.</dd>

                            <dt>Can we get a photo of our finished ID?</dt>
                            <dd>If you pay for tracking/insurance we will email you a photo of your ID(s) along with your tracking number.</dd>
                        </dl>
                    </div><!-- fragment-2 -->

                    <div id="fragment-3">
                        <dl>
                            <dt>Is your shipping discreet?</dt>
                            <dd>Yes. Our standard packages look like this : <img src="img/standard_shipping.jpeg" width="116" height="96" alt="" /></dd>
                            <dd>Priority mail looks like this: <img src="img/priority_shipping.jpeg" width="150" height="150" alt="" /></dd>

                            <dd>There is nothing suspicious about the package. It does not have our name on it and it does not feel like there are IDs inside.</dd>

                            <dt>How long will it take to get my ID?</dt>
                            <dd>The whole process can take anywhere from 7-14 business days depending on when you order, how busy we are, shipping method you choose, etc – If you choose our free first class mail you have no right to complain if your ID takes a long time to arrive. We recommend paying for priority shipping to be sure your ID will arrive 2-3 days after being shipped.</dd>

                            <dt>I want my ID faster than that!</dt>
                            <dd>We offer rush production! Check out our pricing page! Rush production does not get you your ID to you by a specific date. We make our IDs in batches, so when you choose rush production your ID will get made in the next batch. Depending on current wait times this can get your ID to you anywhere from 3-8 business days faster!</dd>

                            <dt>Where do you ship from?</dt>
                            <dd>Our IDs are all made in the USA! We have outsourced the production of our IDs so they ship from different locations. We don’t know exactly which states will come from where so please don’t ask us. Standard mail will take 3-5 days depending on where it is being sent. Priority mail will always take 2-3 days no matter how far it is being shipped, so it really doesn’t matter where we ship it from.</dd>

                            <dt>Can I ship to a college?</dt>
                            <dd>Yes! We also offer <span style="color: red;">GROUP DISCOUNTS</span> so make sure to order with a bunch of your friends for the best possible deal!</dd>

                            <dt>Do I have to pay for priority shipping for each person?</dt>
                            <dd>Paying for priority shipping once covers your entire order.</dd>

                            <dt>Do you provide tracking numbers?</dt>
                            <dd>If you pay for tracking we will email you the tracking number when your order ships!</dd>

                            <dt>Do I have to sign for the “package” when it arrives?</dt>
                            <dd>No, the IDs will be hidden inside of what looks like regular everyday mail.</dd>

                            <dt>Do I have to worry about Customs?</dt>
                            <dd>Nope! Our ID’s ship from inside the USA via USPS. You’re safe when you order with us.</dd>
                        </dl>
                    </div><!-- fragment-3 -->

                    <div id="fragment-4">
                        <dl>
                            <dt>How do I make payment?</dt>
                            <dd>Please see the <a href="payment.html">PAYMENT PAGE</a></dd>
                        </dl>
                    </div><!-- /fragment-4 -->

                </div><!-- tabs -->

                <br/>

                <a href="order.html" class="button">PLACE YOUR ORDER TODAY!</a>
            </div><!-- /container -->
        </section><!-- #order -->

        <section id="footer">
            <div class="container">
                <div class="half">
                    <p class="large">FloridaFakes</p>
                    <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
                </div>

                <div class="half">
                    <ul class="footer-nav">
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li><a href=""></a></li>
                        <li><a href="order.php">Order</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div><!-- /container -->
        </section><!-- #footer -->




        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script src="js/main.js"></script>

        <script>
            $("#tabs").tabs({show: {effect: "fadeIn", duration: 500}});
        </script>
    </body>
</html>
