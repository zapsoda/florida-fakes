<?php
require_once("php_include/orderFunctions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Florida Fakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Kevin Rajaram">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="header">
            <div class="container">
                <div class="logo"><a href="index.php" title="Home"><img src="img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->

                <nav>
                    <ul>
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li class="active"><a href="order.php">Order</a></li>
                    </ul>
                </nav>
            </div><!-- /container -->
        </section><!-- #header -->

        <section id="map">
            <div class="container">
                <a href="order.php"><img src="img/map.png" alt="" /></a>
            </div><!-- /container -->
        </section><!-- #header -->

        <section id="announcements">
            <div class="container">
                <h5 class="announce-icon"><strong>Announcements</strong></h5>
                <?php echo getContent('announcement'); ?>
            </div><!-- /container -->
        </section><!-- #announcements -->

        <section id="about">
            <div class="container">
                <ul>
                    <li class="one">
                        <h1>We Are Legitimate</h1>
                        <p>Don't fall for scams! If a site says they sell all 50 states they're most likely a scam! We only sell states that we have perfected. We've spent thousands of dollars on materials and hundreds of hours on getting this business up. It wouldn't make any sense to scam a few people out for a $100. We're here to stay and we need your business as well as your referrals! Check out our FAQ if you have more questions!</p>
                    </li>
                    <li class="two">
                        <h1>Scannable / Blacklight / Hologram</h1>
                        <p>All of our IDs SCAN. They all pass the blacklight test. They all have high quality holograms. Check out some of our videos or pictures to see the quality of our IDs.</p>
                    </li>
                    <li class="three">
                        <h1>High Quality At Low Prices</h1>
                        <p>We offer the HIGHEST QUALITY FAKE IDS for the LOWEST PRICES. Check out our media section to see further proofs of our IDs! Also see customer reviews!</p>
                    </li>
                    <li class="four">
                        <h1>Customer Satisfaction</h1>
                        <p>We strive for 100% customer satisfaction! Check out some of the reviews on our forums to find out what customers think about our IDs! Contact us if you have any pre-sales question or feedback on our services.</p>
                    </li>
                </ul>
            </div><!-- /container -->
        </section><!-- #about -->

        <section id="order">
            <div class="container">
                <a href="order.html" class="button">PLACE YOUR ORDER TODAY!</a>
            </div><!-- /container -->
        </section><!-- #order -->

        <section id="footer">
            <div class="container">
                <div class="half">
                    <p class="large">FloridaFakes</p>
                    <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
                </div>

                <div class="half">
                    <ul class="footer-nav">
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li><a href=""></a></li>
                        <li><a href="order.php">Order</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div><!-- /container -->
        </section><!-- #footer -->




        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="js/main.js"></script>
    </body>
</html>
