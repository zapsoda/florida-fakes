<?php
require_once("php_include/orderFunctions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Florida Fakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Kevin Rajaram">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/zozo.tabs.min.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="header">
            <div class="container">
                <div class="logo"><a href="index.php" title="Home"><img src="img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->

                <nav>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="prices.php">Prices</a></li>
                        <li class="active"><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li class="active"><a href="order.php">Order</a></li>
                    </ul>
                </nav>
            </div><!-- /container -->
        </section><!-- #header -->

        <section id="announcements">
            <div class="container">
                <h5 class="announce-icon"><strong>Announcements</strong></h5>
                <?php echo getContent('announcement'); ?>
            </div><!-- /container -->
        </section><!-- #announcements -->

        <section id="faq">
            <div class="container">

                <h1>Media</h1>

                <div id="tabbed-nav">
                    <ul>
                        <li><a>Photos</a></li>
                        <li><a>Videos</a></li>
                        <li><a>Proofs</a></li>
                    </ul>

                    <div>
                        <div>
                            <div id="tabbed-nav2">
                                <ul>
                                    <li><a>California</a></li>
                                    <li><a>Florida</a></li>
                                    <li><a>New York</a></li>
                                    <li><a>Minnesota</a></li>
                                    <li><a>Colorado</a></li>
                                </ul>

                                <div><!-- start of tabbed2 content wrapper -->
                                    <div>

                                        <ul class="images">
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>

                                        </ul>

                                        <h2>Security Features</h2>
                                        <small>State flags at bottom visible under UV light<br/>State seal and “DMV” in 2-color optical variable ink<br/>Microprinting</small>
                                        <p>These are the BEST FAKE CA IDs you will find! Most of our customers who buy CA IDs are FROM CA! Watch the video review on our videos page to hear how well they work in-state!</p>

                                        <p>The old California IDs are good until 2015. The last of these IDs were issued in October of 2010. Nobody will think this ID is fake. Even in California. It is IDENTICAL to the real one. We offer the UNDER-21 ID. The photo is on the RIGHT side on the UNDER-21 rather than the LEFT side like on the OVER-21 ID. The ID’s issue date will be 2010, so it will say you were UNDER-21 IN 2010. This means you will currently be 21 years old. We have spent a lot of time on the template perfecting it, nobody else has this template! Other sellers all use the same shitty template to make California IDs. Our California IDs are guaranteed to pass in CA! Make sure you take a good ID photo!</p>

                                        <p>Our FAKE California ID looks and feels exactly like a real California ID. We make our ID using a special technique which gives it the “rounded edges” that some bouncers check for in California. Other sellers attempt to replicate this feature but do so very poorly. Our IDs feel EXACTLY like a real California ID. If you held our FAKE California ID in one hand and your REAL California ID in the other you wouldn’t be able to tell the difference!</p>

                                        <p>GO TO THE PROOF PHOTOS PAGE TO SEE PHOTOS OF CALIFORNIA IDS WE HAVE MADE FOR CUSTOMERS</p>

                                        <p>The California hologram is one of the most difficult features to replicate! Most people attempt to stencil it on or stamp it but the results are sub-par. The letters come out thick and ugly. Our IDs have a HIGH QUALITY California hologram. Look at the letters! They are thin and high quality just like the real California ID.</p>

                                        <h2>Important Info About California</h2>
                                        <p>IF YOU ARE ORDERING AN UNDER-21 ID MAKE SURE THE BIRTHDAY ON YOUR ID IS BEFORE 08-25-92 (OUR IDS ALL HAVE AN ISSUE DATE AROUND SEPTEMBER 2010) – WE RECOMMEND MAKING YOUR BIRTH YEAR 1991. ALL CALIFORNIA IDS EXPIRE ON YOUR BIRTHDAY 5 YEARS AFTER BEING ISSUED. SO IF YOUR BIRTHDAY IS JANUARY 1ST, 1992 THEN YOUR ID WILL EXPIRE JANUARY 1ST 2015. IF YOU’RE NOT GOING TO BE 21 UNTIL 2016 WE RECOMMEND MAKING YOUR BIRTHDAY IN DECEMBER SO YOUR ID LASTS ALL THE WAY UNTIL DECEMBER 2015.</p>

                                    </div>
                                    <div>
                                        <ul class="images">
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                        </ul>

                                        <h2>Security Features</h2>
                                        <small>State flags at bottom visible under UV light<br/>State seal and “DMV” in 2-color optical variable ink<br/>Microprinting</small>
                                        <p>These are the BEST FAKE CA IDs you will find! Most of our customers who buy CA IDs are FROM CA! Watch the video review on our videos page to hear how well they work in-state!</p>

                                        <p>The old California IDs are good until 2015. The last of these IDs were issued in October of 2010. Nobody will think this ID is fake. Even in California. It is IDENTICAL to the real one. We offer the UNDER-21 ID. The photo is on the RIGHT side on the UNDER-21 rather than the LEFT side like on the OVER-21 ID. The ID’s issue date will be 2010, so it will say you were UNDER-21 IN 2010. This means you will currently be 21 years old. We have spent a lot of time on the template perfecting it, nobody else has this template! Other sellers all use the same shitty template to make California IDs. Our California IDs are guaranteed to pass in CA! Make sure you take a good ID photo!</p>

                                        <p>Our FAKE California ID looks and feels exactly like a real California ID. We make our ID using a special technique which gives it the “rounded edges” that some bouncers check for in California. Other sellers attempt to replicate this feature but do so very poorly. Our IDs feel EXACTLY like a real California ID. If you held our FAKE California ID in one hand and your REAL California ID in the other you wouldn’t be able to tell the difference!</p>

                                        <p>GO TO THE PROOF PHOTOS PAGE TO SEE PHOTOS OF CALIFORNIA IDS WE HAVE MADE FOR CUSTOMERS</p>

                                        <p>The California hologram is one of the most difficult features to replicate! Most people attempt to stencil it on or stamp it but the results are sub-par. The letters come out thick and ugly. Our IDs have a HIGH QUALITY California hologram. Look at the letters! They are thin and high quality just like the real California ID.</p>

                                        <h2>Important Info About California</h2>
                                        <p>IF YOU ARE ORDERING AN UNDER-21 ID MAKE SURE THE BIRTHDAY ON YOUR ID IS BEFORE 08-25-92 (OUR IDS ALL HAVE AN ISSUE DATE AROUND SEPTEMBER 2010) – WE RECOMMEND MAKING YOUR BIRTH YEAR 1991. ALL CALIFORNIA IDS EXPIRE ON YOUR BIRTHDAY 5 YEARS AFTER BEING ISSUED. SO IF YOUR BIRTHDAY IS JANUARY 1ST, 1992 THEN YOUR ID WILL EXPIRE JANUARY 1ST 2015. IF YOU’RE NOT GOING TO BE 21 UNTIL 2016 WE RECOMMEND MAKING YOUR BIRTHDAY IN DECEMBER SO YOUR ID LASTS ALL THE WAY UNTIL DECEMBER 2015.</p>
                                    </div>
                                    <div>
                                        <ul class="images">
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                        </ul>

                                        <h2>Security Features</h2>
                                        <small>State flags at bottom visible under UV light<br/>State seal and “DMV” in 2-color optical variable ink<br/>Microprinting</small>
                                        <p>These are the BEST FAKE CA IDs you will find! Most of our customers who buy CA IDs are FROM CA! Watch the video review on our videos page to hear how well they work in-state!</p>

                                        <p>The old California IDs are good until 2015. The last of these IDs were issued in October of 2010. Nobody will think this ID is fake. Even in California. It is IDENTICAL to the real one. We offer the UNDER-21 ID. The photo is on the RIGHT side on the UNDER-21 rather than the LEFT side like on the OVER-21 ID. The ID’s issue date will be 2010, so it will say you were UNDER-21 IN 2010. This means you will currently be 21 years old. We have spent a lot of time on the template perfecting it, nobody else has this template! Other sellers all use the same shitty template to make California IDs. Our California IDs are guaranteed to pass in CA! Make sure you take a good ID photo!</p>

                                        <p>Our FAKE California ID looks and feels exactly like a real California ID. We make our ID using a special technique which gives it the “rounded edges” that some bouncers check for in California. Other sellers attempt to replicate this feature but do so very poorly. Our IDs feel EXACTLY like a real California ID. If you held our FAKE California ID in one hand and your REAL California ID in the other you wouldn’t be able to tell the difference!</p>

                                        <p>GO TO THE PROOF PHOTOS PAGE TO SEE PHOTOS OF CALIFORNIA IDS WE HAVE MADE FOR CUSTOMERS</p>

                                        <p>The California hologram is one of the most difficult features to replicate! Most people attempt to stencil it on or stamp it but the results are sub-par. The letters come out thick and ugly. Our IDs have a HIGH QUALITY California hologram. Look at the letters! They are thin and high quality just like the real California ID.</p>

                                        <h2>Important Info About California</h2>
                                        <p>IF YOU ARE ORDERING AN UNDER-21 ID MAKE SURE THE BIRTHDAY ON YOUR ID IS BEFORE 08-25-92 (OUR IDS ALL HAVE AN ISSUE DATE AROUND SEPTEMBER 2010) – WE RECOMMEND MAKING YOUR BIRTH YEAR 1991. ALL CALIFORNIA IDS EXPIRE ON YOUR BIRTHDAY 5 YEARS AFTER BEING ISSUED. SO IF YOUR BIRTHDAY IS JANUARY 1ST, 1992 THEN YOUR ID WILL EXPIRE JANUARY 1ST 2015. IF YOU’RE NOT GOING TO BE 21 UNTIL 2016 WE RECOMMEND MAKING YOUR BIRTHDAY IN DECEMBER SO YOUR ID LASTS ALL THE WAY UNTIL DECEMBER 2015.</p>
                                    </div>
                                    <div>
                                        <ul class="images">
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                        </ul>

                                        <h2>Security Features</h2>
                                        <small>State flags at bottom visible under UV light<br/>State seal and “DMV” in 2-color optical variable ink<br/>Microprinting</small>
                                        <p>These are the BEST FAKE CA IDs you will find! Most of our customers who buy CA IDs are FROM CA! Watch the video review on our videos page to hear how well they work in-state!</p>

                                        <p>The old California IDs are good until 2015. The last of these IDs were issued in October of 2010. Nobody will think this ID is fake. Even in California. It is IDENTICAL to the real one. We offer the UNDER-21 ID. The photo is on the RIGHT side on the UNDER-21 rather than the LEFT side like on the OVER-21 ID. The ID’s issue date will be 2010, so it will say you were UNDER-21 IN 2010. This means you will currently be 21 years old. We have spent a lot of time on the template perfecting it, nobody else has this template! Other sellers all use the same shitty template to make California IDs. Our California IDs are guaranteed to pass in CA! Make sure you take a good ID photo!</p>

                                        <p>Our FAKE California ID looks and feels exactly like a real California ID. We make our ID using a special technique which gives it the “rounded edges” that some bouncers check for in California. Other sellers attempt to replicate this feature but do so very poorly. Our IDs feel EXACTLY like a real California ID. If you held our FAKE California ID in one hand and your REAL California ID in the other you wouldn’t be able to tell the difference!</p>

                                        <p>GO TO THE PROOF PHOTOS PAGE TO SEE PHOTOS OF CALIFORNIA IDS WE HAVE MADE FOR CUSTOMERS</p>

                                        <p>The California hologram is one of the most difficult features to replicate! Most people attempt to stencil it on or stamp it but the results are sub-par. The letters come out thick and ugly. Our IDs have a HIGH QUALITY California hologram. Look at the letters! They are thin and high quality just like the real California ID.</p>

                                        <h2>Important Info About California</h2>
                                        <p>IF YOU ARE ORDERING AN UNDER-21 ID MAKE SURE THE BIRTHDAY ON YOUR ID IS BEFORE 08-25-92 (OUR IDS ALL HAVE AN ISSUE DATE AROUND SEPTEMBER 2010) – WE RECOMMEND MAKING YOUR BIRTH YEAR 1991. ALL CALIFORNIA IDS EXPIRE ON YOUR BIRTHDAY 5 YEARS AFTER BEING ISSUED. SO IF YOUR BIRTHDAY IS JANUARY 1ST, 1992 THEN YOUR ID WILL EXPIRE JANUARY 1ST 2015. IF YOU’RE NOT GOING TO BE 21 UNTIL 2016 WE RECOMMEND MAKING YOUR BIRTHDAY IN DECEMBER SO YOUR ID LASTS ALL THE WAY UNTIL DECEMBER 2015.</p>
                                    </div>
                                    <div>
                                        <ul class="images">
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                            <li><div><img src="img/mediaimg.jpg" alt="" /></div></li>
                                        </ul>

                                        <h2>Security Features</h2>
                                        <small>State flags at bottom visible under UV light<br/>State seal and “DMV” in 2-color optical variable ink<br/>Microprinting</small>
                                        <p>These are the BEST FAKE CA IDs you will find! Most of our customers who buy CA IDs are FROM CA! Watch the video review on our videos page to hear how well they work in-state!</p>

                                        <p>The old California IDs are good until 2015. The last of these IDs were issued in October of 2010. Nobody will think this ID is fake. Even in California. It is IDENTICAL to the real one. We offer the UNDER-21 ID. The photo is on the RIGHT side on the UNDER-21 rather than the LEFT side like on the OVER-21 ID. The ID’s issue date will be 2010, so it will say you were UNDER-21 IN 2010. This means you will currently be 21 years old. We have spent a lot of time on the template perfecting it, nobody else has this template! Other sellers all use the same shitty template to make California IDs. Our California IDs are guaranteed to pass in CA! Make sure you take a good ID photo!</p>

                                        <p>Our FAKE California ID looks and feels exactly like a real California ID. We make our ID using a special technique which gives it the “rounded edges” that some bouncers check for in California. Other sellers attempt to replicate this feature but do so very poorly. Our IDs feel EXACTLY like a real California ID. If you held our FAKE California ID in one hand and your REAL California ID in the other you wouldn’t be able to tell the difference!</p>

                                        <p>GO TO THE PROOF PHOTOS PAGE TO SEE PHOTOS OF CALIFORNIA IDS WE HAVE MADE FOR CUSTOMERS</p>

                                        <p>The California hologram is one of the most difficult features to replicate! Most people attempt to stencil it on or stamp it but the results are sub-par. The letters come out thick and ugly. Our IDs have a HIGH QUALITY California hologram. Look at the letters! They are thin and high quality just like the real California ID.</p>

                                        <h2>Important Info About California</h2>
                                        <p>IF YOU ARE ORDERING AN UNDER-21 ID MAKE SURE THE BIRTHDAY ON YOUR ID IS BEFORE 08-25-92 (OUR IDS ALL HAVE AN ISSUE DATE AROUND SEPTEMBER 2010) – WE RECOMMEND MAKING YOUR BIRTH YEAR 1991. ALL CALIFORNIA IDS EXPIRE ON YOUR BIRTHDAY 5 YEARS AFTER BEING ISSUED. SO IF YOUR BIRTHDAY IS JANUARY 1ST, 1992 THEN YOUR ID WILL EXPIRE JANUARY 1ST 2015. IF YOU’RE NOT GOING TO BE 21 UNTIL 2016 WE RECOMMEND MAKING YOUR BIRTHDAY IN DECEMBER SO YOUR ID LASTS ALL THE WAY UNTIL DECEMBER 2015.</p>
                                    </div>
                                </div><!-- end of tabbed2 content wrapper -->
                            </div><!-- /end of tabbed-nav2 -->
                        </div><!-- end of first content -->

                        <div>
                            <h2>Video</h2>
                            <p>We understand it can be hard to order Fake IDs over the internet. We made these videos hoping that they would help make our customers feel more comfortable when ordering with us. Scroll to the bottom of the page to see video reviews made by customers.</p>
                            <p><iframe src="http://www.youtube.com/embed/A1e2o-e4nG4" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></p>
                            <p><iframe src="http://www.youtube.com/embed/LDgzueBSbkI" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></p>
                            <p><iframe src="http://www.youtube.com/embed/SU-dMwTplRM" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></p>

                            <h1><strong>VIDEO REVIEWS</strong></h1>
                            <p>These are reviews that customers made. They did not receive free IDs. They paid for group orders and made reviews of our IDs after receiving them.</p>

                            <h2>VIDEO REVIEWS OF OUR FLORIDA ID</h2>
                            <p><iframe src="http://www.youtube.com/embed/O8QUGiBeceA" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></p>
                            <p><iframe src="http://www.youtube.com/embed/zQF3uLlcxJw" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></p>

                            <h2>VIDEO REVIEWS OF OUR CALIFORNIA ID</h2>
                            <p><iframe src="http://www.youtube.com/embed/s7puVyFXaLA" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></p>
                            <p><iframe src="http://www.youtube.com/embed/Pto-QWIQTIo" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></p>

                            <h2>VIDEO REVIEWS OF OUR NEW YORK ID</h2>
                            <p><iframe src="http://www.youtube.com/embed/2JgYNPPoJtI" height="315" width="560" allowfullscreen="" frameborder="0"></iframe><br />
                                <iframe src="http://www.youtube.com/embed/DRf6ZwGrNzA" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></p>

                            <h2>VIDEO REVIEWS OF OUR MINNESOTA ID</h2>
                            <p><iframe src="http://www.youtube.com/embed/lFVz7GEcN_w" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></p>
                        </div>
                        <div>
                            <h2>Proofs</h2>
                            <p style="text-align: center">HERE ARE PHOTOS OF ACTUAL CUSTOMER IDS THAT WE HAVE MADE<br/>
                                <strong>ALL OF THE IDS ON THIS PAGE ARE FAKE!</strong><br/><br/>
                                <em>We have blurred out their faces and information to protect their identities</em><br/><br/>
                                Look at the quality of our California hologram. Notice how crisp and clear it is. Other sellers cannot replicate the California hologram like us.</p>

                            <ul class="proofs">
                                <li><img src="img/proofs/1.jpg" alt="" /></li>
                                <li><img src="img/proofs/2.jpg" alt="" /></li>
                                <li><img src="img/proofs/3.jpg" alt="" /></li>
                                <li><img src="img/proofs/4.jpg" alt="" /></li>
                                <li><img src="img/proofs/5.jpg" alt="" /></li>
                                <li><img src="img/proofs/6.jpg" alt="" /></li>
                                <li><img src="img/proofs/7.jpg" alt="" /></li>
                                <li><img src="img/proofs/8.jpg" alt="" /></li>
                                <li><img src="img/proofs/9.jpg" alt="" /></li>
                                <li><img src="img/proofs/10.jpg" alt="" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <br>       		
                <a href="order.php" class="button">PLACE YOUR ORDER TODAY!</a>
            </div><!-- /container -->
        </section><!-- #order -->

        <section id="footer">
            <div class="container">
                <div class="half">
                    <p class="large">FloridaFakes</p>
                    <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
                </div>

                <div class="half">
                    <ul class="footer-nav">
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li><a href=""></a></li>
                        <li><a href="order.php">Order</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div><!-- /container -->
        </section><!-- #footer -->




        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/zozo.tabs.min.js"></script>
        <script src="js/main.js"></script>

        <script>
            jQuery(document).ready(function($) {
                /* jQuery activation and setting options for parent tabs*/
                $("#tabbed-nav").zozoTabs({
                    rounded: false,
                    multiline: true,
                    shadows: false,
                    defaultTab: "tab1",
                    size: "large",
                    responsive: true,
                    autoContentHeight: true,
                    animation: {
                        effects: "fade"
                    }
                });

                /* jQuery activation and setting options for child tabs within docs tab*/
                $("#tabbed-nav2").zozoTabs({
                    rounded: false,
                    shadows: false,
                    defaultTab: "tab1",
                    animation: {
                        effects: "fade"
                    },
                    size: "medium"
                });
            });
        </script>
    </body>
</html>
