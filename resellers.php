<?php
require_once("php_include/orderFunctions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Florida Fakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Kevin Rajaram">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="header">
            <div class="container">
                <div class="logo"><a href="index.php" title="Home"><img src="img/logo.png" alt="Florida Fakes" /></a></div><!-- /logo -->

                <nav>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li class="active"><a href="resellers.php">Resellers</a></li>
                        <li class="active"><a href="order.php">Order</a></li>
                    </ul>
                </nav>
            </div><!-- /container -->
        </section><!-- #header -->

        <section id="announcements">
            <div class="container">
                <h5 class="announce-icon"><strong>Announcements</strong></h5>
                <?php echo getContent('announcement'); ?>
            </div><!-- /container -->
        </section><!-- #announcements -->

        <section id="faq">
            <div class="container">

                <h1>Resellers</h1>

                <center style="color: #000; font-size:14px;">Want to make money selling something every kid at your school wants?<br/>
                        We’re looking for the most social kids on campus to represent us!</center></br/>

                    <div class="reseller-section">
                        <h4>WHAT’S IN IT FOR YOU?</h4>
                        <p>CASH</p>

                        <h4>HOW IT WORKS</h4>
                        <p>We’ll sell our IDs to you at a special discount price! You can sell them for as much as you want and keep the rest! If you have a lot of friends you’ll easily be able to make $500+ a week! It’s not like you have to work. Everyone at school wants a fake ID. All you have to do is give it to them!</p>
                    </div><!-- reseller-section -->

                    <h5>Send an email with the title “Reseller” to <a href="mailto:resellers@floridafakes.com">resellers@floridafakes.com</a> with this information:</h5>
                    <p style="font-weight:normal;">
                        Name:<br/>
                        School:<br/>
                        Fraternity/Sorority:<br/>
                        Tell us a little about yourself<br/>
                        What organizations are you a part of at school? Are you popular? Do people follow what you do? Do they listen to you a lot?
                    </p>			 

                    <h5>We will email you back with more details.</h5>

                    <br/>
                    <br/>

                    <a href="order.html" class="button">PLACE YOUR ORDER TODAY!</a>
            </div><!-- /container -->
        </section><!-- #order -->

        <section id="footer">
            <div class="container">
                <div class="half">
                    <p class="large">FloridaFakes</p>
                    <p style="padding-right:60px;"><?php echo getContent('footer'); ?></p>
                </div>

                <div class="half">
                    <ul class="footer-nav">
                        <li><a href="prices.php">Prices</a></li>
                        <li><a href="media.php">Media</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="resellers.php">Resellers</a></li>
                        <li><a href=""></a></li>
                        <li><a href="order.php">Order</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                    </ul>
                </div>
            </div><!-- /container -->
        </section><!-- #footer -->




        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script src="js/main.js"></script>

        <script>
            $("#tabs").tabs({show: {effect: "fadeIn", duration: 500}});
        </script>
    </body>
</html>
